
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/22/2018 08:31:37
-- Generated from EDMX file: C:\Users\ahmed\source\repos\orDliveryApp\orDatabase\orDliveryDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [OrDeliveryDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Administrators_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Administrators] DROP CONSTRAINT [FK_Administrators_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_CusFavSP_Customers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CusFavSP] DROP CONSTRAINT [FK_CusFavSP_Customers];
GO
IF OBJECT_ID(N'[dbo].[FK_CusFavSP_ServiceProviders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CusFavSP] DROP CONSTRAINT [FK_CusFavSP_ServiceProviders];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerAddresses_Customers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CustomerAddresses] DROP CONSTRAINT [FK_CustomerAddresses_Customers];
GO
IF OBJECT_ID(N'[dbo].[FK_Customers_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Customers] DROP CONSTRAINT [FK_Customers_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverLocations_Drivers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverLocations] DROP CONSTRAINT [FK_DriverLocations_Drivers];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverLocations_Orders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverLocations] DROP CONSTRAINT [FK_DriverLocations_Orders];
GO
IF OBJECT_ID(N'[dbo].[FK_Drivers_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Drivers] DROP CONSTRAINT [FK_Drivers_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_Items_ItemsCategories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Items] DROP CONSTRAINT [FK_Items_ItemsCategories];
GO
IF OBJECT_ID(N'[dbo].[FK_ItemsCategories_ServiceProviders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ItemsCategories] DROP CONSTRAINT [FK_ItemsCategories_ServiceProviders];
GO
IF OBJECT_ID(N'[dbo].[FK_MobileSessions_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MobileSessions] DROP CONSTRAINT [FK_MobileSessions_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_OIPropertyValues_OrderItems]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OIPropertyValues] DROP CONSTRAINT [FK_OIPropertyValues_OrderItems];
GO
IF OBJECT_ID(N'[dbo].[FK_OIPropertyValues_Properties]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OIPropertyValues] DROP CONSTRAINT [FK_OIPropertyValues_Properties];
GO
IF OBJECT_ID(N'[dbo].[FK_OIPropertyValues_PropertyValues]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OIPropertyValues] DROP CONSTRAINT [FK_OIPropertyValues_PropertyValues];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderAudits_Orders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderAudits] DROP CONSTRAINT [FK_OrderAudits_Orders];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderAudits_OrderStatues]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderAudits] DROP CONSTRAINT [FK_OrderAudits_OrderStatues];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderAudits_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderAudits] DROP CONSTRAINT [FK_OrderAudits_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderItems_Items]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderItems] DROP CONSTRAINT [FK_OrderItems_Items];
GO
IF OBJECT_ID(N'[dbo].[FK_OrderItems_Orders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrderItems] DROP CONSTRAINT [FK_OrderItems_Orders];
GO
IF OBJECT_ID(N'[dbo].[FK_Orders_CustomerAddresses]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_CustomerAddresses];
GO
IF OBJECT_ID(N'[dbo].[FK_Orders_Customers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_Customers];
GO
IF OBJECT_ID(N'[dbo].[FK_Orders_Drivers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_Drivers];
GO
IF OBJECT_ID(N'[dbo].[FK_Orders_OrderStatues]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_OrderStatues];
GO
IF OBJECT_ID(N'[dbo].[FK_Orders_ServiceProviders]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Orders] DROP CONSTRAINT [FK_Orders_ServiceProviders];
GO
IF OBJECT_ID(N'[dbo].[FK_Properties_Items]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Properties] DROP CONSTRAINT [FK_Properties_Items];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyValues_Properties]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertyValues] DROP CONSTRAINT [FK_PropertyValues_Properties];
GO
IF OBJECT_ID(N'[dbo].[FK_ServiceProviders_ServiceProviderCategories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServiceProviders] DROP CONSTRAINT [FK_ServiceProviders_ServiceProviderCategories];
GO
IF OBJECT_ID(N'[dbo].[FK_ServiceProviders_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServiceProviders] DROP CONSTRAINT [FK_ServiceProviders_Users];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Administrators]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Administrators];
GO
IF OBJECT_ID(N'[dbo].[CusFavSP]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CusFavSP];
GO
IF OBJECT_ID(N'[dbo].[CustomerAddresses]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CustomerAddresses];
GO
IF OBJECT_ID(N'[dbo].[Customers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Customers];
GO
IF OBJECT_ID(N'[dbo].[DriverLocations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DriverLocations];
GO
IF OBJECT_ID(N'[dbo].[Drivers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Drivers];
GO
IF OBJECT_ID(N'[dbo].[Items]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Items];
GO
IF OBJECT_ID(N'[dbo].[ItemsCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ItemsCategories];
GO
IF OBJECT_ID(N'[dbo].[MobileSessions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MobileSessions];
GO
IF OBJECT_ID(N'[dbo].[OIPropertyValues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OIPropertyValues];
GO
IF OBJECT_ID(N'[dbo].[OrderAudits]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrderAudits];
GO
IF OBJECT_ID(N'[dbo].[OrderItems]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrderItems];
GO
IF OBJECT_ID(N'[dbo].[Orders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Orders];
GO
IF OBJECT_ID(N'[dbo].[OrderStatues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrderStatues];
GO
IF OBJECT_ID(N'[dbo].[Properties]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Properties];
GO
IF OBJECT_ID(N'[dbo].[PropertyValues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropertyValues];
GO
IF OBJECT_ID(N'[dbo].[ServiceProviderCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ServiceProviderCategories];
GO
IF OBJECT_ID(N'[dbo].[ServiceProviders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ServiceProviders];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Administrators'
CREATE TABLE [dbo].[Administrators] (
    [AdminID] int  NOT NULL,
    [PhoneNumber] nvarchar(100)  NULL
);
GO

-- Creating table 'CustomerAddresses'
CREATE TABLE [dbo].[CustomerAddresses] (
    [AddressID] int IDENTITY(1,1) NOT NULL,
    [CustomerID] int  NOT NULL,
    [AreaName] nvarchar(50)  NOT NULL,
    [City] nvarchar(50)  NOT NULL,
    [Address1] nvarchar(max)  NOT NULL,
    [Address2] nvarchar(max)  NULL,
    [ApartmentNumber] nvarchar(50)  NULL,
    [Company] nvarchar(50)  NULL,
    [Floor] nvarchar(50)  NULL,
    [DeliveryNotes] nvarchar(max)  NULL,
    [Lat] nvarchar(50)  NULL,
    [Lng] nvarchar(50)  NULL
);
GO

-- Creating table 'Customers'
CREATE TABLE [dbo].[Customers] (
    [CustomerID] int  NOT NULL,
    [PhoneNumber] nvarchar(50)  NOT NULL,
    [eMail] nvarchar(100)  NULL,
    [ActivationCode] int  NULL,
    [CodeDate] datetime  NULL
);
GO

-- Creating table 'Drivers'
CREATE TABLE [dbo].[Drivers] (
    [DriverID] int  NOT NULL,
    [PhoneNumber] nvarchar(100)  NULL,
    [LicenseNumber] nvarchar(100)  NULL,
    [BicycleModel] nvarchar(100)  NULL
);
GO

-- Creating table 'Items'
CREATE TABLE [dbo].[Items] (
    [ItemID] int IDENTITY(1,1) NOT NULL,
    [CatID] int  NOT NULL,
    [ItemName] nvarchar(100)  NOT NULL,
    [ItemPrice] decimal(18,2)  NOT NULL,
    [Active] bit  NOT NULL,
    [Photo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ItemsCategories'
CREATE TABLE [dbo].[ItemsCategories] (
    [CatID] int IDENTITY(1,1) NOT NULL,
    [SPID] int  NOT NULL,
    [CatName] nvarchar(100)  NOT NULL,
    [Photo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'MobileSessions'
CREATE TABLE [dbo].[MobileSessions] (
    [SessionID] uniqueidentifier  NOT NULL,
    [UserID] int  NOT NULL,
    [MobileID] nvarchar(max)  NOT NULL,
    [ExpireDate] datetime  NOT NULL
);
GO

-- Creating table 'OrderAudits'
CREATE TABLE [dbo].[OrderAudits] (
    [AuditID] int IDENTITY(1,1) NOT NULL,
    [OrderID] int  NOT NULL,
    [NewStatusID] int  NOT NULL,
    [UserID] int  NOT NULL,
    [ActionDate] datetime  NOT NULL,
    [Notes] nvarchar(max)  NULL
);
GO

-- Creating table 'OrderItems'
CREATE TABLE [dbo].[OrderItems] (
    [OIID] int IDENTITY(1,1) NOT NULL,
    [OrderID] int  NOT NULL,
    [ItemID] int  NOT NULL,
    [ItemPrice] decimal(18,0)  NOT NULL,
    [Quantity] int  NOT NULL
);
GO

-- Creating table 'Orders'
CREATE TABLE [dbo].[Orders] (
    [OrderID] int IDENTITY(1,1) NOT NULL,
    [SPID] int  NULL,
    [CustomerID] int  NOT NULL,
    [OrderDate] datetime  NOT NULL,
    [OrderStatusID] int  NOT NULL,
    [DriverID] int  NULL,
    [AddressID] int  NULL,
    [Rating] int  NULL,
    [CustomerNotes] nvarchar(max)  NULL,
    [RatingComment] nvarchar(max)  NULL
);
GO

-- Creating table 'OrderStatues'
CREATE TABLE [dbo].[OrderStatues] (
    [StatusID] int  NOT NULL,
    [StatusName] nvarchar(100)  NOT NULL
);
GO

-- Creating table 'Properties'
CREATE TABLE [dbo].[Properties] (
    [PropertyID] int IDENTITY(1,1) NOT NULL,
    [ItemID] int  NOT NULL,
    [PropertyName] nvarchar(100)  NOT NULL,
    [IsMandatory] bit  NOT NULL
);
GO

-- Creating table 'PropertyValues'
CREATE TABLE [dbo].[PropertyValues] (
    [ValueID] int IDENTITY(1,1) NOT NULL,
    [PropertyID] int  NOT NULL,
    [ValueName] nvarchar(100)  NOT NULL,
    [ValuePrice] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'ServiceProviderCategories'
CREATE TABLE [dbo].[ServiceProviderCategories] (
    [CatID] int IDENTITY(1,1) NOT NULL,
    [CatName] nvarchar(100)  NOT NULL,
    [Active] bit  NOT NULL,
    [Photo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ServiceProviders'
CREATE TABLE [dbo].[ServiceProviders] (
    [SPID] int  NOT NULL,
    [SPCat] int  NOT NULL,
    [SpName] nvarchar(100)  NOT NULL,
    [MinOrder] decimal(18,0)  NOT NULL,
    [DeliveryCharges] decimal(18,0)  NOT NULL,
    [WrokingFrom] time  NOT NULL,
    [WorkingTill] time  NOT NULL,
    [Address] nvarchar(500)  NOT NULL,
    [Lat] nvarchar(50)  NOT NULL,
    [Lng] nvarchar(50)  NOT NULL,
    [Photo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserID] int IDENTITY(1,1) NOT NULL,
    [LoginName] nvarchar(100)  NOT NULL,
    [LoginPassword] nchar(10)  NULL,
    [fName] nvarchar(100)  NULL,
    [mName] nvarchar(100)  NULL,
    [lName] nvarchar(100)  NULL,
    [Active] bit  NOT NULL,
    [Photo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'OIPropertyValues'
CREATE TABLE [dbo].[OIPropertyValues] (
    [OIPVID] int IDENTITY(1,1) NOT NULL,
    [OIID] int  NOT NULL,
    [PropertyID] int  NOT NULL,
    [ValueID] int  NOT NULL,
    [ValuePrice] decimal(18,0)  NOT NULL
);
GO

-- Creating table 'DriverLocations'
CREATE TABLE [dbo].[DriverLocations] (
    [LocationID] int IDENTITY(1,1) NOT NULL,
    [DriverID] int  NOT NULL,
    [Lat] decimal(18,0)  NOT NULL,
    [Lng] decimal(18,0)  NOT NULL,
    [SyncDate] datetime  NOT NULL,
    [OrderID] int  NULL
);
GO

-- Creating table 'CusFavSP'
CREATE TABLE [dbo].[CusFavSP] (
    [Customers_CustomerID] int  NOT NULL,
    [ServiceProviders_SPID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AdminID] in table 'Administrators'
ALTER TABLE [dbo].[Administrators]
ADD CONSTRAINT [PK_Administrators]
    PRIMARY KEY CLUSTERED ([AdminID] ASC);
GO

-- Creating primary key on [AddressID] in table 'CustomerAddresses'
ALTER TABLE [dbo].[CustomerAddresses]
ADD CONSTRAINT [PK_CustomerAddresses]
    PRIMARY KEY CLUSTERED ([AddressID] ASC);
GO

-- Creating primary key on [CustomerID] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [PK_Customers]
    PRIMARY KEY CLUSTERED ([CustomerID] ASC);
GO

-- Creating primary key on [DriverID] in table 'Drivers'
ALTER TABLE [dbo].[Drivers]
ADD CONSTRAINT [PK_Drivers]
    PRIMARY KEY CLUSTERED ([DriverID] ASC);
GO

-- Creating primary key on [ItemID] in table 'Items'
ALTER TABLE [dbo].[Items]
ADD CONSTRAINT [PK_Items]
    PRIMARY KEY CLUSTERED ([ItemID] ASC);
GO

-- Creating primary key on [CatID] in table 'ItemsCategories'
ALTER TABLE [dbo].[ItemsCategories]
ADD CONSTRAINT [PK_ItemsCategories]
    PRIMARY KEY CLUSTERED ([CatID] ASC);
GO

-- Creating primary key on [SessionID] in table 'MobileSessions'
ALTER TABLE [dbo].[MobileSessions]
ADD CONSTRAINT [PK_MobileSessions]
    PRIMARY KEY CLUSTERED ([SessionID] ASC);
GO

-- Creating primary key on [AuditID] in table 'OrderAudits'
ALTER TABLE [dbo].[OrderAudits]
ADD CONSTRAINT [PK_OrderAudits]
    PRIMARY KEY CLUSTERED ([AuditID] ASC);
GO

-- Creating primary key on [OIID] in table 'OrderItems'
ALTER TABLE [dbo].[OrderItems]
ADD CONSTRAINT [PK_OrderItems]
    PRIMARY KEY CLUSTERED ([OIID] ASC);
GO

-- Creating primary key on [OrderID] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [PK_Orders]
    PRIMARY KEY CLUSTERED ([OrderID] ASC);
GO

-- Creating primary key on [StatusID] in table 'OrderStatues'
ALTER TABLE [dbo].[OrderStatues]
ADD CONSTRAINT [PK_OrderStatues]
    PRIMARY KEY CLUSTERED ([StatusID] ASC);
GO

-- Creating primary key on [PropertyID] in table 'Properties'
ALTER TABLE [dbo].[Properties]
ADD CONSTRAINT [PK_Properties]
    PRIMARY KEY CLUSTERED ([PropertyID] ASC);
GO

-- Creating primary key on [ValueID] in table 'PropertyValues'
ALTER TABLE [dbo].[PropertyValues]
ADD CONSTRAINT [PK_PropertyValues]
    PRIMARY KEY CLUSTERED ([ValueID] ASC);
GO

-- Creating primary key on [CatID] in table 'ServiceProviderCategories'
ALTER TABLE [dbo].[ServiceProviderCategories]
ADD CONSTRAINT [PK_ServiceProviderCategories]
    PRIMARY KEY CLUSTERED ([CatID] ASC);
GO

-- Creating primary key on [SPID] in table 'ServiceProviders'
ALTER TABLE [dbo].[ServiceProviders]
ADD CONSTRAINT [PK_ServiceProviders]
    PRIMARY KEY CLUSTERED ([SPID] ASC);
GO

-- Creating primary key on [UserID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserID] ASC);
GO

-- Creating primary key on [OIPVID] in table 'OIPropertyValues'
ALTER TABLE [dbo].[OIPropertyValues]
ADD CONSTRAINT [PK_OIPropertyValues]
    PRIMARY KEY CLUSTERED ([OIPVID] ASC);
GO

-- Creating primary key on [LocationID] in table 'DriverLocations'
ALTER TABLE [dbo].[DriverLocations]
ADD CONSTRAINT [PK_DriverLocations]
    PRIMARY KEY CLUSTERED ([LocationID] ASC);
GO

-- Creating primary key on [Customers_CustomerID], [ServiceProviders_SPID] in table 'CusFavSP'
ALTER TABLE [dbo].[CusFavSP]
ADD CONSTRAINT [PK_CusFavSP]
    PRIMARY KEY CLUSTERED ([Customers_CustomerID], [ServiceProviders_SPID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AdminID] in table 'Administrators'
ALTER TABLE [dbo].[Administrators]
ADD CONSTRAINT [FK_Administrators_Users]
    FOREIGN KEY ([AdminID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [CustomerID] in table 'CustomerAddresses'
ALTER TABLE [dbo].[CustomerAddresses]
ADD CONSTRAINT [FK_CustomerAddresses_Customers]
    FOREIGN KEY ([CustomerID])
    REFERENCES [dbo].[Customers]
        ([CustomerID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerAddresses_Customers'
CREATE INDEX [IX_FK_CustomerAddresses_Customers]
ON [dbo].[CustomerAddresses]
    ([CustomerID]);
GO

-- Creating foreign key on [AddressID] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_Orders_CustomerAddresses]
    FOREIGN KEY ([AddressID])
    REFERENCES [dbo].[CustomerAddresses]
        ([AddressID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Orders_CustomerAddresses'
CREATE INDEX [IX_FK_Orders_CustomerAddresses]
ON [dbo].[Orders]
    ([AddressID]);
GO

-- Creating foreign key on [CustomerID] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [FK_Customers_Users]
    FOREIGN KEY ([CustomerID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [CustomerID] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_Orders_Customers]
    FOREIGN KEY ([CustomerID])
    REFERENCES [dbo].[Customers]
        ([CustomerID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Orders_Customers'
CREATE INDEX [IX_FK_Orders_Customers]
ON [dbo].[Orders]
    ([CustomerID]);
GO

-- Creating foreign key on [DriverID] in table 'Drivers'
ALTER TABLE [dbo].[Drivers]
ADD CONSTRAINT [FK_Drivers_Users]
    FOREIGN KEY ([DriverID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [DriverID] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_Orders_Drivers]
    FOREIGN KEY ([DriverID])
    REFERENCES [dbo].[Drivers]
        ([DriverID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Orders_Drivers'
CREATE INDEX [IX_FK_Orders_Drivers]
ON [dbo].[Orders]
    ([DriverID]);
GO

-- Creating foreign key on [CatID] in table 'Items'
ALTER TABLE [dbo].[Items]
ADD CONSTRAINT [FK_Items_ItemsCategories]
    FOREIGN KEY ([CatID])
    REFERENCES [dbo].[ItemsCategories]
        ([CatID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Items_ItemsCategories'
CREATE INDEX [IX_FK_Items_ItemsCategories]
ON [dbo].[Items]
    ([CatID]);
GO

-- Creating foreign key on [ItemID] in table 'OrderItems'
ALTER TABLE [dbo].[OrderItems]
ADD CONSTRAINT [FK_OrderItems_Items]
    FOREIGN KEY ([ItemID])
    REFERENCES [dbo].[Items]
        ([ItemID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderItems_Items'
CREATE INDEX [IX_FK_OrderItems_Items]
ON [dbo].[OrderItems]
    ([ItemID]);
GO

-- Creating foreign key on [ItemID] in table 'Properties'
ALTER TABLE [dbo].[Properties]
ADD CONSTRAINT [FK_Properties_Items]
    FOREIGN KEY ([ItemID])
    REFERENCES [dbo].[Items]
        ([ItemID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Properties_Items'
CREATE INDEX [IX_FK_Properties_Items]
ON [dbo].[Properties]
    ([ItemID]);
GO

-- Creating foreign key on [SPID] in table 'ItemsCategories'
ALTER TABLE [dbo].[ItemsCategories]
ADD CONSTRAINT [FK_ItemsCategories_ServiceProviders]
    FOREIGN KEY ([SPID])
    REFERENCES [dbo].[ServiceProviders]
        ([SPID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ItemsCategories_ServiceProviders'
CREATE INDEX [IX_FK_ItemsCategories_ServiceProviders]
ON [dbo].[ItemsCategories]
    ([SPID]);
GO

-- Creating foreign key on [UserID] in table 'MobileSessions'
ALTER TABLE [dbo].[MobileSessions]
ADD CONSTRAINT [FK_MobileSessions_Users]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MobileSessions_Users'
CREATE INDEX [IX_FK_MobileSessions_Users]
ON [dbo].[MobileSessions]
    ([UserID]);
GO

-- Creating foreign key on [OrderID] in table 'OrderAudits'
ALTER TABLE [dbo].[OrderAudits]
ADD CONSTRAINT [FK_OrderAudits_Orders]
    FOREIGN KEY ([OrderID])
    REFERENCES [dbo].[Orders]
        ([OrderID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderAudits_Orders'
CREATE INDEX [IX_FK_OrderAudits_Orders]
ON [dbo].[OrderAudits]
    ([OrderID]);
GO

-- Creating foreign key on [NewStatusID] in table 'OrderAudits'
ALTER TABLE [dbo].[OrderAudits]
ADD CONSTRAINT [FK_OrderAudits_OrderStatues]
    FOREIGN KEY ([NewStatusID])
    REFERENCES [dbo].[OrderStatues]
        ([StatusID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderAudits_OrderStatues'
CREATE INDEX [IX_FK_OrderAudits_OrderStatues]
ON [dbo].[OrderAudits]
    ([NewStatusID]);
GO

-- Creating foreign key on [UserID] in table 'OrderAudits'
ALTER TABLE [dbo].[OrderAudits]
ADD CONSTRAINT [FK_OrderAudits_Users]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderAudits_Users'
CREATE INDEX [IX_FK_OrderAudits_Users]
ON [dbo].[OrderAudits]
    ([UserID]);
GO

-- Creating foreign key on [OrderID] in table 'OrderItems'
ALTER TABLE [dbo].[OrderItems]
ADD CONSTRAINT [FK_OrderItems_Orders]
    FOREIGN KEY ([OrderID])
    REFERENCES [dbo].[Orders]
        ([OrderID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrderItems_Orders'
CREATE INDEX [IX_FK_OrderItems_Orders]
ON [dbo].[OrderItems]
    ([OrderID]);
GO

-- Creating foreign key on [OrderStatusID] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_Orders_OrderStatues]
    FOREIGN KEY ([OrderStatusID])
    REFERENCES [dbo].[OrderStatues]
        ([StatusID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Orders_OrderStatues'
CREATE INDEX [IX_FK_Orders_OrderStatues]
ON [dbo].[Orders]
    ([OrderStatusID]);
GO

-- Creating foreign key on [SPID] in table 'Orders'
ALTER TABLE [dbo].[Orders]
ADD CONSTRAINT [FK_Orders_ServiceProviders]
    FOREIGN KEY ([SPID])
    REFERENCES [dbo].[ServiceProviders]
        ([SPID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Orders_ServiceProviders'
CREATE INDEX [IX_FK_Orders_ServiceProviders]
ON [dbo].[Orders]
    ([SPID]);
GO

-- Creating foreign key on [PropertyID] in table 'PropertyValues'
ALTER TABLE [dbo].[PropertyValues]
ADD CONSTRAINT [FK_PropertyValues_Properties]
    FOREIGN KEY ([PropertyID])
    REFERENCES [dbo].[Properties]
        ([PropertyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyValues_Properties'
CREATE INDEX [IX_FK_PropertyValues_Properties]
ON [dbo].[PropertyValues]
    ([PropertyID]);
GO

-- Creating foreign key on [SPCat] in table 'ServiceProviders'
ALTER TABLE [dbo].[ServiceProviders]
ADD CONSTRAINT [FK_ServiceProviders_ServiceProviderCategories]
    FOREIGN KEY ([SPCat])
    REFERENCES [dbo].[ServiceProviderCategories]
        ([CatID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ServiceProviders_ServiceProviderCategories'
CREATE INDEX [IX_FK_ServiceProviders_ServiceProviderCategories]
ON [dbo].[ServiceProviders]
    ([SPCat]);
GO

-- Creating foreign key on [SPID] in table 'ServiceProviders'
ALTER TABLE [dbo].[ServiceProviders]
ADD CONSTRAINT [FK_ServiceProviders_Users]
    FOREIGN KEY ([SPID])
    REFERENCES [dbo].[Users]
        ([UserID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Customers_CustomerID] in table 'CusFavSP'
ALTER TABLE [dbo].[CusFavSP]
ADD CONSTRAINT [FK_CusFavSP_Customer]
    FOREIGN KEY ([Customers_CustomerID])
    REFERENCES [dbo].[Customers]
        ([CustomerID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ServiceProviders_SPID] in table 'CusFavSP'
ALTER TABLE [dbo].[CusFavSP]
ADD CONSTRAINT [FK_CusFavSP_ServiceProvider]
    FOREIGN KEY ([ServiceProviders_SPID])
    REFERENCES [dbo].[ServiceProviders]
        ([SPID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CusFavSP_ServiceProvider'
CREATE INDEX [IX_FK_CusFavSP_ServiceProvider]
ON [dbo].[CusFavSP]
    ([ServiceProviders_SPID]);
GO

-- Creating foreign key on [OIID] in table 'OIPropertyValues'
ALTER TABLE [dbo].[OIPropertyValues]
ADD CONSTRAINT [FK_OIPropertyValues_OrderItems]
    FOREIGN KEY ([OIID])
    REFERENCES [dbo].[OrderItems]
        ([OIID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OIPropertyValues_OrderItems'
CREATE INDEX [IX_FK_OIPropertyValues_OrderItems]
ON [dbo].[OIPropertyValues]
    ([OIID]);
GO

-- Creating foreign key on [PropertyID] in table 'OIPropertyValues'
ALTER TABLE [dbo].[OIPropertyValues]
ADD CONSTRAINT [FK_OIPropertyValues_Properties]
    FOREIGN KEY ([PropertyID])
    REFERENCES [dbo].[Properties]
        ([PropertyID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OIPropertyValues_Properties'
CREATE INDEX [IX_FK_OIPropertyValues_Properties]
ON [dbo].[OIPropertyValues]
    ([PropertyID]);
GO

-- Creating foreign key on [ValueID] in table 'OIPropertyValues'
ALTER TABLE [dbo].[OIPropertyValues]
ADD CONSTRAINT [FK_OIPropertyValues_PropertyValues]
    FOREIGN KEY ([ValueID])
    REFERENCES [dbo].[PropertyValues]
        ([ValueID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OIPropertyValues_PropertyValues'
CREATE INDEX [IX_FK_OIPropertyValues_PropertyValues]
ON [dbo].[OIPropertyValues]
    ([ValueID]);
GO

-- Creating foreign key on [DriverID] in table 'DriverLocations'
ALTER TABLE [dbo].[DriverLocations]
ADD CONSTRAINT [FK_DriverLocations_Drivers]
    FOREIGN KEY ([DriverID])
    REFERENCES [dbo].[Drivers]
        ([DriverID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverLocations_Drivers'
CREATE INDEX [IX_FK_DriverLocations_Drivers]
ON [dbo].[DriverLocations]
    ([DriverID]);
GO

-- Creating foreign key on [OrderID] in table 'DriverLocations'
ALTER TABLE [dbo].[DriverLocations]
ADD CONSTRAINT [FK_DriverLocations_Orders]
    FOREIGN KEY ([OrderID])
    REFERENCES [dbo].[Orders]
        ([OrderID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverLocations_Orders'
CREATE INDEX [IX_FK_DriverLocations_Orders]
ON [dbo].[DriverLocations]
    ([OrderID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------