﻿
using System.Web;
using System.Web.Optimization;

namespace orDliveryApp
{
    public static class BundelConfig
    {

       // private static readonly string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["CdnBaseUrl"];
        public static void RegesterBundels(BundleCollection bundels) 
        {



           // bundels.Add( new )

            bundels.Add(new StyleBundle("~/Content/Css").Include(
            "~/Theme/css/bootstrap.min.css",
            "~/Theme/css/bootstrap-override.css",
            "~/Theme/css/weather-icons.min.css",
            "~/Theme/css/jquery-ui-1.10.3.css",
            "~/Theme/css/font-awesome.min.css",
            "~/Theme/css/animate.min.css",
             "~/Theme/css/animate.delay.css",
             "~/Theme/css/toggles.css",
             "~/Theme/css/select2.css",
             "~/Theme/css/lato.css",
             "~/Theme/css/roboto.css",
             "~/Theme/css/bootstrap-datepicker.css",

              //"~/Theme/css/font/JF-Flat-regular.eot",
              //"~/Theme/css/font/JF-Flat-regular.eot?#iefix",
              // "~/Theme/css/font/JF-Flat-regular.svg#JF Flat Regular",
              //"~/Theme/css/font/JF-Flat-regular.woff",
              // "~/Theme/css/font/JF-Flat-regular.ttf",


              "~/Theme/css/style.default.css",
              "~/Theme/css/bootstrap-rtl.min.css",
              "~/Theme/css/bootstrap-override-rtl.css",
              "~/Theme/css/style.default-rtl.css",
              "~/Theme/css/flat-jooza.css",
              "~/Theme/css/bootstrap-toggle.min.css",
               "~/Theme/css/jquery.gritter.css",
               "~/Theme/css/jquery.datatables.css",
               "~/Theme/css/bootstrap-timepicker.min.css",
              "~/Theme/css/LoanAppCss.css"
              
             

               ));



            bundels.Add(new ScriptBundle("~/Content/Js").Include(

           //      //  "~/Scripts/jquery-3.3.1.js",
           //       "~/Theme/js/jquery-1.11.1.min.js",
           //      "~/Scripts/jquery-3.3.1.js",
           //        "~/Theme/js/jquery-migrate-1.2.1.min.js",
           //        "~/Theme/js/jquery-ui-1.10.3.min.js",
           //        "~/Theme/js/bootstrap.min.js",
           //        "~/Theme/js/modernizr.min.js",
           //        "~/Theme/js/jquery.sparkline.min.js",
           //        "~/Theme/js/toggles.min.js",
           //        "~/Theme/js/retina.min.js",
           //        "~/Theme/js/jquery.cookies.js",
           //        "~/Theme/js/flot/jquery.flot.min.js",
           //        "~/Theme/js/flot/jquery.flot.resize.min.js",
           //        "~/Theme/js/flot/jquery.flot.spline.min.js",
           //        "~/Theme/js/morris.min.js",
           //        "~/Theme/js/raphael-2.1.0.min.js",
           //      //  "~/Theme/js/custom.js",
           //        "~/Theme/js/jquery.validate.min.js",
           //        "~/Theme/js/jquery.datatables.min.js",
           //        "~/Theme/js/jquery.gritter.min.js",
           //        "~/Theme/js/bootstrap-toggle.min.js",
           //        "~/Scripts/App/app.js"
           ////   "~/Theme/js/gmaps.js"



                 "~/Theme/js/jquery-1.11.1.min.js",
                 "~/Theme/js/jquery-migrate-1.2.1.min.js",
                 "~/Theme/js/jquery-ui-1.10.3.min.js",
                 "~/Theme/js/bootstrap.min.js",
                 "~/Theme/js/modernizr.min.js",
                 "~/Theme/js/jquery.sparkline.min.js",
                 "~/Theme/js/toggles.min.js",
                 "~/Theme/js/retina.min.js",
                 "~/Theme/js/jquery.cookies.js",
                 "~/Theme/js/flot/jquery.flot.min.js",
                 "~/Theme/js/flot/jquery.flot.resize.min.js",
                 "~/Theme/js/flot/jquery.flot.spline.min.js",
                 "~/Theme/js/morris.min.js",
                 "~/Theme/js/raphael-2.1.0.min.js",
                 "~/Theme/js/custom.js",
                 "~/Theme/js/jquery.validate.min.js",
                 "~/Theme/js/jquery.datatables.min.js",
                 "~/Theme/js/jquery.gritter.min.js",
                 "~/Theme/js/bootstrap-toggle.min.js",
                 "~/Theme/js/bootstrap-timepicker.min.js",
                 "~/Scripts/jquery.signalR-2.2.3.min.js",
                 "~/Scripts/App/app.js",
                 "~/Scripts/App/Reports.js",
                 "~/Scripts/jquery.playSound.js",
                 "~/Scripts/bootstrap-datepicker.min.js",
                 "~/SignalR/hubs"
         //    "~/Theme/js/toggles.min.js",
         //   "~/Theme/js/gmaps.js"








         ));

           BundleTable.EnableOptimizations = false ;

        }

    }
}