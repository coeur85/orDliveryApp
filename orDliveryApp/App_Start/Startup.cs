﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartup(typeof(orDliveryApp.Models.Startup))]

namespace orDliveryApp.Models
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            app.MapSignalR();

            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "orCookie",
                LoginPath = new PathString("/login/index"),
                Provider = new CookieAuthenticationProvider(),
                CookieName = "orCookie",
                CookieHttpOnly = true,
                ExpireTimeSpan = TimeSpan.FromHours(1),
            });

            //app.UseWsFederationAuthentication(
            //    new WsFederationAuthenticationOptions
            //    {
            //        Wtrealm = realm,
            //        MetadataAddress = adfsMetadata
            //    });

        }
    }
}
