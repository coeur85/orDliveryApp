﻿


function filterByDriver(id, div)
{
    loadingDiv(div);

    ajaxGet('SearchDriver', 'Reports', id, function (d) {
        $(div).html(d);
        initDT();

    });

}



function filterByCustomer(id, div) {
    loadingDiv(div);

    ajaxGet('SearchCustomer', 'Reports', id, function (d) {
        $(div).html(d);
        initDT();

    });

}


function filterBySP(id, div) {
    loadingDiv(div);

    ajaxGet('SearchSP', 'Reports', id, function (d) {
        $(div).html(d);
        initDT();

    });

}

function reports(js,div) {
    loadingDiv(div);

    ajaxGetJson('CompleteReport', 'Reports', js, function (d) {
        $(div).html(d);
        initDT();

    });

}

function PrintReport(js) {



}


function loadingDiv(div)
{
    $(div).html('<h3 class="text-center">جاري التحميل</h3>')
}