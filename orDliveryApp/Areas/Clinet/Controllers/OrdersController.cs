﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    public class OrdersController : Controller
    {
        // GET: Clinet/Orders


        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();

        private List<Order> Orders()
        {
            return db.Orders.Where(x => x.SPID == _app.UI.CurrentUser.UserID && x.OrderStatusID !=1).ToList()
                .OrderByDescending(x => x.LastAudit.ActionDate).ToList();
        }
        public ActionResult Index()
        {
            return View(Orders());
        }
        public ActionResult CustomerDetails(int id)
        {
            var order = Orders().FirstOrDefault(x=> x.OrderID == id);
            return View(order);
        }
        public ActionResult Status(int id)
        {
            if (id == 2 || id == 4 || id == 11)
            {

                ViewBag.Tilte = db.OrderStatues.FirstOrDefault(x => x.StatusID == id).StatusName;
                return View(Orders().Where(x => x.OrderStatusID == id).ToList());

            }
            else { return HttpNotFound(); }
        }
        public ActionResult OrderDetails(int id)
        {
            var ord = Orders().FirstOrDefault(x => x.OrderID == id);

            return View(ord);
        }


        public ActionResult DeclineOrder(Order order)
        {
            var dbord = Orders().FirstOrDefault(x=> x.OrderID== order.OrderID);

            _app.Audits.NewForStatus5(dbord, _app.UI.CurrentUser.UserID,order.DeclineReason );
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }
        public ActionResult AcceptOrder(Order order)
        {
            var dbord = Orders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus4(dbord, _app.UI.CurrentUser.UserID);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }
        public ActionResult RequestDriver(Order order)
        {
            var dbord = Orders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus6(dbord, _app.UI.CurrentUser.UserID);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }
        public ActionResult PackagePickedUp(Order order)
        {

            var dbord = Orders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus10(dbord, _app.UI.CurrentUser.UserID);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });

        }
      
    }
}