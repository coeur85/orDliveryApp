﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Clinet/Profile
        public ActionResult Index()
        {
            return View(_app.UI.CurrentUser);
        }


        [HttpPost]
        public ActionResult Save(User u)
        {
            OrDeliveryDBEntities db = new OrDeliveryDBEntities();
            var oldusr = db.Users.Find(u.UserID);
            oldusr.fName = u.fName;
            oldusr.lName = u.lName;
            oldusr.mName = u.mName;
            oldusr.LoginPassword = u.LoginPassword;
            oldusr.Photo = _app.UI.Photos.replacePhoto(oldusr.Photo, Request.Files);
            db.Entry(oldusr).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            _app.UI.msg.Show = "تم حفظ التعيرات بنجاح";

            return RedirectToAction("index", new { id = oldusr.UserID });
            // return View();
        }
    }
}