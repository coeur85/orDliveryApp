﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    public class MySettingsController : Controller
    {
        // GET: Clinet/MySettings

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult Index()
        {

            var sp = db.ServiceProviders.Find(_app.UI.CurrentUser.UserID);


            ViewBag.SPCat = new SelectList(db.ServiceProviderCategories.Where(x => x.CatID == sp.SPCat), "CatID", "CatName", sp.SPCat);

            return View(sp);
        }

        [HttpPost]
        public ActionResult Index(ServiceProvider sp)
        {

            var old = db.ServiceProviders.Find(sp.SPID);

            old.Address = sp.Address;
            old.DeliveryCharges = sp.DeliveryCharges;
            //old.Lat = sp.Lat;
            //old.Lng = sp.Lng;
            old.MinOrder = sp.MinOrder;
            old.Photo = _app.UI.Photos.replacePhoto(old.Photo, sp.Photo);
            old.SPCat = sp.SPCat;
            old.SpName = sp.SpName;
            old.WorkingTill = sp.WorkingTill;
            old.WrokingFrom = sp.WrokingFrom;


            old.User.fName = sp.User.fName;
            old.User.mName = sp.User.mName;
            old.User.lName = sp.User.lName;

            old.User.LoginName = sp.User.LoginName;
            old.User.Photo = _app.UI.Photos.replacePhoto(old.User.Photo, sp.User.Photo);

            old.Lat = Convert.ToString(Request.Form["gmap" + old.SPID.ToString() + "-t"]);
            old.Lng = Convert.ToString(Request.Form["gmap" + old.SPID.ToString() + "-g"]);


            db.Entry(old).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("index", "Home");


        }



    }
}