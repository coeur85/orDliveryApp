﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Clinet/Customer
        OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult Details(int id)
        {
            var s = db.Customers.Find(id);
            return View(s);
        }
    }
}