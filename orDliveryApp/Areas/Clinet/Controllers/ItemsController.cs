﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    public class ItemsController : Controller
    {
        // GET: Clinet/Items
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult Index()
        {
            var sp = _app.UI.CurrentUser.ServiceProvider;
            var cl = db.ItemsCategories.Where(x => x.SPID == sp.SPID);
            var il = db.Items.Where(x => cl.Any(y => x.CatID == y.CatID)).ToList();
            return View(il);
        }

        public ActionResult Create()
        {
            Item p = new Item(true);
             var sp = _app.UI.CurrentUser.ServiceProvider;
            sp.ItemsCategories.Clear();
            sp.ItemsCategories = db.ItemsCategories.Where(x => x.SPID == sp.SPID).ToList();
            if (Request.QueryString["catid"] == null) { ViewBag.CatID = new SelectList(sp.ItemsCategories, "CatID", "CatName"); }
            else { ViewBag.CatID = new SelectList(sp.ItemsCategories, "CatID", "CatName", Convert.ToInt32(Request.QueryString["catid"]) );  }
            

            return View(p);
        }

        [HttpPost]
        public ActionResult Create(Item item)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(item);
                db.SaveChanges();
                return RedirectToAction("index");
            }
            var sp = _app.UI.CurrentUser.ServiceProvider;
            ViewBag.CatID = new SelectList(sp.ItemsCategories, "CatID", "CatName");
            return View(item);
        }
        public ActionResult Edit(int id)
        {
            var sp = _app.UI.CurrentUser.ServiceProvider;
            var item = db.Items.Find(id);
            sp.ItemsCategories.Clear();
            sp.ItemsCategories = db.ItemsCategories.Where(x => x.SPID == sp.SPID).ToList();
            if (!sp.ItemsCategories.Any(x => x.CatID == item.CatID)) { return HttpNotFound(); }
            ViewBag.CatID = new SelectList(sp.ItemsCategories, "CatID", "CatName",item.CatID);
            return View(item);
        }
        [HttpPost]
        public ActionResult Edit(Item item)
        {
            var old = db.Items.Find(item.ItemID);

            old.Active = item.Active;
            old.ItemName = item.ItemName;
            old.ItemPrice = item.ItemPrice;
            old.CatID = item.CatID;
            old.Photo = _app.UI.Photos.replacePhoto(old.Photo, Request.Files);
            old.Description = item.Description;
            db.Entry(old).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("index");
        }

        public ActionResult Details(int id)
        {
            var item = db.Items.Find(id);
            var sp = _app.UI.CurrentUser.ServiceProvider;
            if (!sp.ItemsCategories.Any(x => x.CatID == item.CatID)) { return HttpNotFound(); }
            return View(item);
        }

    }
}