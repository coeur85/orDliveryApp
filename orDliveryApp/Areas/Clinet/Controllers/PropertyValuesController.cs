﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    public class PropertyValuesController : Controller
    {
        // GET: Clinet/PropertyValues

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();


        public ActionResult Index(int id)
        {

            // if (!isValied(id)) { return HttpNotFound(); }

            var it = db.Properties.Find(id);
            var cl = db.ItemsCategories.Where(x => x.SPID == _app.UI.CurrentUser.UserID);
            if (!cl.Any(x => x.CatID == it.Item.CatID)) { return HttpNotFound(); }

            var pr = db.Properties.FirstOrDefault(x => x.PropertyID == id);
            return View(pr);
        }

        public ActionResult Create(int id)
        {
            PropertyValue v = new PropertyValue { PropertyID = id };
            v.Property = db.Properties.Find(id);
            return View(v);
        }
        [HttpPost]
        public ActionResult Create(PropertyValue property)
        {
            if (ModelState.IsValid)
            {

                db.PropertyValues.Add(property);
                db.SaveChanges();
                return RedirectToAction("index", new { id = property.PropertyID });
            }

            return View(property);
        }
        public ActionResult Edit(int id)
        {
            if (!isValied(id)) { return HttpNotFound(); }
            var v = db.PropertyValues.Find(id);
            return View(v);
        }
        [HttpPost]
        public ActionResult Edit(PropertyValue property)
        {
            var old = db.PropertyValues.Find(property.PropertyID);


            old.ValueName = property.ValueName;
            old.ValuePrice = property.ValuePrice;

            db.Entry(old).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("index", new { id = property.PropertyID });
        }
        public ActionResult Details(int id)
        {
            if (!isValied(id)) { return HttpNotFound(); }

            var pv = db.PropertyValues.Find(id);

            return View(pv);
        }


        private bool isValied(int id)
        {

            var sp = _app.UI.CurrentUser.ServiceProvider;
            var pv = db.PropertyValues.Find(id);
            var cl = db.ItemsCategories.Where(x => x.SPID == sp.SPID);

            if (cl.Any(x => pv.Property.Item.CatID == x.CatID)) { return true; }
            else { return false; }




        }


    }
}