﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDliveryApp;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    public class HomeController : Controller
    {
        // GET: Clinet/Home
        public ActionResult Index()
        {
            //ServiceProviderHub spHub = new ServiceProviderHub();


            OrDeliveryDBEntities db = new OrDeliveryDBEntities();
            var sp = _app.UI.CurrentUser.ServiceProvider;

            var ord = sp.Orders;

            _app.PageClass.ClientHomePage hp = new _app.PageClass.ClientHomePage();

            hp.CusAll = ord.Select(x => x.CustomerID).Distinct().Count();

            hp.IsOpen = _app.WepApi.Convert.DB2Json.isWorkinkNow(sp.WrokingFrom, sp.WorkingTill);
            hp.From = sp.WrokingFrom;
            hp.To = sp.WorkingTill;


            hp.OrdAll = ord.Where(x => x.OrderStatusID != 1).Count();         
            hp.OrdOpen = ord.Where(x => OrderStatusEnum.OpenOrders.Any(y => x.OrderStatusID == y && x.OrderStatusID != 1)).Count();
            hp.OrdClosed = hp.OrdAll - hp.OrdOpen;
            hp.OpenOrders = ord.Where(x=> OrderStatusEnum.OpenOrders.Any(y=> x.OrderStatusID == y && x.OrderStatusID != 1));


            foreach (var menu in sp.ItemsCategories.ToList())
            {
                

                foreach (var item in menu.Items.ToList())
                {
                    hp.ItemAll += 1;
                    if (item.Active) { hp.ItemOn += 1; }
                    else { hp.itemOff += 1; }

                }


            }



            var chr = ord.GroupBy(x => x.LastAudit.ActionDate.Date).Select(x => new { value = x.Count(), date = (DateTime)x.Key }).ToList();


            hp.Chart1 = new _app.PageClass.ChartClass
            {
                CanvesID = 1,
                ChartType = "line",
                text = "الطلبات الناجحه في خلال 30 يوم",
                PointTilite = "طلب ",
                label = "عدد الطلبات",
                xAexTitle = "التاريخ" ,
                yAexTitle = "العدد",
                Color = "red",
                Lables = chr.Select(x => x.date.ToShortDateString()).ToArray(),
                Data = chr.Select(x => x.value.ToString()).ToArray()

            };

            return View(hp);
        }



        public ActionResult Search(string keyword)
        {
            _app.PageClass.ClientSearch search = new _app.PageClass.ClientSearch
            {

                KeyWord = keyword


            };
             OrDeliveryDBEntities db = new OrDeliveryDBEntities();
            var sp = db.ServiceProviders.FirstOrDefault(x=> x.SPID == _app.UI.CurrentUser.UserID);
            var ord = sp.Orders.ToList();
            var cus =sp.MyCustomers.ToList();
           

            if (string.IsNullOrEmpty(keyword)) { return View(search); }

            var splstr = keyword.Trim().Split(' ');



            foreach (var s in splstr)
            {

                if (s.Any(x => Char.IsLetter(x)))
                {
                    search.Customers.AddRange(cus.Where(x => x.User.fName.Contains(s) || x.User.lName.Contains(s) ||
                    x.User.mName.Contains(s) || x.User.LoginName.Contains(s) &&
                    !search.Customers.Any(c => x.User.UserID == c.CustomerID)).ToList());


                    foreach (var itemCat in sp.ItemsCategories.ToList())
                    {

                        search.Items.AddRange(itemCat.Items.ToList().Where(x => 
                        x.ItemName.Contains(s)   || Containes(x.Description , s)  ||
                           x.ItemsCategory.CatName.Contains(s) 
                       && !search.Items.Any(y => y.ItemID == x.ItemID)
                        ).ToList());
                    }


                }

                else

                {

                    search.Customers.AddRange(cus.Where(x => x.PhoneNumber.Contains(s) &&
                   !search.Customers.Any(c => x.User.UserID == c.CustomerID)).ToList());


                    foreach (var itemCat in sp.ItemsCategories.ToList())
                    {

                        search.Items.AddRange(itemCat.Items.Where(x => x.ItemPrice.ToString() == s 
                         && !search.Items.Any(y => y.ItemID == x.ItemID)).ToList());
                    }

                    search.Orders.AddRange(ord.Where(x => x.OrderID.ToString().Contains(s)
                   && !search.Orders.Any(y => y.OrderID == x.OrderID)).ToList());


                }


            }

            return View(search);

        }

        private bool Containes(string ob, string value)
        {
            if (string.IsNullOrEmpty(ob)) { return false; }

            return ob.Contains(value);
        }
    }
}