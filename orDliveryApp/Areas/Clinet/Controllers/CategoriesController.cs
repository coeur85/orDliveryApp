﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    
    public class CategoriesController : Controller
    {
        // GET: Clinet/Categories

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        int user = _app.UI.CurrentUser.UserID;
        public ActionResult Index()
        {

            var cl = db.ItemsCategories.Where(x => x.SPID == user).ToList() ;
            return View(cl);
        }

        public ActionResult Create()
        {
            ItemsCategory cr = new ItemsCategory(true);


            return View(cr);
        }

        [HttpPost]
        public ActionResult Create(ItemsCategory itemsCategory)
        {
            if (ModelState.IsValid)
            {

                itemsCategory.SPID = user;
                db.ItemsCategories.Add(itemsCategory);
                db.SaveChanges();
                return RedirectToAction("index");


            }


            return View(itemsCategory);
        }

        public ActionResult Edit(int id)
        {

            var c = db.ItemsCategories.FirstOrDefault(x => x.CatID == id && x.SPID == user);
            return View(c);
        }
        [HttpPost]
        public ActionResult Edit(ItemsCategory itemsCategory)
        {

            var old = db.ItemsCategories.Find(itemsCategory.CatID);
            old.CatName = itemsCategory.CatName;
            old.Photo = _app.UI.Photos.replacePhoto(old.Photo, Request.Files);
            db.Entry(old).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("index");
        }

        public ActionResult Details(int id)
        {
            var c = db.ItemsCategories.FirstOrDefault(x => x.CatID == id && x.SPID == user);
            return View(c);
        }


        public ActionResult Tree()
        {

            var cl = db.ItemsCategories.Where(x => x.SPID == user).ToList();

            return View(cl);
        }

    }
}