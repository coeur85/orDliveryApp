﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Areas.Clinet.Controllers
{
    public class ItemPropertiesController : Controller
    {
        // GET: Clinet/ItemProperties
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult Index(int id)
        {

            // if (!isValied(id)) { return HttpNotFound(); }

            var it = db.Items.Find(id);
            var cl = db.ItemsCategories.Where(x => x.SPID == _app.UI.CurrentUser.UserID);
            if (!cl.Any(x => x.CatID == it.CatID)) { return HttpNotFound(); }

           // var prl = db.Properties.Where(x => x.ItemID == id);
            return View(it);
        }

        public ActionResult Create(int id)
        {
            Property pr = new Property { ItemID = id };
            pr.Item = db.Items.Find(id);
            return View(pr);
        }
        [HttpPost]
        public ActionResult Create(Property property)
        {
            if (ModelState.IsValid)
            {

                db.Properties.Add(property);
                db.SaveChanges();
                return RedirectToAction("index", new { id = property.ItemID });
            }

            return View(property);
        }


        public ActionResult Edit(int id)
        {
            if (!isValied(id)) { return HttpNotFound(); }
            var pr = db.Properties.Find(id);
            return View(pr);
        }

        [HttpPost]
        public ActionResult Edit(Property property)
        {
            var old = db.Properties.Find(property.PropertyID);

            old.IsMandatory = property.IsMandatory;
            old.PropertyName = property.PropertyName;
            db.Entry(old).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("index", new { id = property.ItemID });
        }


        public ActionResult Details(int id)
        {
            if (!isValied(id)) { return HttpNotFound(); }

            var pr = db.Properties.Find(id);

            return View(pr);
        }


        private bool isValied(int id)
        {

            var sp = _app.UI.CurrentUser.ServiceProvider;
            var prop = db.Properties.Find(id);
            var cl = db.ItemsCategories.Where(x => x.SPID == sp.SPID);

            if (cl.Any(x => prop.Item.CatID == x.CatID)) { return true; }
            else { return false; }




        }

    }
}