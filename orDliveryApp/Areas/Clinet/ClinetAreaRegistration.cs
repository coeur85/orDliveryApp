﻿using System.Web.Mvc;

namespace orDliveryApp.Areas.Clinet
{
    public class ClinetAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Clinet";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Clinet_default",
                "Clinet/{controller}/{action}/{id}",
                new { controller ="Home", action = "Index", id = UrlParameter.Optional }
            );

           
        }
    }
}