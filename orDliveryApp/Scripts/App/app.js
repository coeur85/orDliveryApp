﻿ 


$(document).ready(function () {
    initDT();
    initDatePicker();
});

        
function initDT() {

    $(".dataTable").dataTable(
        {
            "oLanguage":
                {

                    "sProcessing": "جارٍ التحميل...",
                    "sLengthMenu": "أظهر _MENU_ مدخلات",
                    "sZeroRecords": "لم يعثر على أية سجلات",
                    "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                    "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",

                    "sInfoPostFix": "",
                    "sSearch": "ابحث:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "الأول",
                        "sPrevious": "السابق",
                        "sNext": "التالي",
                        "sLast": "الأخير"

                    }
                },
            "ordering": false
        });



}

function initDatePicker()
{
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
}

        

        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('.timepicker').timepicker({
                defaultTIme: false,
            }).on("change", function (time) {
                console.log(time);
              //  var element = $(this), text;
                // get access to this Timepicker instance
              //  var timepicker = element.timepicker();
              //  console.log(time);
              //  $(element).val(e.format(time));
                var d = new Date(time['timeStamp'] * 1000);
                console.log(time['timeStamp']);
                console.log(d);
                var h = d.getHours() - 6;
                console.log('hourse:' + h);
               // $(this).val();


            });


            $('form').each(function (selindex) {
               // alert('index' + ":" + selindex);

                if ($(this).hasClass('searchform') == false)
                {

                    $(this).validate({
                        highlight: function (element) {
                            jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                        },
                        success: function (element) {
                            jQuery(element).closest('.form-group').removeClass('has-error');
                        },


                    });

                    return false;
                }
                 

            });



         

         
            
});



        $(document).ready(function () {
            $('#filePhoto').change(function () {
               console.log('int')
                var filepath = $(this).val();
                var ext = filepath.split('.');
                var fext = ext[ext.length - 1]
                if (fext == 'jpg' || fext == 'png' || fext == 'jpeg') {
                    var reader = new FileReader();

                    reader.readAsDataURL($(this).prop('files')[0]);
                    reader.onload = function (event) {
                        $('#U_Photo').attr('src', event.target.result);
                        $('#User_Photo').val(event.target.result);
                        $('#Photo').val(event.target.result);
                        console.log('done load');
                    };

                   // reader.onloadend = function () { $('#U_Photo').attr('src', null ); };
                }
                else { alert('Only .jpg , .png or .jpeg photo formats are allowed !'); $(this).val(''); }
            });



            $('#SPfilePhoto').change(function () {

                var filepath = $(this).val();
                var ext = filepath.split('.');
                var fext = ext[ext.length - 1]
                if (fext == 'jpg' || fext == 'png' || fext == 'jpeg') {
                    var reader = new FileReader();

                    reader.readAsDataURL($(this).prop('files')[0]);
                    reader.onload = function (event) {
                        $('#SPPhoto').attr('src', event.target.result);
                        $('#Photo').val(event.target.result);
                    };
                }
                else { alert('Only .jpg , .png or .jpeg photo formats are allowed !'); $(this).val(''); }
            });

            $('#Add_Photo').change(function () {

                var filepath = $(this).val();
                var ext = filepath.split('.');
                var fext = ext[ext.length - 1]
                if (fext == 'jpg' || fext == 'png' || fext == 'jpeg') {
                    var reader = new FileReader();

                    reader.readAsDataURL($(this).prop('files')[0]);
                    reader.onload = function (event) {
                        $('#add_Photo').attr('src', event.target.result);
                        $('#AddPhoto').val(event.target.result);
                    };
                }
                else { alert('Only .jpg , .png or .jpeg photo formats are allowed !'); $(this).val(''); }
            });


        });

function ShowMSg(msgText) {
    jQuery.gritter.add({
        title: 'تنــبــيـــه',
        text: msgText,
        class_name: 'growl-info',
        // image: '../images/screen.png',
        sticky: false,
        time: ''
    });

}



function ajaxPost(method,cls,json,suc)
{
    

    var url = document.location.origin + '/api/' + cls + '/' + method;

    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(json),
        contentType: "application/json;charset=utf-8",
        success: function (d) {
          suc(d)

        },
        error: function (x) {
            console.log(x);


        }
    });

}


function ajaxGet(method, cls, id, suc) {


    var url = document.location.origin +'/'+cls + '/' + method + '/' + id;
    console.log(url);

    $.ajax({
        url: url,
        type: 'GET',
       // data: JSON.stringify(json),
       // contentType: "application/json;charset=utf-8",
        success: function (d) {
            console.log('s');
            suc(d);

        },
        error: function (x,y,z) {
            console.log('error');
            console.log(x);
            console.log(y);
            console.log(z);


        }
    });

}

function ajaxGetJson(method, cls, json, suc) {


    var url = document.location.origin + '/' + cls + '/' + method  + '/?value=' + JSON.stringify(json) ;
    console.log(url);

    $.ajax({
        url: url,
        type: 'GET',
      //  data: JSON.stringify(json),
        contentType: "application/json;charset=utf-8",
        success: function (d) {
            console.log('s');
            suc(d);

        },
        error: function (x, y, z) {
            console.log('error');
            console.log(x);
            console.log(y);
            console.log(z);


        }
    });

}