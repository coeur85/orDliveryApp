﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
    public class SystemSetupController : Controller
    {
        // GET: SystemSetup

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult MobileAppWellcomeScreen()
        {
            var l = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.mobileWellcomeScreen).ToList();

            return View(l);
        }



        public ActionResult SupportPhone()
        {
            var phone = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.SupportPhone).FirstOrDefault();
            if (phone == null) { phone = new SystemSetup { Code = 2, Value = "55123456" }; }
            return View(phone);
        }

        [HttpPost]
        public ActionResult SupportPhone(SystemSetup Newphone)
        {
            var phone = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.SupportPhone).FirstOrDefault();
            if (phone == null) { phone = new SystemSetup { Code = 2 }; db.SystemSetups.Add(phone); }
            phone.Value = Newphone.Value;
            db.SaveChanges();
            _app.UI.msg.Show = ("تم تحديث رقم الدعم الفني");
            return View(phone);
        }

    }
}