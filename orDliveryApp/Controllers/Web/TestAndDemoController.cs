﻿using GoogleDistanceMatrix.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
   
    public class TestAndDemoController : Controller
    {
        // GET: TestAndDemo
        OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult Index()
        {

            //GoogleDistanceMatrixApi api = new GoogleDistanceMatrixApi(new[] { "31.1797691,29.9257256" },
            //    new[] { "31.1905631,29.9163486", "31.212058,29.9398614", "31.2301398,29.9522345" });
            //var response =  api.GetResponse();
            //ViewBag.response = Json(response, JsonRequestBehavior.AllowGet);
            return View();

        }


        public ActionResult DeleteMobileSession()
        {
          
            db.MobileSessions.RemoveRange(db.MobileSessions);
            //db.SaveChanges();

            var dl = db.Drivers.ToList();

            foreach (var d in dl)
            {
                d.Online = false;
            }
            var i = db.SaveChanges();
            ViewBag.i = i;
            return View();
        }


        public ActionResult RestCutomersPassword()
        {

            var cl = db.Customers;

            foreach (var c in cl)
            {
                c.User.LoginPassword = "123456";
            }
            var i = db.SaveChanges();
            ViewBag.i = i;

            return View();
        }
    }
}