﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
    public class MapsController : Controller
    {
        // GET: Maps
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DriverMapPath(int id )
        {
            int dvid = Convert.ToInt32(Request.QueryString["did"]);
            var drv = db.Drivers.FirstOrDefault(x => x.DriverID == dvid);
            var ord = db.Orders.FirstOrDefault(x => x.OrderID == id);

            _app.PageClass.DriverMapPath dmp = new _app.PageClass.DriverMapPath
            {
                Order = ord,
                Driver = drv,
                StartPoint = new _app.PageClass.GoogleLocation { lat = drv.lastKnownLocation.Lat, lng = drv.lastKnownLocation.Lng },
                EndPoint = new _app.PageClass.GoogleLocation { lat = Convert.ToDecimal( ord.CustomerAddress.Lat) , lng = Convert.ToDecimal(ord.CustomerAddress.Lng) },
                ServericeProvider = new _app.PageClass.GoogleLocation { lat = Convert.ToDecimal(ord.ServiceProvider.Lat), lng = Convert.ToDecimal(ord.ServiceProvider.Lng) }
            };

            var drvord = drv.Orders.Where(x => orDatabase.OrderStatusEnum.OpenOrders.Any(y => x.OrderStatusID == y)).ToList();
            if (drvord.Count == 0)
            {
                dmp.RoutList.Add(new _app.PageClass.TextValue
                {
                    Text = "موقع السائق / " + drv.User.fName + " الحالي",
                    URL = new _app.PageClass.ActionURL { Action = "Details", Contoller = "Drivers", Parametr = drv.DriverID }
                });
                dmp.IsDriverBusy = false;
            }
            else
            {
                var o = drvord.LastOrDefault();
                dmp.RoutList.Add(new _app.PageClass.TextValue
                {
                    Text = "تسليم الطلب رقم " + o.OrderID.ToString() ,
                    URL = new _app.PageClass.ActionURL { Action = "OrderDetails", Contoller = "orders", Parametr = o.OrderID }
                });
                dmp.IsDriverBusy = true;

            }

            
            dmp.RoutList.Add(new _app.PageClass.TextValue { Text = "بينات مقدم الخدمه /" + ord.ServiceProvider.SpName ,
                URL = new _app.PageClass.ActionURL { Action = "Details", Contoller = "ServiceProviders", Parametr = ord.SPID.Value } });
            dmp.RoutList.Add(new _app.PageClass.TextValue { Text = "بينات العميل/ " + ord.Customer.User.FullName,
                URL = new _app.PageClass.ActionURL { Action = "Details", Contoller= "Customer", Parametr = ord.CustomerID } });

            return PartialView("~/Views/Shared/Maps/_DriverPath.cshtml" , dmp);
        }


        public ActionResult DriversLocations()
        {
            return View();
        }

    }
}