﻿using orDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace orDliveryApp.Controllers
{
    public class NotificationsController : Controller
    {
        // GET: Notifications

         private orDatabase.OrDeliveryDBEntities db = new orDatabase.OrDeliveryDBEntities();
        public ActionResult OrderStatus6(int id)
        {

            var ord = _app.UI.NotiMethods.Orders(6).FirstOrDefault(x => x.OrderID == id);
            if (ord == null) {return null; }
            return PartialView("~/Views/Shared/Notifications/_NotiModel.cshtml", ord);
        }
        public ActionResult OrderStatus2(int id)
        {

            var ord = _app.UI.NotiMethods.Orders(2).FirstOrDefault(x => x.OrderID == id);
            if (ord == null) { return null; }
            return PartialView("~/Views/Shared/Notifications/_NotiModel.cshtml", ord);
        }

        public ActionResult newDriverLogin(int id)
        {
            var d = db.Drivers.Find(id);
            if (d == null) { return null; }
            return PartialView("~/Views/Shared/Notifications/_newDriverLogin.cshtml", d);
        }


        public ActionResult Index()
        {
            return View(db.Notifications.OrderByDescending(x=> x.NotificationID));
        }


        public ActionResult Create()
        {
            orDatabase.Notification noti = new orDatabase.Notification();
            return View(noti);
        }

        public ActionResult Details(int id)
        {

            return View(db.Notifications.Find(id));
        }

        [HttpPost]
        public ActionResult Create(orDatabase.Notification noti)
        {

            if (ModelState.IsValid)
            {
                _app.Send.Notifications not = new _app.Send.Notifications();
                Notification n = new Notification();

                noti.Photo = _app.UI.Photos.replacePhoto(n.Photo, Request.Files);
                db.Notifications.Add(noti);
                db.SaveChanges();

                //  var ms = db.MobileSessions.Select(x => x.MobileID).Distinct();

                if (noti.ToCustomers)
                {
                    var cl = db.Customers.ToList();
                    foreach (var c in cl)
                    {
                        not.SendNotification(c.User, noti);
                    }

                }

                if (noti.ToDrivers)
                {
                    var drl = db.Drivers.ToList();
                    foreach (var d in drl)
                    {
                        not.SendNotification(d.User, noti);
                    }

                }




                _app.UI.msg.Show = "تم ارسال التنبيه بنجاح";
                return RedirectToAction("Index");

            }

            return View(noti);
        }
    }
}