﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            if (_app.UI.CurrentUser.ServiceProvider != null) { return RedirectToAction("index", "Home", new { area = "Clinet" }); }

         //   if (_app.UI.CurrentUser.Administrator != null) { return View(); }



            OrDeliveryDBEntities db = new OrDeliveryDBEntities();

            _app.PageClass.AdminHomePage hp = new _app.PageClass.AdminHomePage();

            hp.DrAll = db.Drivers.Count();
            hp.DrOff = db.Drivers.Where(x => x.Online == false).Count();
            hp.DrOn = hp.DrAll - hp.DrOff;

            hp.CusAll = db.Customers.Count();
            hp.CusNew = db.Customers.Where(x => x.CodeDate.HasValue == true).ToList().Where(x => x.CodeDate.Value.AddDays(7) >= _app.DateTime.Now).Count();
            hp.CusOld = hp.CusAll - hp.CusNew;

            hp.OrdAll = db.Orders.Where(x=> x.OrderStatusID != 1).Count();
            hp.OrdOpen = db.Orders.Where(x => OrderStatusEnum.OpenOrders.Any(y => y == x.OrderStatusID) && x.OrderStatusID != 1).Count();
            hp.OrdClosed = hp.OrdAll - hp.OrdOpen;


            hp.AdAll = db.Adds.Count();
            hp.AdOn = db.Adds.Where(x => x.Active == true).Count();
            hp.AdOff = hp.AdAll - hp.AdOn;

            hp.ItemAll = db.Items.Count();
            hp.itemOff = db.Items.Where(x => x.Active == false).Count();
            hp.ItemOn = hp.ItemAll - hp.itemOff;


            hp.OpenOrders = db.Orders.Where(x => OrderStatusEnum.OpenOrders.Any(y => x.OrderStatusID == y && x.OrderStatusID != 1));

            // chart data
            var ordlist = db.Orders.Where(x => x.OrderStatusID == OrderStatusEnum.Competed).ToList().Where(x=>
            x.LastAudit.ActionDate.AddDays(30) >= _app.DateTime.Now).ToList();


            var chr = ordlist.GroupBy(x => x.LastAudit.ActionDate.Date).Select(x => new { value = x.Count(), date = (DateTime)x.Key }).ToList();


            hp.Chart1 = new _app.PageClass.ChartClass
            {
                 CanvesID = 1, ChartType = "line", text="الطلبات الناجحه في خلال 30 يوم", PointTilite = "طلب ",  label = "عدد الطلبات", xAexTitle = "التاريخ"
                , yAexTitle= "العدد", Color = "red",
                //Lables = (from x in ordlist select x.LastAudit.ActionDate.ToShortDateString()).ToArray(),
                Lables = chr.Select(x=> x.date.ToShortDateString()).ToArray(),
                 Data = chr.Select(x=> x.value.ToString()).ToArray()

            };


            var chr1 = ordlist.GroupBy(x => x.ServiceProvider.SpName).Select(x => new { value = x.Count(), spname = x.Key }).ToList();


            hp.Chart2 = new _app.PageClass.ChartClass
            {
                CanvesID = 2, ChartType = "bar", text = "عدد الطلبات علي حسب موفر الخدمه", PointTilite = "طلب", label = "عدد الطلبات", xAexTitle = "مقدمين الخدمات",
                yAexTitle = "عدد الطلبات", Lables = chr1.Select(x => x.spname).ToArray(), Data = chr1.Select(x => x.value.ToString()).ToArray(),
                Color="blue"


            };


            _app.Send.TextMessage text = new _app.Send.TextMessage();
            hp.MessageBlance =  text.GetBalance();

            return View(hp);
        }


        public ActionResult Search( string keyword)
        {
            _app.PageClass.Search search = new _app.PageClass.Search
            {

                KeyWord = keyword


            };
            OrDeliveryDBEntities db = new OrDeliveryDBEntities();

            if (string.IsNullOrEmpty(keyword)) { return View(search); }

            var splstr = keyword.Trim().Split(' ');



            foreach (var s in splstr)
            {

                if (s.Any(x => Char.IsLetter(x)))
                {
                    search.Users.AddRange(db.Users.Where(x => x.fName.Contains(s) || x.lName.Contains(s) ||
                    x.mName.Contains(s) || x.LoginName.Contains(s) && !search.Users.Any(c => x.UserID == c.UserID) && x.Administrator == null).ToList());

                }

                else

                {
                    search.Users.AddRange(db.Users.Where(x => x.Customer.PhoneNumber.Contains(s) || x.Driver.PhoneNumber.Contains(s) 
                    && !search.Users.Any(c => x.UserID == c.UserID)).ToList());

                    search.Orders.AddRange(db.Orders.Where(x => x.OrderID.ToString().Contains(s) && x.OrderStatusID != OrderStatusEnum.NewOrder
                    && !search.Orders.Any(y => y.OrderID == x.OrderID)).ToList());



                }

                
            }

            return View(search);
               
        }


        [AllowAnonymous]
        public ActionResult Wellcome()
        {
            return View();
        }

    }
}