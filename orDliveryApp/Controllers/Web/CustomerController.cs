﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;


namespace orDliveryApp.Controllers
{
    public class CustomerController : Controller
    {

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();



        public ActionResult Index()
        {
           
            return View(db.Customers);
        }


      


        public ActionResult Edit(int id)
        {

            var s = db.Customers.Find(id);
            return View(s);
        }
        [HttpPost]
        public ActionResult Edit(Customer cust)
        {
            var old = db.Customers.Find(cust.CustomerID);


            old.eMail = cust.eMail;
            
            old.User.Active = cust.User.Active;
            old.User.fName = cust.User.fName;
            old.User.mName = cust.User.mName;
            old.User.lName = cust.User.lName;
            old.User.Photo = _app.UI.Photos.replacePhoto(old.User.Photo, cust.User.Photo);
           


            db.Entry(old).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("index");
        }



        public ActionResult Details(int id)
        {
            var s = db.Customers.Find(id);
            return View(s);
        }


        public ActionResult Delete(int id)
        {
            var c = db.Customers.Find(id);
            _app.PageClass.DeleteCustomer cus = new _app.PageClass.DeleteCustomer();
            cus.customer = c;

            if (c.Orders.Count > 0 ) { cus.CanBeDeleted = false; }
            else { cus.CanBeDeleted = true; }

            return View(cus);
        }


        [HttpPost]
        public ActionResult Delete(_app.PageClass.DeleteCustomer cus)
        {
            var c = db.Customers.Find(cus.customer.CustomerID);
            if (c.Orders.Count > 0) { _app.UI.msg.Show = "لا يمكن الحذف"; return View(cus); }



            // c.CustomerAddresses.Clear();
            db.CustomerAddresses.RemoveRange(c.CustomerAddresses);
          
            c.ServiceProviders.Clear();



            // c.User.MobileSessions.Clear();
            db.MobileSessions.RemoveRange(c.User.MobileSessions);

            db.Users.Remove(c.User);
            db.Customers.Remove(c);
            db.SaveChanges();
            return RedirectToAction("index");
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}