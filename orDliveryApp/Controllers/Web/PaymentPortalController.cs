﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using orDatabase;


namespace orDliveryApp.Controllers
{
    [AllowAnonymous]
    public class PaymentPortalController : Controller
    {
        // GET: PaymentPortal

        private List<BankPayement> bankPayements() {
            return db.BankPayements.OrderByDescending(x => x.TransactionDate).Where(x=> 
            x.Order.SPID != null).ToList();
        }


        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ok(int id)
        {
            var p = db.BankPayements.Find(id);
            return View(p);
        }

        [AllowAnonymous]     
        public ActionResult error(int id)
        {
            var p = db.BankPayements.Find(id);
            return View(p);         
        }

        public ActionResult BankError(int id)
        {
            var ord = db.Orders.Find(id);
            var p = ord.BankPayements.ToList().OrderBy(x => x.TransactionDate).LastOrDefault();
            return RedirectToAction("error", p.LocalPayementID );
        }


        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Post)]
        [ActionName("ok")]
        public string ok_post(int id)
        {
           var p = ProcessPayement(id);
            if (p.PaymentStatusID == PayemntStatusEnum.Success) {
                    return "REDIRECT=https://ordeliverykw.com/PaymentPortal/ok/" + p.LocalPayementID.ToString();
            }
            else
            {
                return "REDIRECT=https://ordeliverykw.com/PaymentPortal/error/" + p.LocalPayementID.ToString();

            }
            
        }
        //[AllowAnonymous]
        //[AcceptVerbs(HttpVerbs.Post)]
        //[ActionName("ok")]
        //public ActionResult error_post(int id)
        //{
        //    var p = payement(id, false);
        //    return View(p);

        //}






        private BankPayement ProcessPayement(int id)
        {
            var ord = db.Orders.Find(id);
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string queryString = new StreamReader(req).ReadToEnd();

            var dict = HttpUtility.ParseQueryString(queryString);
            var json = new JavaScriptSerializer().Serialize(
                                dict.AllKeys.ToDictionary(k => k, k => dict[k])
                       );
            _app.WepApi.Paymnets.BankResponse model = Newtonsoft.Json.JsonConvert.DeserializeObject<_app.WepApi.Paymnets.BankResponse>(json);


            var p = ord.BankPayements.Where(x => x.BankPayementID == model.PaymentId).FirstOrDefault();
           
            

            if (model.result == "CAPTURED") {
                p.PaymentStatusID = PayemntStatusEnum.Success;
                _app.Audits.NewForStatus2(ord,1, "Auto subment due to successful online payment"); // check out the order
            }
            else { p.PaymentStatusID = PayemntStatusEnum.Failed; }



           


            p.ResultCode = model.result;
            p.AuthCode = model.Auth;
            p.ReferenceID = model.Ref;
            p.TransactionID = model.Tranid;
            p.TrackID = model.trackid;
            p.ResponseBody = json;
            p.UDFs = "eci=" + model.eci;
           




            db.SaveChanges();
            _app.Payments pay = new _app.Payments();
            pay.SendPaymentEmail(p);
            return db.BankPayements.Find(p.LocalPayementID); 

        }
        private string QueryStringData(string name)
        {
            if (Request.QueryString[name] != null) { return Request.QueryString[name]; }
            else return string.Empty;
        }

        public ActionResult index()
        {
            return View(bankPayements());
        }
        public ActionResult PaymentStatus(int id)
        {

            if (id == 1)
            {
                ViewBag.Title = "عمليات مقبوله";
                return View(bankPayements().Where(x => x.PaymentStatusID == PayemntStatusEnum.Success).ToList()); }

            else {
                ViewBag.Title = "عمليات مرفوضه";
                return View(bankPayements().Where(x => x.PaymentStatusID == PayemntStatusEnum.Failed).ToList());
            }
            

        }
        public ActionResult Details(int id)
        {
            var p = db.BankPayements.Find(id);
            return View(p);
        }

    }
}