﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
    public class AddsController : Controller
    {
        // GET: Adds
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult Index()
        {
            return View(db.Adds);
        }

        public ActionResult Create()
        {
            Add ad = new Add();

            ViewBag.SPCatID = new SelectList(db.ServiceProviderCategories, "CatID", "CatName");
            ViewBag.SPID = new SelectList(db.ServiceProviders.Where(x=> x.SPCat ==1), "SPID", "SPName");
            ViewBag.CatID = new SelectList(db.ItemsCategories.Where(x=> x.SPID ==1), "CatID", "CatName");
            return View(ad);
        }

        [HttpPost]
        public ActionResult Create(Add ad)
        {
            if (ModelState.IsValid)
            {

                ad.DateCreated = _app.DateTime.Now;
                db.Adds.Add(ad);
                db.SaveChanges();
                return RedirectToAction("index");
            }

            return View();
        }


        public ActionResult Edit(int id)
        {
            var ad = db.Adds.FirstOrDefault(x => x.AddID == id);

            ViewBag.SPCatID = new SelectList(db.ServiceProviderCategories, "CatID", "CatName",ad.ItemsCategory.ServiceProvider.SPCat);
            ViewBag.SPID = new SelectList(db.ServiceProviders.Where(x => x.SPCat == ad.ItemsCategory.ServiceProvider.SPCat),
                "SPID", "SPName", ad.ItemsCategory.SPID);
            ViewBag.CatID = new SelectList(db.ItemsCategories.Where(x => x.SPID == ad.ItemsCategory.SPID),
                "CatID", "CatName", ad.CatID);
            return View(ad);
        }

        [HttpPost]
        public ActionResult Edit(Add ad)
        {
            if (ModelState.IsValid)
            {
            var old = db.Adds.FirstOrDefault(x => x.AddID == ad.AddID);
            old.Active = ad.Active;
            old.AddPhoto = _app.UI.Photos.replacePhoto(old.AddPhoto, ad.AddPhoto);
            old.CatID = ad.CatID;
            db.Entry(old).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("index");

            }
            return View(ad);
           
        }


        public ActionResult Details(int id)
        {
            var ad = db.Adds.Find(id);
            return View(ad);
        }



        public ActionResult GetSP()
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);
            List<_app.PageClass.DropDownItem> list = new List<_app.PageClass.DropDownItem>();
            list = (from x in db.ServiceProviders.Where(y => y.SPCat == id)
                    select new _app.PageClass.DropDownItem { Text = x.SpName, Value = x.SPID }).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCat()
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);

            List<_app.PageClass.DropDownItem> list = new List<_app.PageClass.DropDownItem>();
            list = (from x in db.ItemsCategories.Where(y => y.SPID == id)
                    select new _app.PageClass.DropDownItem { Text = x.CatName, Value = x.CatID }).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);

        }


    }
}