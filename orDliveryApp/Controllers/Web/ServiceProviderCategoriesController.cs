﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
    public class ServiceProviderCategoriesController : Controller
    {
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();


        public ActionResult index()
        {

            ServiceProviderCategory sc = new ServiceProviderCategory();

            


            return View(db.ServiceProviderCategories);
        }


        public ActionResult Create()
        {

            ServiceProviderCategory sp = new ServiceProviderCategory(true);

            return View(sp);
        }
        [HttpPost]
        public ActionResult Create(ServiceProviderCategory spc)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    ServiceProviderCategory c = new ServiceProviderCategory(true);

                    spc.Photo = _app.UI.Photos.replacePhoto(c.Photo, Request.Files);
                    db.ServiceProviderCategories.Add(spc);
                    db.SaveChanges();
                    return RedirectToAction("index");
                }
            }
            catch (OrException e)
            {
                _app.UI.msg.Show = e.ErrorMessage;

            }

            


            return View(spc);
        }


        public ActionResult Delete(int id)
        {

            var spc = db.ServiceProviderCategories.Find(id);
            _app.PageClass.DeleteSPC p = new _app.PageClass.DeleteSPC();
            p.ServiceProviderCategory = spc;
            if (spc.ServiceProviders.Count > 0)
            { p.CanBeDeleted = false; }
            else { p.CanBeDeleted = true; }
            return View(p);
        }
        [HttpPost]
        public ActionResult Delete(_app.PageClass.DeleteSPC model)
        {

            if (ModelState.IsValid)
            {
                var spc = db.ServiceProviderCategories.Find(model.ServiceProviderCategory.CatID);
                db.ServiceProviderCategories.Remove(spc);  db.SaveChanges();
                return RedirectToAction("index");
            }

           

            return View(model);
        }


        public ActionResult Details(int id)
        {

            var s = db.ServiceProviderCategories.Find(id);
            return View(s);
        }


        public ActionResult Edit(int id)
        {
            var s = db.ServiceProviderCategories.Find(id);
            return View(s);
        }

        [HttpPost]
        public ActionResult Edit(ServiceProviderCategory spc)
        {

            try
            {
                var old = db.ServiceProviderCategories.Find(spc.CatID);
                old.Active = spc.Active;
                old.CatName = spc.CatName;
                old.Photo = _app.UI.Photos.replacePhoto(spc.Photo, Request.Files);
                db.Entry(old).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("index");
            }
            catch (OrException e)
            {
                _app.UI.msg.Show = e.ErrorMessage;

            }

            return View(spc);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
