﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _app;
using orDatabase;

namespace orDliveryApp.Controllers
{
    public class DriversController : Controller
    {
        // GET: Drivers

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();


        public ActionResult Index()
        {
            return View( db.Drivers);
        }

        public ActionResult Create()
        {
            Driver d = new Driver { User = new orDatabase.User(true) };

            return View(d);
        }
        [HttpPost]
        public ActionResult Create(Driver d)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    d.User.LoginName = d.PhoneNumber;
                    d.Online = false;
                    db.Drivers.Add(d);
                    db.SaveChanges();
                    return RedirectToAction("index");

                }
            }
            catch (OrException e)
            {
                _app.UI.msg.Show = e.ErrorMessage;
                
            }
           


            return View(d);
        }

        public ActionResult Edit(int id)
        {
            var d = db.Drivers.Find(id);
            return View(d);
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(Driver d)
        {


            try
            {
                if (ModelState.IsValid)
                {

                    var old = db.Drivers.Find(d.DriverID);
                    old.BicycleModel = d.BicycleModel;
                    old.LicenseNumber = d.LicenseNumber;
                    old.PhoneNumber = d.PhoneNumber;
                    old.User.Active = d.User.Active;
                    old.User.fName = d.User.fName;
                    old.User.lName = d.User.lName;
                    old.User.LoginName = d.PhoneNumber;
                    old.User.Photo = _app.UI.Photos.replacePhoto(old.User.Photo, d.User.Photo);
                    old.User.mName = d.User.mName;

                    db.Entry(old).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("index");

                }
            }
            catch (OrException e)
            {
                _app.UI.msg.Show = e.ErrorMessage;

            }
           

            return View(d);
        }

        public ActionResult Details(int id)
        {

            var d = db.Drivers.Find(id);
            return View(d);
        }

        public ActionResult Delete(int id)
        {
            var d = db.Drivers.Find(id);
            _app.PageClass.DeleteDriver dp = new _app.PageClass.DeleteDriver();
            dp.Driver = d;
            if (d.Orders.Count > 0) { dp.CanBeDeleted = false; }
            else { dp.CanBeDeleted = true; }
            return View(dp);
        }
        [HttpPost]
        public ActionResult Delete(_app.PageClass.DeleteDriver  dp)
        {
            var d = db.Drivers.Find(dp.Driver.DriverID);

            if (d.Orders.Count == 0)
            {
                db.Drivers.Remove(d);
                db.SaveChanges();
                return RedirectToAction("index");
            }

            return View(dp);
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "RestPassword")]
        public ActionResult RestPassword(Driver drv)
        {
            var d = db.Drivers.FirstOrDefault(x => x.DriverID == drv.DriverID);
            d.User.LoginPassword = "123456789";
            db.SaveChanges();
            _app.UI.msg.Show = ("كلمه المرور الجديده : " + d.User.LoginPassword);
            return RedirectToAction("Edit", new { id = drv.DriverID });
        }
    }
}