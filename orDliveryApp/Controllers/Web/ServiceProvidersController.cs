﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
    public class ServiceProvidersController : Controller
    {
        // GET: ServiceProviders
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();



        public ActionResult Index()
        {

            //IHubContext adminHub = GlobalHost.ConnectionManager.GetHubContext<AdminHub>();
            //adminHub.Clients.All.message("index was called");

            return View(db.ServiceProviders);
        }
        public ActionResult Create()
        {
            ServiceProvider sp = new ServiceProvider(true);
            ViewBag.SPCat = new SelectList(db.ServiceProviderCategories, "CatID", "CatName");
            return View(sp);
        }
        [HttpPost]
        public ActionResult Create(ServiceProvider srp )
        {
            ServiceProvider sp = new ServiceProvider(true);
            sp.Photo = _app.UI.Photos.replacePhoto(sp.Photo, srp.Photo );
            sp.User.Photo = _app.UI.Photos.replacePhoto(sp.User.Photo, srp.User.Photo );

            sp.Lat = Convert.ToString( Request.Form["gmap0-t"]);
            sp.Lng = Convert.ToString(Request.Form["gmap0-g"]);

            sp.Address = srp.Address;
            sp.DeliveryCharges = srp.DeliveryCharges;
            sp.MinOrder = srp.MinOrder;
            sp.SPCat = srp.SPCat;
            sp.SpName = srp.SpName;
            sp.WorkingTill = srp.WorkingTill;
            sp.WrokingFrom = srp.WrokingFrom;
            sp.Notes = srp.Notes;
            sp.DeliveryTimeframe = srp.DeliveryTimeframe;
            sp.ProfitShare = srp.ProfitShare;

            sp.User.Active = srp.User.Active;
            sp.User.fName = srp.User.fName;
            sp.User.lName = srp.User.lName;
            sp.User.LoginName = srp.User.LoginName;
            sp.User.LoginPassword = srp.User.LoginPassword;
            sp.User.mName = srp.User.mName;
            


            db.ServiceProviders.Add(sp);
            db.SaveChanges();
             return RedirectToAction("index");


        }
        public ActionResult Edit(int id)
        {

            var s = db.ServiceProviders.Find(id);
            ViewBag.SPCat = new SelectList(db.ServiceProviderCategories, "CatID", "CatName", s.SPCat);
            return View(s);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit(ServiceProvider sp)
        {
            var old = db.ServiceProviders.Find(sp.SPID);
          
            old.Address = sp.Address;
            old.DeliveryCharges = sp.DeliveryCharges;
            //old.Lat = sp.Lat;
            //old.Lng = sp.Lng;
            old.MinOrder =  sp.MinOrder;
            old.Photo = _app.UI.Photos.replacePhoto(old.Photo, sp.Photo);
            old.SPCat = sp.SPCat;
            old.SpName = sp.SpName;
            old.WorkingTill = sp.WorkingTill;
            old.WrokingFrom = sp.WrokingFrom;
            old.DeliveryTimeframe = sp.DeliveryTimeframe;
            old.Notes = sp.Notes;
            old.ProfitShare = sp.ProfitShare;

            old.User.Active = sp.User.Active;
            old.User.fName = sp.User.fName;
            old.User.mName = sp.User.mName;
            old.User.lName = sp.User.lName;

            old.User.LoginName = sp.User.LoginName;
            old.User.Photo = _app.UI.Photos.replacePhoto(old.User.Photo, sp.User.Photo);

            old.Lat = Convert.ToString(Request.Form["gmap" +old.SPID.ToString() + "-t"]);
            old.Lng = Convert.ToString(Request.Form["gmap" +old.SPID.ToString() + "-g"]);


            db.Entry(old).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("index");
        }
        public ActionResult Details(int id)
        {
            var s = db.ServiceProviders.Find(id);
            return View(s);
        }
        public ActionResult Delete(int id)
        {
            var s = db.ServiceProviders.Find(id);
            _app.PageClass.DeleteSP sp = new _app.PageClass.DeleteSP();
            sp.ServiceProvider = s;

            if (s.Orders.Count > 0 || s.ItemsCategories.Count > 0) { sp.CanBeDeleted = false; }
            else { sp.CanBeDeleted = true; }

         return View(sp);
        }
        [HttpPost]
        public ActionResult Delete(_app.PageClass.DeleteSP sp)
        {
            var s = db.ServiceProviders.Find(sp.ServiceProvider.SPID);
            db.Users.Remove(s.User);
            db.ServiceProviders.Remove(s);
            db.SaveChanges();
            return RedirectToAction("index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [HttpPost]
        [MultipleButton(Name = "action", Argument = "RestPassword")]
        public ActionResult RestPassword(ServiceProvider srp)
        {
            var srvProv = db.ServiceProviders.FirstOrDefault(x => x.SPID == srp.SPID);
            srvProv.User.LoginPassword = "123456789";
            db.SaveChanges();
            _app.UI.msg.Show = ("كلمه المرور الجديده : " + srvProv.User.LoginPassword);
            return RedirectToAction("Edit", new { id = srp.SPID });
        }
    }
}