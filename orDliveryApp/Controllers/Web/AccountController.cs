﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace orDliveryApp.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }


       
        public ActionResult SingOut()
        {
            _app.UI.CurrentUser = null;

            return RedirectToAction("index", "Login");
        }
    }
}