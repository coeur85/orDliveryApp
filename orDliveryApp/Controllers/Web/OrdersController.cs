﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
    public class OrdersController : Controller
    {
        // GET: OrderActions

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        private List<Order> AllOrders()
        {
            return db.Orders.Where(x=> x.OrderStatusID != 1).OrderByDescending(x => x.OrderDate).ToList();
        }

        public ActionResult OrderDetails(int id )
        {
            return View(AllOrders().FirstOrDefault(x=> x.OrderID == id));
        }
        
        public ActionResult FilterByStatus(int id)
        {

           
            if (id == OrderStatusEnum.Competed)
            { ViewBag.completeControl = true; }
            if (id == OrderStatusEnum.DriverDeclined)
            {
                ViewBag.Title = "طلبات سبق رفضها من قبل السائقين";
                return View(AllOrders().Where(x => x.OrderAudits.Any(y => y.NewStatusID == OrderStatusEnum.DriverDeclined)).ToList()); }
            else
            {
                var stat = db.OrderStatues.FirstOrDefault(x => x.StatusID == id);
                ViewBag.Title = stat.StatusName;
                return View(AllOrders().Where(x=> x.OrderStatusID == id).ToList());
            }
            
        }
        public ActionResult index()
        {
            return View(AllOrders());
        }

        public ActionResult CustomerDetails(int id)
        {
            return RedirectToAction("Details", "Customer", new { id = id });
        }
        public ActionResult PickDriver(int id) 
        {
            var ord = db.Orders.FirstOrDefault(x=> x.OrderID == id);
            if (ord.OrderStatusID != orDatabase.OrderStatusEnum.OrderReadyWaiting4Delivery) { return HttpNotFound(); }

            var drv = db.Drivers.Where(x => x.Online == true).ToList();
            
            

            _app.PageClass.PickDriver PD = new _app.PageClass.PickDriver();
            PD.Order = ord;
            foreach (var d in drv)
            {

                _app.PageClass.driverDistance wd = new _app.PageClass.driverDistance();
                _app.PageClass.PickDriverSearchItem jsonSearch = new _app.PageClass.PickDriverSearchItem();
                wd.Driver = d;

                jsonSearch.name = d.User.FullName;
                jsonSearch.driverid = d.DriverID;
                if (d.lastKnownLocation != null)
                {
                    GoogleDistanceMatrix.Services.GoogleDistanceMatrixApi gdapi =
                    new GoogleDistanceMatrix.Services.GoogleDistanceMatrixApi(new[] { d.lastKnownLocation.Lat + "," + d.lastKnownLocation.Lng },
                    new[] { ord.ServiceProvider.Lat + "," + ord.ServiceProvider.Lng });

                    var   r = gdapi.GetResponse();
                    if (r.Rows.Count() > 0)
                    {
                        if (r.Rows[0].Elements[0].Status == "OK")
                        { wd.GoogleRespons = r.Rows[0].Elements[0]; }
                        else { wd.GoogleRespons = null; }
                    }
                    else { wd.GoogleRespons = null; }
                   

                }
                else { wd.GoogleRespons = null; }
                var drvord = d.Orders.ToList();
                var count = drvord.Where(x => OrderStatusEnum.OpenOrders.Any(y => x.OrderStatusID == y)).Count();
                if (count > 0)
                { wd.IsFree = false; jsonSearch.cssClass = "panel-danger"; }
                else { wd.IsFree = true; jsonSearch.cssClass = "panel-success"; }

                //PD.distances.Add(new _app.PageClass.driverDistance { DriverID = d.DriverID , Distance = r });
                PD.Drivers.Add(wd);
                PD.pickDriverSearchItems.Add(jsonSearch);
            }
            var nuldr = PD.Drivers.Where(x => x.GoogleRespons == null).ToList();
            var notnuldv = PD.Drivers.Where(x => x.GoogleRespons != null).ToList();

            PD.Drivers.Clear();
            PD.Drivers.AddRange(notnuldv.OrderBy(x => x.GoogleRespons.Distance.Value));
            PD.Drivers.AddRange(nuldr);

            return View(PD);
        }
        public ActionResult AssignOrder2Driver(int id)
        {
            var dbord = AllOrders().FirstOrDefault(x => x.OrderID == id);
            dbord.DriverID = Convert.ToInt32(Request.QueryString["did"]) ;
            _app.Audits.NewForStatus7(dbord, _app.UI.CurrentUser.UserID);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }
        //--------- order actions partial view

        public ActionResult DeclineOrder(Order order)
        {
            var dbord = AllOrders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus13(dbord, _app.UI.CurrentUser.UserID, order.DeclineReason);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }
        public ActionResult AcceptOrder(Order order)
        {
            var dbord = AllOrders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus4(dbord, _app.UI.CurrentUser.UserID);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }
        public ActionResult RequestDriver(Order order)
        {
            var dbord = AllOrders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus6(dbord, _app.UI.CurrentUser.UserID);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }
        public ActionResult PackagePickedUp(Order order)
        {

            var dbord = AllOrders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus10(dbord, _app.UI.CurrentUser.UserID);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });

        }
        public ActionResult DeliveryFailure(Order order)
        {
            var dbord = AllOrders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus12(dbord, _app.UI.CurrentUser.UserID, order.DeclineReason);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }
        public ActionResult DeliverySuccessful(Order order)
        {
            var dbord = AllOrders().FirstOrDefault(x => x.OrderID == order.OrderID);

            _app.Audits.NewForStatus11(dbord, _app.UI.CurrentUser.UserID);
            db.SaveChanges();
            return RedirectToAction("OrderDetails", new { id = dbord.OrderID });
        }

    }
}