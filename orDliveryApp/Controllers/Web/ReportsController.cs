﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Converters;
using orDatabase;
using Rotativa;

namespace orDliveryApp.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        private IEnumerable<Order> Orders()
        {
            return db.Orders.Where(x=> x.OrderStatusID != OrderStatusEnum.NewOrder)
                .OrderByDescending(x => x.OrderID);
        }
        private List<Order> Search(_app.PageClass.Reports.OrderSearch model)
        {

            var ordl = Orders();
            if (model.CustomerID > 0)
            { ordl = ordl.Where(x => x.CustomerID == model.CustomerID); }
            if (model.ServiceProviderID > 0)
            { ordl = ordl.Where(x => x.SPID == model.ServiceProviderID); }
            if (model.DriverID > 0)
            { ordl = ordl.Where(x => x.DriverID == model.DriverID); }
            
            if(model.From.Date.Date != new DateTime(1,1,1) && model.To.Date != new DateTime(1, 1, 1))
            { ordl = ordl.ToList().Where(x => x.OrderDate.Date >= model.From.Date 
            && x.OrderDate.Date <= model.To.Date); }

            return ordl.ToList();
        }


        public ActionResult ByDriver()
        {
            var dl = (from x in db.Drivers.ToList().OrderBy(x=> x.User.FullName) select new { x.DriverID, x.User.FullName }).ToList();
            dl.Insert(0, new { DriverID = 0 , FullName = "الكل" });

            ViewBag.DriverID = new SelectList(
               dl
                , "DriverID", "FullName").ToList();
            return View(Orders().Where(x=> x.DriverID != null).ToList());
        }
        public ActionResult SearchDriver(int id)
        {
            var o = Search(new _app.PageClass.Reports.OrderSearch { DriverID = id });
            o = o.Where(x => x.DriverID != null).ToList();
            return PartialView("~/Views/Shared/Orders/_OrdersTableCompleted.cshtml", o);
        }
        public ActionResult ByCustomer()
        {
            var dl = (from x in db.Customers.ToList().Where(x=> !string.IsNullOrEmpty(x.User.FullName) &&
                      !string.IsNullOrWhiteSpace(x.User.FullName)).OrderBy(x => x.User.FullName).ToList()
                      select new { x.CustomerID, x.User.FullName }).ToList();
            dl.Insert(0, new { CustomerID = 0, FullName = "الكل" });

            ViewBag.CustomerID = new SelectList(
               dl
                , "CustomerID", "FullName").ToList();
            return View(Orders().ToList());
        }
        public ActionResult SearchCustomer(int id)
        {
            var o = Search(new _app.PageClass.Reports.OrderSearch { CustomerID = id });
            return PartialView("~/Views/Shared/Orders/_OrdersTable.cshtml", o);
        }
        public ActionResult BySP()
        {
            var dl = (from x in db.ServiceProviders.ToList().OrderBy(x => x.SpName).ToList()
                      select new { x.SPID, SpName = (x.SpName + " -- " + x.ServiceProviderCategory.CatName) }).ToList();
            dl.Insert(0, new { SPID = 0, SpName = "الكل" });

            ViewBag.SPID = new SelectList(
               dl
                , "SPID", "SpName").ToList();
            return View(Orders().ToList());
        }
        public ActionResult SearchSP(int id)
        {
            var o = Search(new _app.PageClass.Reports.OrderSearch { ServiceProviderID = id });
            return PartialView("~/Views/Shared/Orders/_OrdersTable.cshtml", o);
        }
        public ActionResult Completed()
        {

            var dl = (from x in db.ServiceProviders.ToList().OrderBy(x => x.SpName).ToList()
                      select new { x.SPID, SpName = (x.SpName + " -- " + x.ServiceProviderCategory.CatName) }).ToList();
          //  dl.Insert(0, new { SPID = 0, SpName = "الكل" });



            ViewBag.SPID = new SelectList(
             dl , "SPID", "SpName").ToList();

            _app.PageClass.Reports.Complted page = new _app.PageClass.Reports.Complted
            { from = _app.DateTime.Now.AddDays(-30), to = _app.DateTime.Now };

            var o = Search(new _app.PageClass.Reports.OrderSearch { From = page.from , To = page.to });
            page.Orders = o.Where(x=> x.OrderStatusID == OrderStatusEnum.Competed && x.SPID ==dl.FirstOrDefault().SPID).ToList(); ;
            return View(page);
        }      
        public ActionResult CompleteReport(string value)
        {
            var model = StringJson(value);
            var o = Search(model);
            o = o.Where(x => x.OrderStatusID == orDatabase.OrderStatusEnum.Competed).ToList();
            return    PartialView("~/Views/Shared/Orders/_OrdersCompletedReport.cshtml",o  );
        }
        public ActionResult Print(string value)
        {
            return new ActionAsPdf("OrderPrintReport", new { value = value }) { FileName = "PDF_File.pdf" };
           // return RedirectToAction("OrderPrintReport", new { value = value }); //OrderPrintReport(value);
        }
        [AllowAnonymous]
        public ActionResult OrderPrintReport(string value)
        {
            var model = StringJson(value);
            model.ServiceProvider = db.ServiceProviders.Find(model.ServiceProviderID);
            _app.PageClass.Reports.ServiceProviderPrintReport sp = new _app.PageClass.Reports.ServiceProviderPrintReport();
            sp.SearchParamters = model;
          //  sp.UserName = _app.UI.CurrentUser.FullName;
            sp.Orders = Search(model).Where(x => x.OrderStatusID == OrderStatusEnum.Competed).ToList();
            return View(sp);
        }


        private _app.PageClass.Reports.OrderSearch StringJson(string value)
        {
            var format = "dd/mm/yyyy"; // your datetime format
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
            _app.PageClass.Reports.OrderSearch model = Newtonsoft.Json.JsonConvert.DeserializeObject<_app.PageClass.Reports.OrderSearch>(value, dateTimeConverter);

            var far = model.Fromstr.Split('/');
            model.From = new DateTime(Convert.ToInt32(far[2]), Convert.ToInt32(far[1]), Convert.ToInt32(far[0]));
            var tar = model.Tostr.Split('/');
            model.To = new DateTime(Convert.ToInt32(tar[2]), Convert.ToInt32(tar[1]), Convert.ToInt32(tar[0]));

            //var o = Search(model);
            //o = o.Where(x => x.OrderStatusID == orDatabase.OrderStatusEnum.Competed).ToList();

            return model;
        }
    }
}