﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using orDatabase;

namespace orDliveryApp.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        // GET: Login

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        public ActionResult Index()
        {

            _app.PageClass.Login l = new _app.PageClass.Login();

            return View(l);
        }


        [HttpPost]
        public ActionResult index(_app.PageClass.Login model)
        {


            var u = db.Users.FirstOrDefault(x => x.LoginName == model.UserName && x.LoginPassword == model.Password && x.Active == true);

            if (u != null) { _app.UI.CurrentUser = u; return RedirectToAction("index", "Home"); }

            model.Message = "اسم المسنخدم او كلمه المرور غير صحيحه";
            



            return View(model);
        }

    }
}