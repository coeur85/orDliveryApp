﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using orDatabase;

namespace orDliveryApp.Controllers.API.DriverApp
{
    public class DriverAccountController : ApiController
    {


        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        _app.Validation v = new _app.Validation();
        _app.WepApi.Methods m = new _app.WepApi.Methods();
        _app.WepApi.Response.Ok ok = new _app.WepApi.Response.Ok();

        public IHttpActionResult SingIn(_app.WepApi.Account.Singin model)
        {

            //var d = db.Drivers.FirstOrDefault(x => x.User.LoginName == model.LoginID && x.User.LoginPassword == model.Password);
            var d = db.Drivers.FirstOrDefault(x => x.PhoneNumber == model.LoginID && x.User.LoginPassword == model.Password);
            var ex = v.isDriver(d); if (ex.HasErrors()) { return Ok(ex); }


            if (d.User.Active == false) { v.NewEx("حساب غير مفعل", _app.WepApi.Response.ErrorType.Account); return Ok(ex); }

            if (model.Location == null) { ex = v.NewEx("pleae add the location JSON , review the postman enviroment!", _app.WepApi.Response.ErrorType.Developer); }
            if (model.Location.Lat == 0 || model.Location.Lng == 0)
            { ex = v.NewEx("الموقع غير معروف برجاء تشغيل خدمه المواقع", _app.WepApi.Response.ErrorType.Account); }
            if (ex.HasErrors()) { return Ok(ex); }
            ok = new _app.WepApi.Response.Ok();

            var session = m.LogUserIn(d.User, model.DeviceID);
            d.DriverLocations.Add(new DriverLocation { DriverID = d.DriverID , SyncDate = _app.DateTime.Now ,
                Lat = model.Location.Lat , Lng= model.Location.Lng });
            db.SaveChanges();
            AdminHub hub = new AdminHub();
            hub.newDriverLogin(d.DriverID);
            ok.Data = session;
            return Ok(ok);

        }
        public IHttpActionResult ResetPassword(_app.WepApi.Account.Singin model)
        {


            var d = db.Drivers.FirstOrDefault(x => x.User.LoginName == model.LoginID);
            var ex = v.isDriver(d);
            if (ex.HasErrors()) { return Ok(ex); }

            Random r = new Random();
            var p = r.Next(000000, 999999);
            d.User.LoginPassword = p.ToString();

            db.SaveChanges();
            ok = new _app.WepApi.Response.Ok();
            ok.Messages = "تم رسالة بكلمه المرور";
            ok.Data = "password is :" + d.User.LoginPassword.ToString();
            return Ok(ok);
        }
        public IHttpActionResult MyProfile(_app.WepApi.Session model)
        {

            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var drv = ms.User.Driver;
            _app.WepApi.Response.Ok ok = new _app.WepApi.Response.Ok();
            var profile = _app.WepApi.Convert.DB2Json.Profile(drv);
            ok.Data = profile;
            return Ok(ok);



        }
        public IHttpActionResult LocationSync(_app.WepApi.Account.LocationSync model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var drv = ms.User.Driver;


            var currentorder = drv.Orders.ToList().OrderByDescending(x => x.LastAudit.ActionDate).
                FirstOrDefault(x => OrderStatusEnum.DriverOnDuty.Any(y=> y == x.OrderStatusID));
            int? i = null;
            if (currentorder != null) { i = currentorder.OrderID; }
            drv.DriverLocations.Add( new DriverLocation
            {
                 DriverID = drv.DriverID,
                 Lat = model.Lat,
                 Lng = model.Lng,
                 SyncDate = _app.DateTime.Now,
                 OrderID = i
            });
            db.SaveChanges();
            ok = new _app.WepApi.Response.Ok { Messages = "saved !" };
            

            return Ok(ok);
        }



        public IHttpActionResult UpdateProfile(_app.WepApi.Account.UpdateProfileInfo model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            ex = v.UpdateDriverProfile(model);
            if (ex.HasErrors()) { return Ok(ex); }

            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
          
          
            var drv = ms.User.Driver;

            if (drv.User.LoginPassword.Trim() != model.userProfile.Password.Trim())
            { ex = v.NewEx("كلمه مرور غير مطابقه ، برجاء ادخال كلمه المرور السليمه من اجل تخديث البينات",
                _app.WepApi.Response.ErrorType.Password);
                return Ok(ex);
            }

            drv.User.fName = model.userProfile.fName;
            drv.User.mName = model.userProfile.mName;
            drv.User.lName = model.userProfile.lName;
            drv.BicycleModel = model.userProfile.BicycleModel;
            drv.LicenseNumber = model.userProfile.LicenseNumber;
            drv.PhoneNumber = model.userProfile.PhoneNumber;
            
            drv.User.Photo = _app.UI.Photos.replacePhoto(drv.User.Photo, model.userProfile.Photo);
           // drv.User.LoginPassword = model.userProfile.Password;
            db.SaveChanges();

            //var sid = m.LogUserIn(cus.User, model.login.DeviceID);
            _app.WepApi.Response.Ok o = new _app.WepApi.Response.Ok();
            //o.Data = sid;
            o.Messages = "تم تحديث بينات حسابك";
            return Ok(o);
        }
        public IHttpActionResult UpdatePassword(_app.WepApi.Account.UpdatePassword model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            v.Password(model.Password);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var d = ms.User.Driver;
            d.User.LoginPassword = model.Password;
            db.SaveChanges();

            _app.WepApi.Response.Ok o = new _app.WepApi.Response.Ok();
            o.Messages = "تم تحديث كلمه المرور";
            return Ok(o);

        }


        public IHttpActionResult SingOut(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            m.logUserOut(ms);
            ok = new _app.WepApi.Response.Ok { Messages = "تم تسجيل الخروج بنجاح" };
            return Ok(ok);


        }
    }
}
