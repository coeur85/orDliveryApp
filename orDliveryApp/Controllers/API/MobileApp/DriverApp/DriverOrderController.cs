﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using orDatabase;

namespace orDliveryApp.Controllers.API.DriverApp
{
    public class DriverOrderController : ApiController
    {


        OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        _app.Validation v = new _app.Validation();
        _app.WepApi.Response.Ok OkResponse = new _app.WepApi.Response.Ok();



        public IHttpActionResult MyOrders(_app.WepApi.Order.MyOrders model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var driv = ms.User.Driver;

            List<orDatabase.Order> drdord = new List<Order>();

            switch (model.OrderStatus)
            {

                case 1:
                    drdord = driv.Orders.Where(x => x.OrderStatusID == orDatabase.OrderStatusEnum.Sent2Driver).ToList();
                    break;

                case 2:
                    drdord = driv.Orders.Where(x => x.OrderStatusID == orDatabase.OrderStatusEnum.DriverAcceptedHeading2Sp ||
                    x.OrderStatusID == orDatabase.OrderStatusEnum.DriverGotOrderHeading2Customer ).ToList();
                    break;
                case 3:
                    drdord = driv.Orders.Where(x => x.OrderStatusID == orDatabase.OrderStatusEnum.Competed ||
                    x.OrderStatusID == orDatabase.OrderStatusEnum.FailedToDeliver || x.OrderStatusID == orDatabase.OrderStatusEnum.CancelledByAdmin).ToList();
                    break;
                case 0:
                    drdord = driv.Orders.ToList();
                    break;
                default:
                    break;
            }

              var ord = _app.WepApi.Convert.DB2Json.Orders(drdord);

            OkResponse = new _app.WepApi.Response.Ok { Data = ord };
            return Ok(OkResponse);
        }
        public IHttpActionResult OrderDetails(_app.WepApi.Order.orderDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var drv = ms.User.Driver;

            var myorder = drv.Orders.FirstOrDefault(x => x.OrderID == model.OrderID);


            var ord = _app.WepApi.Convert.DB2Json.Order(myorder);

            //_app.WepApi.Order.Sp sp = new _app.WepApi.Order.Sp
            //{
            //     Address = myorder.ServiceProvider.Address,
            //     Lat = myorder.ServiceProvider.Lat,
            //     Lng = myorder.ServiceProvider.Lng,
            //     Photo = _app.UI.Photos.AbsoluteUrl(myorder.ServiceProvider.Photo),
            //     SPID = myorder.SPID.Value,
            //     SpName = myorder.ServiceProvider.SpName,
            //     WorkingTill = myorder.ServiceProvider.WorkingTill,
            //     WrokingFrom = myorder.ServiceProvider.WrokingFrom
            //};

            var sp = _app.WepApi.Convert.DB2Json.ServiceProvider(myorder.ServiceProvider);
            var cus = _app.WepApi.Convert.DB2Json.Profile(myorder.Customer);
            var add = _app.WepApi.Convert.DB2Json.Address(myorder.CustomerAddress);
            _app.WepApi.Order.DriverOrderDetails drvord = new _app.WepApi.Order.DriverOrderDetails
            {
                 Order = ord,
                 SP = sp,
                 Customer = cus,
                 CustomerAddress = add
            };



            OkResponse = new _app.WepApi.Response.Ok { Data = drvord };
            return Ok(OkResponse);
        }


        // --- order actions ----

        public IHttpActionResult AccepteOrder(_app.WepApi.Order.orderDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var drv = ms.User.Driver;

            var myorder = drv.Orders.FirstOrDefault(x => x.OrderID == model.OrderID);

            if (myorder.OrderStatusID != OrderStatusEnum.Sent2Driver)
            {
                ex = v.NewEx("لا يمكن قبول الطلب لان حالة الطلب الحاليه "+ myorder.OrderStatue.StatusName
                    , _app.WepApi.Response.ErrorType.Order);
                return Ok(ex);

            }

            _app.Audits.NewForStatus8(myorder, drv.DriverID);
            db.SaveChanges();
            OkResponse = new _app.WepApi.Response.Ok { Messages= "تم العمليه بنجاح" };
            return Ok(OkResponse);

        }
        public IHttpActionResult DeclineOrder(_app.WepApi.Order.orderDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var drv = ms.User.Driver;

            var myorder = drv.Orders.FirstOrDefault(x => x.OrderID == model.OrderID);

            if (myorder.OrderStatusID != OrderStatusEnum.Sent2Driver)
            {
                ex = v.NewEx("لا يمكن قبول الطلب لان حالة الطلب الحاليه " + myorder.OrderStatue.StatusName
                    , _app.WepApi.Response.ErrorType.Order);
                return Ok(ex);
            }

            if (string.IsNullOrEmpty(model.Notes))
            {
                ex = v.NewEx("لابد من توفير سبب لرفض الطلب", _app.WepApi.Response.ErrorType.Order);
                return Ok(ex);
            }

            _app.Audits.NewForStatus9(myorder, drv.DriverID, model.Notes);
            db.SaveChanges();
            OkResponse = new _app.WepApi.Response.Ok { Messages = "تم العمليه بنجاح " };
            return Ok(OkResponse);

        }
        public IHttpActionResult PackagePickedUp(_app.WepApi.Order.orderDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var drv = ms.User.Driver;

            var myorder = drv.Orders.FirstOrDefault(x => x.OrderID == model.OrderID);

            if (myorder.OrderStatusID != OrderStatusEnum.DriverAcceptedHeading2Sp)
            {
                ex = v.NewEx("لا يمكن قبول الطلب لان حالة الطلب الحاليه " + myorder.OrderStatue.StatusName
                    , _app.WepApi.Response.ErrorType.Order);
                return Ok(ex);
            }

          

            _app.Audits.NewForStatus10(myorder, drv.DriverID);
            db.SaveChanges();
            OkResponse = new _app.WepApi.Response.Ok { Messages = "تم العمليه بنجاح" };
            return Ok(OkResponse);

        }
        public IHttpActionResult DeliverySuccessful(_app.WepApi.Order.orderDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var drv = ms.User.Driver;
            var myorder = drv.Orders.FirstOrDefault(x => x.OrderID == model.OrderID);
            if (myorder.OrderStatusID != OrderStatusEnum.DriverGotOrderHeading2Customer)
            {
                ex = v.NewEx("لا يمكن تعديل الطلب لان حالة الطلب الحاليه " + myorder.OrderStatue.StatusName
                    , _app.WepApi.Response.ErrorType.Order);
                return Ok(ex);
            }          

            _app.Audits.NewForStatus11(myorder, drv.DriverID);
            db.SaveChanges();
            OkResponse = new _app.WepApi.Response.Ok { Messages = "تم العمليه بنجاح" };
            return Ok(OkResponse);

        }
        public IHttpActionResult DeliveryFailure(_app.WepApi.Order.orderDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var drv = ms.User.Driver;

            var myorder = drv.Orders.FirstOrDefault(x => x.OrderID == model.OrderID);

            if (myorder.OrderStatusID != OrderStatusEnum.DriverGotOrderHeading2Customer)
            {
                ex = v.NewEx("لا يمكن تعديل الطلب لان حالة الطلب الحاليه " + myorder.OrderStatue.StatusName
                    , _app.WepApi.Response.ErrorType.Order);
                return Ok(ex);
            }

            if (string.IsNullOrEmpty(model.Notes))
            {
                ex = v.NewEx("لابد من توفير سبب لرفض الطلب", _app.WepApi.Response.ErrorType.Order);
                return Ok(ex);
            }

            _app.Audits.NewForStatus12(myorder, drv.DriverID, model.Notes);
            db.SaveChanges();
            OkResponse = new _app.WepApi.Response.Ok { Messages = "saved !" };
            return Ok(OkResponse);

        }
        public IHttpActionResult RateCustomer(_app.WepApi.Order.RateOrder model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isDriver(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var ord = ms.User.Driver.Orders.FirstOrDefault(x => x.OrderID == model.OrderID);
            if (ord == null) { ex = v.NewEx("invalied order id", _app.WepApi.Response.ErrorType.Developer); return Ok(ex);  }

            ex = v.DriverRateCustomer(ord);
            if (ex.HasErrors()) { return Ok(ex); }


            ord.DriverRating = model.rating;
            ord.DriverComment = model.Comment;
            db.SaveChanges();

            OkResponse = new _app.WepApi.Response.Ok { Messages = "تم تقيم العميل بنجاح" };
            return Ok(OkResponse);
        }

    }
}
