﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using orDatabase;
using _app.WepApi.Order;
namespace orDliveryApp.Controllers.API.CustomerApp
{
    public class CustomerOrderController : ApiController
    {


        _app.WepApi.Methods m = new _app.WepApi.Methods();
        _app.Validation v = new _app.Validation();
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        _app.WepApi.Response.Ok OkResponse = new _app.WepApi.Response.Ok();
        private int ItemsPerPage = 10;

      
        private IHttpActionResult Returnlist(IEnumerable<object> l)
        {
            if (l.ToList().Count > 0)
            {
                OkResponse = new _app.WepApi.Response.Ok() { Data = l };
                return Ok(OkResponse);
            }
            else { var ex = v.NoDataFound();
                return Ok(ex);
            }

        }
        private IHttpActionResult Returnlist(IEnumerable<object> l , int max)
        {
            if (l.ToList().Count > 0)
            {
                
                OkResponse = new _app.WepApi.Response.Ok() { Data = new { itemList= l , MaxItemID = max } };
                return Ok(OkResponse);
            }
            else
            {
                var ex = v.NoDataFound();
                return Ok(ex);
            }

        }
        private void RemoveOI( Order order,int OIID)
        {
            var oi = order.OrderItems.FirstOrDefault(x => x.OIID == OIID);

            foreach (var v in oi.OIPropertyValues.ToList())
            {
                db.OIPropertyValues.Remove(v);
            }

            db.OrderItems.Remove(oi);


        }
        private List<Sp> ClosestServiceProviders(decimal lat, decimal lng, List<ServiceProvider> spl, string SessionID)
        {

            var json = _app.WepApi.Convert.DB2Json.ServiceProvider(spl);
            isSpFavorate(json, SessionID);
            if (lat != 0 && lng != 0)
            {

               
                List<string> spar = new List<string>();
                foreach (var j in json)
                {
                    spar.Add(j.Lat + "," + j.Lng);
                }

                GoogleDistanceMatrix.Services.GoogleDistanceMatrixApi api = new GoogleDistanceMatrix.Services.GoogleDistanceMatrixApi
                    (new [] { lat.ToString() + "," + lng.ToString() }, spar.ToArray());

                var res = api.GetResponse();

                if (res.Rows.Count() > 0)
                {
                      for (int i = 0; i < json.Count; i++)
                      {
                        if (res.Rows[0].Elements[i].Status == "OK")
                        {
                            json[i].Distance = res.Rows[0].Elements[i].Distance.Value;
                            json[i].Duration = res.Rows[0].Elements[i].Duration.Value;

                        }
                        else
                        {
                            json[i].Distance = null;
                            json[i].Duration = null;

                        
} 
                       }
                }
              


            }

            return json.OrderBy(x=> x.Distance == null).ThenBy(x=> x.Distance).Take(3).ToList();

        }
        private int basketQuantaty(Order order)
        {
            int basketitems = 0;
            foreach (var it in order.OrderItems)
            {
                basketitems += it.Quantity;
            }
            return basketitems;
        }
        private void isSpFavorate(List<Sp> spl, string SessionID)
        {
            var ex = v.SessionID(SessionID);
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == SessionID);
            if (ms != null)
            {
                ex = v.isCustomer(ms);
                if (!ex.HasErrors())
                {
                    var cspl = ms.User.Customer.ServiceProviders.ToList();

                    foreach (var sp in cspl)
                    {
                        //if (cspl.Any(x => x.SPID == sp.SPID))
                        //{ sp.isFavourite = true; }
                        if (spl.Any(x => x.SPID == sp.SPID))
                        {
                            spl.FirstOrDefault(x => x.SPID == sp.SPID).isFavourite = true;
                        }

                    }





                }
            }

        }

        #region GetMethods
       
        private IHttpActionResult GetSPCat(decimal lat, decimal lng, string SessionID)
        {

            List<_app.WepApi.Order.SPCategory> spc = (from x in db.ServiceProviderCategories.ToList()
                                                            where x.Active == true
                                                            select new
                                      _app.WepApi.Order.SPCategory
                                                            {
                                                                CatID = x.CatID,
                                                                CatName = x.CatName,
                                                                Photo = _app.UI.Photos.AbsoluteUrl(x.Photo),
                                                                ServiceProviders = ClosestServiceProviders(lat,lng,x.ServiceProviders.ToList(), SessionID)
                                                            }).ToList();
            return Returnlist(spc);

        }       
        private IHttpActionResult GetSp(int CatID, string SessionID)
        {


            //List<Sp> spl = (from x in db.ServiceProviders.Where(y=> y.SPCat == CatID).ToList()
            //                      where x.User.Active == true
            //                      select new Sp
            //                      {
            //                          Address = x.Address,
            //                          DeliveryCharges = x.DeliveryCharges,
            //                          MinOrder = x.MinOrder,
            //                          Photo = _app.UI.Photos.AbsoluteUrl(x.Photo),
            //                          SpName = x.SpName,
            //                          SPID = x.SPID,
            //                          WorkingTill = x.WorkingTill,
            //                          WrokingFrom = x.WrokingFrom,
            //                          isWorkingNow = isWorkinkNow(x.WrokingFrom, x.WorkingTill),
            //                          isFavourite =false
            //                      }).ToList();



            List<Sp> spl = _app.WepApi.Convert.DB2Json.ServiceProvider(db.ServiceProviders.Where(x => x.SPCat == CatID).ToList());


            isSpFavorate(spl, SessionID);
         



            return Returnlist(spl);



        }    
        private IHttpActionResult GetSpMenu(int CatID , int SPID)
        {

            List<Menu> menu = (from x in db.ItemsCategories.Where(y => y.ServiceProvider.SPCat == CatID &&
                                        y.SPID ==SPID).ToList()
                                     where x.ServiceProvider.User.Active == true
                                     select new Menu
                                     {
                                         MenuID = x.CatID,
                                         MenuName = x.CatName,
                                         Photo = _app.UI.Photos.AbsoluteUrl(x.Photo)
                                     }).ToList();


            if (menu.Count > 0)
            {
                menu.Insert(0, new Menu { MenuID = -1 , MenuName = "كل الاصناف" , Photo = null });
            }

            return Returnlist(menu);

        }      
        private IHttpActionResult GetMenuItems(int CatID, int SPID, int MenuID, int MaxItemID)
        {

            List<items> it = new List<items>();

            if (MenuID > 0)
            {
                it = (from x in db.Items.Where(y => y.ItemsCategory.ServiceProvider.SPCat == CatID &&
                                       y.ItemsCategory.SPID == SPID && y.CatID == MenuID && y.ItemID > MaxItemID).ToList()
                                    where x.Active == true && x.ItemsCategory.ServiceProvider.User.Active == true
                                   
                                    select new items
                                    {
                                        ItemID = x.ItemID,
                                        ItemName = x.ItemName,
                                        ItemPrice = x.ItemPrice,
                                        Photo = _app.UI.Photos.AbsoluteUrl(x.Photo),
                                        itemInfo = x.Description,
                                        Properties = null,
                                        MenuID = x.CatID
                                    }).Take(ItemsPerPage).ToList();

            }

            else
            {
                it = (from x in db.Items.Where(y => y.ItemsCategory.ServiceProvider.SPCat == CatID &&
                                       y.ItemsCategory.SPID == SPID && y.ItemID > MaxItemID ).ToList()
                                    where x.Active == true && x.ItemsCategory.ServiceProvider.User.Active == true
                                    select new items
                                    {
                                        ItemID = x.ItemID,
                                        ItemName = x.ItemName,
                                        ItemPrice = x.ItemPrice,
                                        Photo = _app.UI.Photos.AbsoluteUrl(x.Photo),
                                        itemInfo = x.Description,
                                        Properties = null,
                                        MenuID = x.CatID
                                    }).Take(ItemsPerPage).ToList();



            }

            if (it.Count > 0)
            {
                return Returnlist(it, it.Max(x => x.ItemID));

            }
            else
            {
                return Returnlist(it);

            }

           


        }  
        private IHttpActionResult getItemDetails(int CatID, int SPID , int MenuID, int ItemID)
        {
            

            var item = db.Items.FirstOrDefault(x => x.ItemID == ItemID &&
               x.CatID == MenuID && x.ItemsCategory.SPID == SPID &&
               x.ItemsCategory.ServiceProvider.SPCat == CatID);


            if (item != null)
            {
                _app.WepApi.Order.items it = new items()
                {
                    ItemID = item.ItemID,
                    ItemName = item.ItemName,
                    ItemPrice = item.ItemPrice,
                    Photo = _app.UI.Photos.AbsoluteUrl(item.Photo)
                };
                foreach (var p in item.Properties)
                {

                    ItemProperty pr = new ItemProperty()
                    {
                        IsMandatory = p.IsMandatory,
                        PropertyID = p.PropertyID,
                        PropertyName = p.PropertyName
                    };
                    it.Properties.Add(pr);


                    foreach (var v in p.PropertyValues)
                    {
                        pr.PropertyValues.Add(new ItemPropertyValues
                        {
                            ValueID = v.ValueID,
                            ValueName = v.ValueName
                        ,
                            ValuePrice = v.ValuePrice
                        });
                    }

                }
                OkResponse = new _app.WepApi.Response.Ok() { Data = it };
                return Ok(OkResponse);
            }
            else
            {
                var ex = v.NewEx("item not found check the 'ID's'", _app.WepApi.Response.ErrorType.Developer);
                return Ok(ex);
            }

        }
        #endregion

        [HttpPost]
        public IHttpActionResult DataDetails(MyDataDetails model)
        {

            //var ex = v.SessionID(model.SessionID);
            //if (ex.HasErrors()) { return Ok(ex); }
            //var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            //ex = v.isCustomer(ms);
            //if (ex.HasErrors()) { return Ok(ex); }

            if (model.CatID == 0 && model.SPID == 0 && model.MenuID == 0 &&  // request for service providers categories
                model.ItemID == 0) { return GetSPCat(model.lat, model.lng, model.SessionID); }
            else if (model.CatID != 0 && model.SPID == 0 && model.MenuID == 0 && // request for service prividers
                model.ItemID == 0) { return GetSp(model.CatID, model.SessionID); }
            else if (model.CatID != 0 && model.SPID != 0 && model.MenuID == 0 && // request for service provider menu categories
                model.ItemID == 0) { return GetSpMenu(model.CatID, model.SPID); }
            else if (model.CatID != 0 && model.SPID != 0 && model.MenuID != 0 && // request for menu category itmes
                 model.ItemID == 0) { return GetMenuItems(model.CatID, model.SPID, model.MenuID, model.MaxItemID ); }
            else if (model.CatID != 0 && model.SPID != 0 && model.MenuID != 0 && // request for item details
                model.ItemID != 0) { return getItemDetails(model.CatID, model.SPID, model.MenuID, model.ItemID); }
            else
            {var ex = v.NewEx("invaled json object", _app.WepApi.Response.ErrorType.Developer); return Ok(ex); }

        }
        public IHttpActionResult Ads()
        {

            var ads = _app.WepApi.Convert.DB2Json.Adds(db.Adds.Where(x=> x.Active==true).OrderByDescending(x=> x.DateCreated).ToList());


            OkResponse = new _app.WepApi.Response.Ok { Data = ads };
            return Ok(OkResponse);
        }

        //----------- order processing ---------//

        public IHttpActionResult MyBasket(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var cus = ms.User.Customer;


            var order = cus.Orders.FirstOrDefault(x => x.OrderStatusID == OrderStatusEnum.NewOrder);
           
            if (order == null) { ex = v.BaseketEmpty(); return Ok(ex); }
            else
            {

                //_app.WepApi.Order.order ord = new _app.WepApi.Order.order()
                //{ OrderDate = order.OrderDate, OrderID = order.OrderID, CustomerNotes = order.CustomerNotes,
                //    AddressID = order.AddressID, TotalOrder = order.Grandtotal, DeliveryCharges = order.ServiceProvider.DeliveryCharges  };

                var ord = _app.WepApi.Convert.DB2Json.Order(order);

                //ord.items = (from x in order.OrderItems.ToList()
                //             select new _app.WepApi.Order.items { ItemID = x.ItemID, ItemName = x.Item.ItemName, Quantity = x.Quantity,
                //                 Photo = _app.UI.Photos.AbsoluteUrl(x.Item.Photo), ItemPrice = x.TotalPrice, itemInfo = x.Item.Description,
                //                 MenuID = x.Item.CatID,
                //                 TotalPrice = (x.TotalPrice * x.Quantity), OIID = x.OIID
                //                //Properties = (from y in x.OIPropertyValues 
                //                //              select new _app.WepApi.Order.ItemProperty
                //                //              { IsMandatory = y.Property.IsMandatory , PropertyID = y.PropertyID , PropertyName = y.Property.PropertyName
                //                //             , SelectedValueID = y.ValueID ,PropertyValues = (from z in y.Property.PropertyValues
                //                //                select new _app.WepApi.Order.ItemPropertyValues { ValueID = z.ValueID , ValueName = z.ValueName
                //                //               , ValuePrice = z.ValuePrice 
                //                //              //, isSellected = x.OIPropertyValues.Any(w=> w.ValueID == z.ValueID)
                //                //                }).ToList()} ).ToList()
                //            }).ToList();


                if (order.OrderStatusID == orDatabase.OrderStatusEnum.NewOrder) {
                    ord.ItmesQuantaty = basketQuantaty(order);
                }
                else
                {
                    ord.ItmesQuantaty = 0;

                }

               

               


                OkResponse = new _app.WepApi.Response.Ok();
                OkResponse.Data = ord;
                return Ok(OkResponse);

            }

        }
        public IHttpActionResult UpdateOrderItems(AddItem model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }


            var customer = ms.User.Customer;
            var order = customer.CurrentOpenOrder;
            if (order == null)
            {
                order = new Order()
                {
                    Customer = customer,
                    CustomerID = customer.CustomerID,
                    OrderDate = _app.DateTime.Now,
                    OrderID = 0
                };
                _app.Audits.NewForStatus1(order, customer.CustomerID);
                db.Orders.Add(order);
            }

            ex = v.CanBeModifiedByCustomer(order);
            if (ex.HasErrors()) { return Ok(ex); }


            //-- assing order service provider
            var item = db.Items.FirstOrDefault(x => x.ItemID == model.item.ItemID);
            if (order.SPID == null)
            { order.SPID = item.ItemsCategory.SPID; order.ServiceProvider = item.ItemsCategory.ServiceProvider; }
            else if (order.SPID != item.ItemsCategory.SPID && order.SPID != null)
            {
                ex = v.ChangeSP(); return Ok(ex);
            }

            //-- validation mandatory properties
            var pl = item.Properties.Where(x => x.IsMandatory == true).ToList();
            if (pl.Count > 0)
            {
                foreach (var p in pl)
                {
                    var sp = model.item.Properties.FirstOrDefault(x => x.PropertyID == p.PropertyID);
                    if (sp == null)
                    {
                        ex = v.MissingPropertyValue(p);
                        return Ok(ex);
                    }
                    else { if (!p.PropertyValues.Any(x => x.ValueID == sp.SelectedValueID))
                            {
                            ex = v.MissingPropertyValue(p);
                            return Ok(ex);
                        }

                    }

                }

            }

            // check quantaty
            if (model.item.Quantity <= 0) { ex = v.ZeroQuantaty(); return Ok(ex); }

            // -- add or update item
            orDatabase.OrderItem oi = new OrderItem();
            if (model.item.OIID != 0)
            {
                oi = order.OrderItems.FirstOrDefault(x => x.OIID == model.item.OIID);

            }
            else {

                // -- check if this item was added be4
                var oldoiList = order.OrderItems.Where(x => x.ItemID == model.item.ItemID).ToList();
                if (oldoiList.Count >0)
                {

                    foreach (var oldoi in oldoiList)
                    {
                        int i = 0;

                    foreach (var p in oldoi.OIPropertyValues)
                    {
                        var selectedProperty = model.item.Properties.FirstOrDefault(x => x.PropertyID == p.PropertyID);
                        if (selectedProperty != null && p.ValueID == selectedProperty.SelectedValueID) { i += 1; }

                    }

                    if (i == oldoi.OIPropertyValues.Count)
                    {
                        model.item.Quantity = model.item.Quantity + oldoi.Quantity;
                        oi = oldoi;
                            break;
                    }
                    }

                    order.OrderItems.Add(oi);

                }
                else
                {
                order.OrderItems.Add(oi);
                }




            }
            //  ItemJson2DbModel(model.item, oi);
            _app.WepApi.Convert.Json2DB.item(model.item, oi,db);

            db.SaveChanges();


            OkResponse = new _app.WepApi.Response.Ok() { Messages = "تم تحديث السله", Data = basketQuantaty(order)  };
            return Ok(OkResponse);

        }
        public IHttpActionResult OrderItemDetails(ItemDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }


            var customer = ms.User.Customer;
            var order = customer.CurrentOpenOrder;


            
            var oi = order.OrderItems.FirstOrDefault(x => x.OIID == model.OIID);
            _app.WepApi.Order.items item = new items()
            { OIID = oi.OIID, ItemID = oi.ItemID , ItemName = oi.Item.ItemName , ItemPrice = oi.ItemPrice,
            Photo = _app.UI.Photos.AbsoluteUrl(oi.Item.Photo), Quantity = oi.Quantity, TotalPrice = oi.TotalPrice };

            item.Properties = (from x in oi.OIPropertyValues select new _app.WepApi.Order.ItemProperty
            { IsMandatory = x.Property.IsMandatory , PropertyID = x.PropertyID , PropertyName = x.Property.PropertyName,
            SelectedValueID = x.ValueID  , PropertyValues = (from y in x.Property.PropertyValues.ToList() 
                                                          select new _app.WepApi.Order.ItemPropertyValues
                                                          { ValueID = y.ValueID , ValueName = y.ValueName , ValuePrice = y.ValuePrice }
                                                          ).ToList() }
            ).ToList();

            OkResponse = new _app.WepApi.Response.Ok() { Data = item };
            return Ok(item);

        }
     
        [HttpPost]
        public IHttpActionResult DeleteOrderItem(ItemDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }


            var customer = ms.User.Customer;
            var order = customer.CurrentOpenOrder;

            ex = v.CanBeModifiedByCustomer(order);
            if (ex.HasErrors()) { return Ok(ex); }
            RemoveOI(order, model.OIID);
           

            db.SaveChanges();

            OkResponse = new _app.WepApi.Response.Ok() { Messages = "تم تحديث السله" , Data = basketQuantaty(order) };
            return Ok(OkResponse);

        }
        [HttpPost]
        public IHttpActionResult DeleteBasket(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }


            var customer = ms.User.Customer;
            var order = customer.CurrentOpenOrder;

            ex = v.CanBeModifiedByCustomer(order);
            if (ex.HasErrors()) { return Ok(ex); }

            foreach (var oi in order.OrderItems.ToList())
            {
                RemoveOI(order, oi.OIID);
            }

            order.ServiceProvider = null;
            order.SPID = null;
            db.SaveChanges();

            OkResponse = new _app.WepApi.Response.Ok() { Messages = "تم مسح محتويات السله" , Data = 0 };
            return Ok(OkResponse);

        }
        [HttpPost]
        public IHttpActionResult UpdateOrderAddress(_app.WepApi.Order.OrderAddress model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }


            var customer = ms.User.Customer;
            var order = customer.CurrentOpenOrder;

             ex = v.CanBeModifiedByCustomer(order);
            if (ex.HasErrors()) { return Ok(ex); }

            var add = customer.CustomerAddresses.FirstOrDefault(x => x.AddressID == model.AddressID);
            if (add != null) { order.AddressID = add.AddressID; }
            else { ex = v.NewEx("address id not related to customer", _app.WepApi.Response.ErrorType.Developer);
                return Ok(ex);
            }


            db.SaveChanges();

            OkResponse = new _app.WepApi.Response.Ok() { Messages = "تم تحديد عنوان التوصيل بنجاح" };
            return Ok(OkResponse);

        }
        public IHttpActionResult CheckOut(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }


            var customer = ms.User.Customer;
            var order = customer.CurrentOpenOrder;

            ex = v.CheckOutOrder(order);
            if (ex.HasErrors()) { return Ok(ex); }


            if (order.OrderStatusID == OrderStatusEnum.NewOrder)
            {
                _app.Audits.NewForStatus2(order, customer.CustomerID);

                db.SaveChanges();

                OkResponse = new _app.WepApi.Response.Ok() { Messages = "تم ارسال الطلب الي موفر الخدمه", Data = order.ServiceProvider.DeliveryTimeframe };
                return Ok(OkResponse);

            }
            else
            {
                ex = v.NewEx("order status id :" + order.OrderStatusID.ToString() + " and can not be changed", _app.WepApi.Response.ErrorType.Developer);
                return Ok(ex);
            }




        }
        public IHttpActionResult CancelOrder(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }


            var customer = ms.User.Customer;
            var order = customer.CurrentOpenOrder;


            



            if (order.OrderStatusID == OrderStatusEnum.Sent2SP)
            {

                if (order.PMID == 2) { ex = v.NewEx("لا يمكن الغاء هذا الطلب لانه تم دفعه ببطاقه " +
                    "الائتمان رجاء الاتصال بمدير النظام", _app.WepApi.Response.ErrorType.Order); return Ok(ex); }
                else
                {

                    _app.Audits.NewForStatus3(order, customer.CustomerID);
                    db.SaveChanges();
                    OkResponse = new _app.WepApi.Response.Ok() { Messages = "نم الغاء الطلب" };
                    return Ok(OkResponse);

                }
               

            }
            else
            {
                ex = v.NewEx("order status id :" + order.OrderStatusID.ToString() + " and can not be changed", _app.WepApi.Response.ErrorType.Developer);
                return Ok(ex);
            }




        }
        public IHttpActionResult MyOrders(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var cus = ms.User.Customer;

           // List<_app.WepApi.Order.order> ord = new List<_app.WepApi.Order.order>();

          var ord =  _app.WepApi.Convert.DB2Json.Orders( cus.Orders.ToList());


            OkResponse = new _app.WepApi.Response.Ok { Data = ord };
            return Ok(OkResponse);
        }
        public IHttpActionResult OrderDetails(_app.WepApi.Order.orderDetails model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var cus = ms.User.Customer;

           // _app.WepApi.Order.order ord = new _app.WepApi.Order.order();


           var ord = _app.WepApi.Convert.DB2Json.Order( cus.Orders.FirstOrDefault(x => x.OrderID == model.OrderID));

            OkResponse = new _app.WepApi.Response.Ok { Data = ord };
            return Ok(OkResponse);
        }
        public IHttpActionResult MyOpenOrder(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var cus = ms.User.Customer;
            var order = cus.CurrentOpenOrder;
            ex = v.OpenOrder(order);
            if (ex.HasErrors()) { return Ok(ex); }
            var ord = _app.WepApi.Convert.DB2Json.Order(order);

            ord.orderActions = new List<OrderAction>();
            ord.orderActions = _app.WepApi.Convert.DB2Json.OrderActions(order);

            OkResponse = new _app.WepApi.Response.Ok { Data = ord };
            return Ok(OkResponse);
        }
        public IHttpActionResult RateMyOrder(_app.WepApi.Order.RateOrder model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }


            var ord = ms.User.Customer.Orders.FirstOrDefault(x => x.OrderID == model.OrderID);
            ex = v.CustomerRateOrder(ord);
            if (ex.HasErrors()) { return Ok(ex); }
            ord.OrderRating = model.rating;
            ord.RatingComment = model.Comment;
            db.SaveChanges();
            OkResponse = new _app.WepApi.Response.Ok { Messages = "saved !" };
            return Ok(OkResponse);
        }
        public IHttpActionResult PaymentMethods(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var pm = _app.WepApi.Convert.DB2Json.PaymentMethods(db.PaymentMethods.ToList());
            OkResponse = new _app.WepApi.Response.Ok { Data = pm };
            return Ok(OkResponse);

        }
        public IHttpActionResult UpdateOrderPayment(_app.WepApi.Order.OrderPayment model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var customer = ms.User.Customer;
            var order = customer.CurrentOpenOrder;

            ex = v.CanBeModifiedByCustomer(order);
            if (ex.HasErrors()) { return Ok(ex); }

            if (order.Grandtotal == 0) { ex = v.NewEx("لابد ان تكون قيمه الطلب اكثر من صفر", _app.WepApi.Response.ErrorType.Order);
                return Ok(ex);
            }


            if (model.MethodID == 1)
            {
                order.PMID = model.MethodID;
                db.SaveChanges();
                 OkResponse = new _app.WepApi.Response.Ok() { Messages = "تم تحديد طريقه الدفع" };
              //  return CheckOut(new _app.WepApi.Session { SessionID = model.SessionID });
            }
            else if (model.MethodID == 2)
            {

                ex = v.Email(model.CustomerEmail);
                if (ex.HasErrors()) { return Ok(ex); }

                var onlinepayment = order.BankPayements.ToList();
                if (onlinepayment.Where(x => x.PaymentStatusID == PayemntStatusEnum.Success).Count() > 0) // this order has been payd for
                { var p = onlinepayment.FirstOrDefault(x => x.PaymentStatusID == PayemntStatusEnum.Success);
                    ex = v.NewEx("order has been payed for. payemntID#" + p.BankPayementID.ToString(),
                        _app.WepApi.Response.ErrorType.Developer);
                    return Ok(ex);
                }
                foreach (var p in onlinepayment.Where(x => x.PaymentStatusID == PayemntStatusEnum.Pending).ToList())
                {  p.PaymentStatusID = PayemntStatusEnum.Failed; } db.SaveChanges();

                order.PMID = model.MethodID;
                db.SaveChanges();
                _app.Payments pay = new _app.Payments();
                var url = pay.InitOrderPayment(order, model.CustomerEmail);
                OkResponse = new _app.WepApi.Response.Ok { Data = new { BankUrl = url } };
               
            }
            return Ok(OkResponse);

        }
       
        public IHttpActionResult OrderNotes(_app.WepApi.Order.OrderNote model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var cus = ms.User.Customer;


            var order = cus.Orders.FirstOrDefault(x => x.OrderStatusID == OrderStatusEnum.NewOrder);

            if (order == null) { ex = v.BaseketEmpty(); return Ok(ex); }

            order.CustomerNotes = model.OrderNotes;
            db.SaveChanges();

            OkResponse = new _app.WepApi.Response.Ok { Messages = "تم اضافه الملاحظات" };
            return Ok(OkResponse);

        }
        public IHttpActionResult Search(_app.WepApi.Search model)
        {

            if (string.IsNullOrEmpty(model.KeyWord))
            { var ex = v.NewEx("برجاء توفير كلمات بحث", _app.WepApi.Response.ErrorType.Order); return Ok(ex); }

            var spl = db.ServiceProviders.Where(x => x.SpName.ToLower().Contains(model.KeyWord.ToLower()) ||
             x.Notes.ToLower().Contains(model.KeyWord.ToLower())).ToList();


            var jsp = _app.WepApi.Convert.DB2Json.ServiceProvider(spl);
            OkResponse = new _app.WepApi.Response.Ok { Data = jsp };
            return Ok(OkResponse);
        }
    }
}
