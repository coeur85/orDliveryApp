﻿using _app.WepApi;
using _app.WepApi.Account;
using orDatabase;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace orDliveryApp.Controllers.API.CustomerApp
{

    public class CustomerAccountController : ApiController
    {

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        _app.Validation v = new _app.Validation();
        new _app.WepApi.Response.Ok Ok = new _app.WepApi.Response.Ok();
        _app.WepApi.Methods m = new _app.WepApi.Methods();


        [HttpPost]
        public IHttpActionResult Singup(_app.WepApi.Account.Singup model)
        {
            var ex = v.SingUp(model.PhoneNumber);
            if (ex.HasErrors()) { return Ok(ex); }
            Customer customer = new Customer { User = new orDatabase.User(true) };
            customer.PhoneNumber = model.PhoneNumber;
            customer.User.LoginName = customer.PhoneNumber;

            //Random r = new Random();
            //customer.ActivationCode = r.Next(100000, 999999);
            //customer.CodeDate = _app.DateTime.Now;
            //customer.User.Active = false;
            m.SendCode(customer);

            db.Customers.Add(customer);
            db.SaveChanges();
            _app.WepApi.Response.Ok ok = new _app.WepApi.Response.Ok();
            ok.Messages = "sent code:" + customer.ActivationCode.ToString();
            return Ok(ok);
        }
        public IHttpActionResult ConfirmCode(ConfirmCode model)
        {
            var ex = v.PhoneNumer(model.PhoneNumber);
            if (ex.HasErrors()) { return Ok(ex); }
            ex = v.ConfrimCode(model);
            if (ex.HasErrors()) { return Ok(ex); }
            var c = db.Customers.FirstOrDefault(x => x.PhoneNumber == model.PhoneNumber);
            c.User.Active = true;
            db.SaveChanges();
            _app.WepApi.Response.Ok ok = new _app.WepApi.Response.Ok();
            //var profile = m.CustomerProfile(c);
            //ok.Data = profile;
            var session = m.LogUserIn(c.User, model.DeviceID);
            ok.Messages = "account is active";
            ok.Data = session;
            return Ok(ok);
        }
        public IHttpActionResult ResendCode(_app.WepApi.Account.Singup model)
        {
            var ex = v.ResendCode(model.PhoneNumber);
            if (ex.HasErrors()) { return Ok(ex); }

            var c = db.Customers.FirstOrDefault(x => x.User.LoginName == model.PhoneNumber);
            m.SendCode(c);
            db.SaveChanges();
            _app.WepApi.Response.Ok ok = new _app.WepApi.Response.Ok
            {
                Messages = "sent code:" + c.ActivationCode.ToString()
            };
            return Ok(ok);

        }
        public IHttpActionResult UpdateProfile(UpdateProfileInfo model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            ex = v.UpdateCustomerProfile(model);
            if (ex.HasErrors()) { return Ok(ex); }
           
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }
            var cus = ms.User.Customer;
            cus.User.fName = model.userProfile.fName;
            cus.User.mName = model.userProfile.mName;
            cus.User.lName = model.userProfile.lName;
            cus.eMail = model.userProfile.eMail;
            cus.User.Photo = _app.UI.Photos.replacePhoto(cus.User.Photo, model.userProfile.Photo);
            cus.User.LoginPassword = model.userProfile.Password;
            db.SaveChanges();

            //var sid = m.LogUserIn(cus.User, model.login.DeviceID);
            _app.WepApi.Response.Ok o = new _app.WepApi.Response.Ok();
            //o.Data = sid;
            o.Messages = "تم تحديث بينات حسابك";
            return Ok(o);
        }
        public IHttpActionResult MyProfile(Session model)
        {

            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var c = ms.User.Customer;
            _app.WepApi.Response.Ok ok = new _app.WepApi.Response.Ok();
            var profile = _app.WepApi.Convert.DB2Json.Profile(c);
            ok.Data = profile;
            return Ok(ok);



        }
        public IHttpActionResult RestPassword(Singup model)
        {
            var ex = v.RestPassword(model.PhoneNumber);
            if (ex.HasErrors()) { return Ok(ex); }

            var c = db.Customers.FirstOrDefault(x => x.User.LoginName == model.PhoneNumber);
            m.SendCode(c);
            db.SaveChanges();
            _app.WepApi.Response.Ok o = new _app.WepApi.Response.Ok();
            o.Messages = "تم ارسال الكود";
            o.Data = "code is :" + c.ActivationCode.ToString();
            return Ok(o);
        }
        public IHttpActionResult UpdatePassword(UpdatePassword model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            v.Password(model.Password);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var c = ms.User.Customer;
            c.User.LoginPassword = model.Password;
            db.SaveChanges();

            _app.WepApi.Response.Ok o = new _app.WepApi.Response.Ok();
            o.Messages = "تم تحديث كلمه المرور";
            return Ok(o);

        }
        public IHttpActionResult MyAddresses(Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }
            var customer = ms.User.Customer;
            _app.WepApi.Response.Ok o = new _app.WepApi.Response.Ok();
            if (customer.CustomerAddresses.Count == 0)
            {
                o.Messages = "لم يتم العثور علي اي عنوان برجاء الاضافه حتي تتمكن من طلب المنتجات";
                return Ok(o);
            }
            else
            {
                // List<Address> adds = new List<Address>();

                //IEnumerable<Address> adds = from x in customer.CustomerAddresses select new
                //                            Address { Address1 = x.Address1 , Address2 = x.Address2 , ApartmentNumber = x.ApartmentNumber ,
                //AreaName = x.AreaName , AddressID = x.AddressID , City = x.City , Company = x.Company , DeliveryNotes = x.DeliveryNotes ,
                //Floor = x.Floor , Lat = x.Lat , Lng = x.Lng };
                var adds = _app.WepApi.Convert.DB2Json.Address(customer.CustomerAddresses.ToList());

                o.Data = adds.ToList();
                return Ok(o);

            }

        }
        public IHttpActionResult UpdateAddress(upDateAddress model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }
            ex = v.Address(model.Address);
            if (ex.HasErrors()) { return Ok(ex); }
            var customer = ms.User.Customer;


            var add = customer.CustomerAddresses.FirstOrDefault(x => x.AddressID == model.Address.AddressID);
            if (add == null) {
                if (model.Address.AddressID == 0)
                {
                    add = new CustomerAddress(); customer.CustomerAddresses.Add(add);
                }
                else
                {
                    ex = v.NewEx("invalied address id", _app.WepApi.Response.ErrorType.Developer);
                    return Ok(ex);
                }
                
            }
            add.Address1 = model.Address.Address1;
            add.Address2 = model.Address.Address2;
            add.ApartmentNumber = model.Address.ApartmentNumber;
            add.AreaName = model.Address.AreaName;
            add.City = model.Address.City;
            add.Company = model.Address.Company;
            add.CustomerID = customer.CustomerID;
            add.DeliveryNotes = model.Address.DeliveryNotes;
            add.Floor = model.Address.Floor;
            add.Lat = model.Address.Lat;
            add.Lng = model.Address.Lng;
            db.SaveChanges();
            _app.WepApi.Response.Ok o = new _app.WepApi.Response.Ok();
            o.Messages = "تم حفظ العنوان بنجاح";
            return Ok(o);

        }
        public IHttpActionResult SingIn(_app.WepApi.Account.Singin model)
        {
            var ex = v.PhoneNumer(model.LoginID);
            if (ex.HasErrors()) { return Ok(ex); }
            var c = db.Customers.FirstOrDefault(x => x.User.LoginName == model.LoginID && x.User.LoginPassword == model.Password);
            ex = v.isCustomer(c);
            if (ex.HasErrors()) { return Ok(ex); }
            if (c.User.Active == false) { v.NewEx("حساب غير مفعل", _app.WepApi.Response.ErrorType.Account); return Ok(ex); }


            _app.WepApi.Response.Ok ok = new _app.WepApi.Response.Ok();

            var session = m.LogUserIn(c.User, model.DeviceID);
            ok.Data = session;
            return Ok(ok);

        }

        public IHttpActionResult MyFavourite(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var spl = ms.User.Customer.ServiceProviders.ToList();

            if (spl.Count == 0) { ex = v.NewEx("لا يوجد مطاعم مفضله ، ابداء بالتسوق  وحدد المتاجر التي تحبها",
                _app.WepApi.Response.ErrorType.Account); return Ok(ex); }

            var json = _app.WepApi.Convert.DB2Json.ServiceProvider(spl);
            _app.WepApi.Response.Ok k = new _app.WepApi.Response.Ok { Data = json };
            return Ok(k);
        }
        public IHttpActionResult UpdateMyFavourite(_app.WepApi.Account.FavouriteSP model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            var cust = ms.User.Customer;
            var sp = db.ServiceProviders.Find(model.SPID);
            _app.WepApi.Response.Ok k = new _app.WepApi.Response.Ok();
            if (cust.ServiceProviders.Any(x => x.SPID == model.SPID))
            {
                cust.ServiceProviders.Remove(sp);
                k.Messages = "تم ازاله موفر الخدمه من قائمه المفضل";

            }
            else
            {
                cust.ServiceProviders.Add(sp);
                k.Messages = "تم اضافه موفر الخدمه لقائمه المفضل";
            }

            db.SaveChanges();
            return Ok(k);


        }


        public IHttpActionResult SingOut(_app.WepApi.Session model)
        {
            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            if (ex.HasErrors()) { return Ok(ex); }

            m.logUserOut(ms);
            Ok = new _app.WepApi.Response.Ok { Messages = "تم تسجيل الخروج بنجاح"  };
            return Ok(Ok);

        }


        public IHttpActionResult SupportPhone() {
            var phone = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.SupportPhone).FirstOrDefault();
            Ok = new _app.WepApi.Response.Ok { Data = new { PhoneNumber = phone.Value } };
            return Ok(Ok);
        }
    }
}
