﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using orDatabase;

namespace orDliveryApp.Controllers.API.MobileApp.GlobalApi
{
    public class AllController : ApiController
    {


        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        _app.Validation v = new _app.Validation();
        //public IHttpActionResult UploadMobileScreenPhoto(_app.WepApi.MobileWelcomeScreen model)
        //{
        //    try
        //    {

        //        var p = db.SystemSetups.FirstOrDefault(x => x.SID == model.dbid);
        //        p.Value = model.Photo;
        //        db.SaveChanges();
        //        return Ok();
        //    }
        //    catch (Exception e)
        //    {

        //        return InternalServerError(e);
        //    }




        //}
        public IHttpActionResult WellcomeScreen()
        {
            List<_app.WepApi.MobileWelcomeScreen> ws = new List<_app.WepApi.MobileWelcomeScreen>();

            var pl = db.SystemSetups.Where(x => x.Code == (int)_app.SystemSetup.mobileWellcomeScreen).ToList();

            foreach (var p in pl)
            {
                ws.Add(new _app.WepApi.MobileWelcomeScreen {  Photo = _app.UI.Photos.AbsoluteUrl(p.Value) });
            }

            _app.WepApi.Response.Ok k = new _app.WepApi.Response.Ok();
            k.Data = ws;
            return Ok(k);

        }
        public IHttpActionResult MyNotifications(_app.WepApi.MyNotificationDetails model)
        {

            var ex = v.SessionID(model.SessionID);
            if (ex.HasErrors()) { return Ok(ex); }
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            ex = v.isCustomer(ms);
            List<_app.WepApi.Notification> nl = new List<_app.WepApi.Notification>();
            if (!ex.HasErrors())
            {
                if (model.NotificationID <= 0)
                { nl = _app.WepApi.Convert.DB2Json.Notifications(db.Notifications.Where(x => x.ToCustomers == true).ToList()); }
                else
                {
                    nl = _app.WepApi.Convert.DB2Json.Notifications(db.Notifications.Where(x => x.ToCustomers == true&&
                    x.NotificationID == model.NotificationID ).ToList());
                }
            }

            v.Clear();
            ex = v.isDriver(ms);
            if (!ex.HasErrors())
            {
                if (model.NotificationID <= 0)
                { nl = _app.WepApi.Convert.DB2Json.Notifications(db.Notifications.Where(x => x.ToDrivers == true).ToList()); }
                else
                {
                    nl = _app.WepApi.Convert.DB2Json.Notifications(db.Notifications.Where(x => x.ToDrivers == true &&
                    x.NotificationID == model.NotificationID ).ToList());
                }
                   
            }

            _app.WepApi.Response.Ok k = new _app.WepApi.Response.Ok { Data = nl.OrderByDescending(x => x.NotificationID).ToList() };
            return Ok(k);
        }

    }
}
