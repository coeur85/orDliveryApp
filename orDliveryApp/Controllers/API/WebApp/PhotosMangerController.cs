﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using orDatabase;

namespace orDliveryApp.Controllers.API.WepApp
{
    public class PhotosMangerController : ApiController
    {
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();

        public IHttpActionResult UploadMobileScreenPhoto(_app.WepApi.MobileWelcomeScreen model)
        {
            try
            {

                var p = db.SystemSetups.FirstOrDefault(x => x.SID == model.dbid);
                p.Value = model.Photo;
                db.SaveChanges();
                return Ok();
            }
            catch (Exception e)
            {

                return InternalServerError(e);
            }




        }

    }
}
