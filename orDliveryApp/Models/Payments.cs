﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using orDatabase;

namespace _app
{
    public class Payments
    {

        //id:373301
        //password:373301
        //amt:15
        //currencycode:414
        //langid:eng
        //responseURL:https://www.magdi.com
        //errorURL:https://www.error.magdi.com
        //trackId:9787087261562730
        //action:1

        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();
        private string id = "373301";
        private string password = "373301";
        private string currencycode = "414";
        private string langid = "eng";
        private string responseURL = "https://ordeliverykw.com/PaymentPortal/ok/{id}";
       // private string responseURL = "https://ordeliverykw.com/api/PaymentPortal/Success";
        private string errorURL = "https://ordeliverykw.com/PaymentPortal/BankError/{id}";
        private string trackId = "9787087261562730";
        private string action = "1";



        private string BankUrl { get {
                return "https://www.knetpaytest.com.kw/CGW302/servlet/PaymentInitHTTPServlet";
            } }


        //private string CustomerEmail { get; set; }
        //private Order Order { get; set {


        //    }
        //}

        //public Payments(string customerMail , orDatabase.Order order)
        //{

        //    CustomerEmail = customerMail;
        //    Order = order;
        //}

        public string InitOrderPayment(Order order, string customerMail) {

            string data ="id="+ id +"&password="+password+"&amt="+order.Grandtotal.ToString() +"&" +
                "currencycode="+ currencycode +"&langid="+ langid+"&responseURL="+ responseURL +"&" +
                "errorURL="+ errorURL +"&trackId="+ trackId+"&action="+ action ; //replace <value>

            data = data.Replace("{id}", order.OrderID.ToString());

            byte[] dataStream = Encoding.UTF8.GetBytes(data);
           
            WebRequest webRequest = WebRequest.Create(BankUrl);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = dataStream.Length;  
            Stream newStream = webRequest.GetRequestStream();
            // Send the data.
            newStream.Write(dataStream,0,dataStream.Length);
            newStream.Close();
            
            string strResponse;
            StreamReader stIn = new StreamReader(webRequest.GetResponse().GetResponseStream());
          
            strResponse = stIn.ReadToEnd();
            stIn.Close();
            string[] strArray = strResponse.Split(':');
            string url = "";

            for (int i = 1; i < strArray.Count(); i++)
            {
                url += (strArray[i] + ":" );
            }

            url = url.Remove(url.Length - 1, 1);
            url= url + "?PaymentID=" + strArray[0];

           
            var pl = db.BankPayements.Where(x => x.OrderID == order.OrderID).ToList();


            // close all open tickets
            foreach (var p in pl.Where(x => x.PaymentStatusID == PayemntStatusEnum.Pending ).ToList())
            {
                p.PaymentStatusID = PayemntStatusEnum.Failed;
            }
            db.SaveChanges();
            BankPayement py = new BankPayement {
                 Amount = order.Grandtotal,
                 AuthCode = null,
                 BankPayementID = Convert.ToInt64(strArray[0]),
                 PaymentStatusID = PayemntStatusEnum.Pending,
                 OrderID = order.OrderID,
                 ReferenceID = null,
                 ResultCode = null,
                 TrackID = trackId,
                 TransactionID = null,
                 TransactionDate = _app.DateTime.Now,
                 UDFs = null,
                 CustomerPaymentEmail = customerMail,
                 PaymentURL = url,                 
            };

            db.BankPayements.Add(py);
            db.SaveChanges();


            return url;
           // return (strArray[1] + ":" + strArray[2] + "?PaymentID=" + strArray[0]);

        }


        public void SendPaymentEmail(BankPayement payement)
        {
            string[] readText = File.ReadAllLines(HttpContext.Current.Server.MapPath("~/html/emin.html"));
            StringBuilder strbuild = new StringBuilder();
            foreach (string s in readText)
            {
                strbuild.Append(s);
                strbuild.AppendLine();
            }
            var body =  strbuild.ToString();


            // replace the dynamc data

            body = body.Replace("{date}", payement.TransactionDate.ToLongDateString());
            body = body.Replace("{time}", payement.TransactionDate.ToShortTimeString());
            body = body.Replace("{status}", payement.PaymentStatu.Name);
            body = body.Replace("{amount}", payement.Amount.ToString());
            body = body.Replace("{referenceId}", payement.ReferenceID);
            body = body.Replace("{payemntId}", payement.BankPayementID.ToString());
            body = body.Replace("{trackId}", payement.TrackID);

            body = body.Replace("{ResultCode}", payement.ResultCode);
            body = body.Replace("{AuthCode}", payement.AuthCode);
            body = body.Replace("{TransactionID}", payement.TransactionID);

            if (payement.PaymentStatusID == PayemntStatusEnum.Failed) {
                body = body.Replace("{imgName}", "error");
            }
            else if (payement.PaymentStatusID == PayemntStatusEnum.Success)
            {
                body = body.Replace("{imgName}", "ok");
            }
            _app.Send.eMail mail = new Send.eMail();

            mail.SendFromPaymentMail(payement.PaymentStatu.Name, body, payement.CustomerPaymentEmail);


        }

    }
}