﻿
using orDatabase;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;

namespace _app
{

    namespace Send
    {



        public class TextMessage



        {
            private string userName = System.Configuration.ConfigurationManager.AppSettings["smsUserName"];
            private string password = System.Configuration.ConfigurationManager.AppSettings["smsPassword"];
            private string sender = System.Configuration.ConfigurationManager.AppSettings["smsSender"];
            private string domain = "https://" +  System.Configuration.ConfigurationManager.AppSettings["smsDomain"] + "/SMSGateway/Services/Messaging.asmx/";
            private string CustomerID = System.Configuration.ConfigurationManager.AppSettings["smsCustomerID"]; 



            private string FormatePhoneNUmber(string phoneNumer)
            {
                // if (phoneNumer.StartsWith("05")) { phoneNumer = phoneNumer.Remove(0, 1); phoneNumer = ("0966" + phoneNumer); }
                if (!phoneNumer.StartsWith("965")) { phoneNumer = "965" + phoneNumer; }

                return phoneNumer;
            }
            public string SendConfirmText(string phoneNumer, string code)
            {

                phoneNumer = FormatePhoneNUmber(phoneNumer);
                string text = "OR Delivery -- your activation code is: " + code;
                return SendMessage(text, phoneNumer);
                // return sendText(phoneNumer, code);

            }
            public int SendRestPasswordTest(string phoneNumer, string password)
            {

                phoneNumer = FormatePhoneNUmber(phoneNumer);
                string text = "كلمه المرور الجديده هي : " + password;
                return Convert.ToInt32(SendMessage(text, phoneNumer));
                // return sendText(phoneNumer, code);

            }
            //public string GetBalance()
            //{
            //    //int temp = '0';
            //    //WebResponse myResponse = null;

            //    try
            //    {
            //        HttpWebRequest req = (HttpWebRequest)
            //        WebRequest.Create( domain +"balance.aspx?");
            //        req.Method = "POST";
            //        req.ContentType = "application/x-www-form-urlencoded";
            //        string postData = "username=" + userName + "&password=" + password;
            //        req.ContentLength = postData.Length;

            //        StreamWriter stOut = new
            //        StreamWriter(req.GetRequestStream(),
            //        System.Text.Encoding.ASCII);
            //        stOut.Write(postData);
            //        stOut.Close();
            //        // Do the request to get the response
            //        string strResponse;
            //        StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
            //        strResponse = stIn.ReadToEnd();
            //        stIn.Close();

            //        //if (strResponse == "1" || strResponse == "2" || strResponse == "-2" || strResponse == "-1")
            //        //{ return "error"; }

            //        return strResponse;
            //    }
            //    catch (Exception)
            //    {

            //        return "error";
            //    }


            //}
            //private string SendMessage(string msg, string numbers)
            //{
            //    //int temp = '0';

            //    HttpWebRequest req = (HttpWebRequest)
            //    WebRequest.Create(domain + "send.aspx");
            //    req.Method = "POST";
            //    req.ContentType = "application/x-www-form-urlencoded";
            //    string postData = "username=" + userName + "&password=" + password + "&mobile=" + numbers + "&sender=" + sender +
            //        "&message=" + ConvertToUnicode(msg) + "&applicationType=59&language=3";
            //    req.ContentLength = postData.Length;

            //    StreamWriter stOut = new
            //    StreamWriter(req.GetRequestStream(),
            //    System.Text.Encoding.ASCII);
            //    stOut.Write(postData);
            //    stOut.Close();
            //    // Do the request to get the response
            //    string strResponse;
            //    StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
            //    strResponse = stIn.ReadToEnd();
            //    stIn.Close();
            //    return strResponse;
            //}
            public string GetBalance()
            {
                //int temp = '0';
                //WebResponse myResponse = null;

                try
                {
                    HttpWebRequest req = (HttpWebRequest)
                    WebRequest.Create(domain + "/Http_AuthenticateUser?");
                    req.Method = "POST";
                    req.ContentType = "application/x-www-form-urlencoded";
                    string postData = "username=" + userName + "&password=" + password + "&customerId=" + CustomerID;
                    req.ContentLength = postData.Length;

                    StreamWriter stOut = new
                    StreamWriter(req.GetRequestStream(),
                    System.Text.Encoding.ASCII);
                    stOut.Write(postData);
                    stOut.Close();
                    // Do the request to get the response
                    string strResponse;
                    StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
                    strResponse = stIn.ReadToEnd();
                    stIn.Close();

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(strResponse);

                    // return xmlDoc.SelectNodes("/Message").Count.ToString();
                    // return strResponse;
                    //  return xmlDoc.SelectSingleNode("AuthenticationResult/NetPoints").InnerText;
                    return xmlDoc.ChildNodes.Item(1).ChildNodes.Item(3).InnerText;
                }
                catch (Exception)
                {

                    return "error";
                }


            }
            private string SendMessage(string msg, string numbers)
            {
                //int temp = '0';

                HttpWebRequest req = (HttpWebRequest)
                WebRequest.Create(domain + "/Http_SendSMS?");
                req.Method = "POST";
                req.ContentType = "application/x-www-form-urlencoded";
                string postData = "username=" + userName + "&password=" + password + "&recipientNumbers=" + numbers + "&senderText=" + sender +
                    "&messageBody=" + msg + //ConvertToUnicode(msg) +
                    "&defdate=&isBlink=false&isFlash=false&customerId=" +CustomerID;
                req.ContentLength = postData.Length;

                StreamWriter stOut = new
                StreamWriter(req.GetRequestStream(),
                System.Text.Encoding.ASCII);
                stOut.Write(postData);
                stOut.Close();
                // Do the request to get the response
                string strResponse;
                StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
                strResponse = stIn.ReadToEnd();
                stIn.Close();
                return strResponse;
            }
            private string ConvertToUnicode(string val)
            {
                string msg2 = string.Empty;

                for (int i = 0; i < val.Length; i++)
                {
                    msg2 += convertToUnicode(System.Convert.ToChar(val.Substring(i, 1)));
                }

                return msg2;
            }
            private string convertToUnicode(char ch)
            {
                System.Text.UnicodeEncoding class1 = new System.Text.UnicodeEncoding();
                byte[] msg = class1.GetBytes(System.Convert.ToString(ch));

                return fourDigits(msg[1] + msg[0].ToString("X"));
            }
            private string fourDigits(string val)
            {
                string result = string.Empty;

                switch (val.Length)
                {
                    case 1: result = "000" + val; break;
                    case 2: result = "00" + val; break;
                    case 3: result = "0" + val; break;
                    case 4: result = val; break;
                }

                return result;
            }
        }
        public class Notifications
        {

            string applicationID = "AAAAtYNh3E8:APA91bFRH86lP0XHNwH1BQBs5_hYXjCUdXgVF31EFrit7YDYjl439s5VsHxx973Zh7CFeaB-xFAsfdfNu7WnqIVkgLvCLSK2rqJ-f2JEMo5sKVQ7rbf4_Srni21vnPusgp6aqcfrt9FX";
            string senderId = "779593309263";




            private string SendNotification(MobileSession ms, _app.WepApi.Notification noti) //string Message, string Title, string token
            {
                try
                {


                    string deviceId = ms.MobileID;

                    WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                    tRequest.Method = "post";

                    tRequest.ContentType = "application/json";





                    var data = new
                    {
                        notification = new
                        {
                            title = noti.Header,  //Any value
                            body = noti.Continet,  //Any value
                            sound = "trembling", //If you want notification sound
                          //  sound = "trembling",          //  click_action = noti.PageName ,///"FCM_PLUGIN_ACTIVITY",  //Must be present for Android
                            icon = "logo",  //White icon Android resource
                                                     //   tag = noti.LoanID
                        },
                        data = new
                        {
                            page = noti.PageName,  //Any data to be retrieved in the notification callback
                            orderID = noti.OrderID,
                            notificationID = noti.NotificationID
                        },
                        to = deviceId, //Topic or single device
                        priority = "high", //If not set, notification won't be delivered on completely closed iOS app
                        restricted_package_name = "" //Optional. Set for application filtering
                    };










                    var serializer = new JavaScriptSerializer();

                    var json = serializer.Serialize(data);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                    tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                    tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                    tRequest.ContentLength = byteArray.Length;

                    tRequest.UseDefaultCredentials = true;
                    tRequest.PreAuthenticate = true;
                    tRequest.Credentials = CredentialCache.DefaultCredentials;

                    using (Stream dataStream = tRequest.GetRequestStream())
                    {

                        dataStream.Write(byteArray, 0, byteArray.Length);


                        using (WebResponse tResponse = tRequest.GetResponse()) //Error here (The remote server returned an error: (401) Unauthorized.)
                        {

                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {

                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {

                                    String sResponseFromServer = tReader.ReadToEnd();

                                    string str = sResponseFromServer;

                                    //alert(str);
                                    return str;

                                }
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    string str = ex.Message;
                    //alert(str);
                    return str;
                }

            }


            public void RateCompeletedOrder(orDatabase.Order order)
            {
                _app.WepApi.Notification noti = new WepApi.Notification
                {
                    Header = "تم تسليم الطلب بنجاح برجاء تقييم الخدمة",
                    OrderID = order.OrderID,
                    PageName = "RateOrder"

                };

               
                OrDeliveryDBEntities db = new OrDeliveryDBEntities();
                var ms = db.Customers.Find(order.CustomerID).User.MobileSessions
                    .OrderByDescending(x=> x.ExpireDate).FirstOrDefault();

               
                    SendNotification(ms, noti);
                


            }
            public void SendOrder2Driver(orDatabase.Order order)
            {
                _app.WepApi.Notification noti = new WepApi.Notification
                {
                    Header = "تم استلام طلب جديد بحاجه الي التوصيل",
                    OrderID = order.OrderID,
                    PageName = "NewOrder"

                };


                OrDeliveryDBEntities db = new OrDeliveryDBEntities();
                var ms = db.Drivers.Find(order.DriverID).User.MobileSessions
                    .OrderByDescending(x => x.ExpireDate).FirstOrDefault();


                SendNotification(ms, noti);



            }


            public string SendNotification(User u, Notification noti)
            {
                var ms = u.MobileSessions.OrderByDescending(x => x.ExpireDate).FirstOrDefault();

                WepApi.Notification no = new WepApi.Notification { Continet = noti.Continet , Header = noti.Header , PageName="notifications",
                    OrderID = 0 , NotificationID = noti.NotificationID   };

                if (ms != null) { return SendNotification(ms, no); }
                else { return null; }

            }
        }

        public class eMail
        {


            public bool SendFromPaymentMail(string subject, string body, string to)
            {


                try
                {
                   //  SmtpClient client = new SmtpClient("webmail.ordeliverykw.com");
                    //   SmtpClient client = new SmtpClient("ordeliverykw.com");
                    //If you need to authenticate
                    //   client.EnableSsl = true;

                    SmtpClient client = new SmtpClient("localhost", 25);
                  //  SmtpClient client = new SmtpClient("ordeliverykw.a2hosted.com", 25);
                    client.EnableSsl = false;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                   client.Credentials = new NetworkCredential("payment-no-replay@ordeliverykw.com", "~Lz9vi88");
                   // client.Credentials = new NetworkCredential("ahmed@ordeliverykw.com", "Ahmed123");

                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress("payment-no-replay@ordeliverykw.com", "Or Payement Portal");
                    mailMessage.To.Add(to);
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = true;


                    client.Send(mailMessage);
                    return true;
                }
                catch (Exception e)
                {

                    return false;
                }


            }


            //public string ContactUSMailTemplate()
            //{


            //    //string text;
            //    //var fileStream = new FileStream(HttpContext.Current.Server.MapPath(@"html\contactusMailTemplate.html"), FileMode.Open, FileAccess.Read);
            //    //using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            //    //{
            //    //    text = streamReader.ReadToEnd();
            //    //}

            //    //return text;

            //    string[] readText = File.ReadAllLines(HttpContext.Current.Server.MapPath("~/html/contactusMailTemplate.html"));
            //    StringBuilder strbuild = new StringBuilder();
            //    foreach (string s in readText)
            //    {
            //        strbuild.Append(s);
            //        strbuild.AppendLine();
            //    }
            //    return strbuild.ToString();

            //}

        }

    }


}


    


