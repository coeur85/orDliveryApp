﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using orDatabase;
using System.IO;
using System.Drawing;
using Newtonsoft.Json;
using orDliveryApp;
using Microsoft.AspNet.SignalR;
using System.Security.Claims;
using Microsoft.Owin.Security;
using System.Web.Mvc;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

namespace _app
{
    public class DateTime
    {
        public static System.DateTime Now
        {
            get
            {
                // return System.DateTime.Now ;
                return TimeZoneInfo.ConvertTime(System.DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Arab Standard Time"));
            }
        }


    }


    namespace PageClass
    {
        namespace Reports
        {
            public class OrderSearch
            {
                public int CustomerID { get; set; }
                public int DriverID { get; set; }
                public int ServiceProviderID { get; set; }

                public System.DateTime From { get; set; }
                public System.DateTime To { get; set; }

                public string Fromstr { get; set; }
                public string Tostr { get; set; }

                public ServiceProvider ServiceProvider { get; set; }


            }

            public class Complted
            {
                [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
                public System.DateTime from { get; set; }
                [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
                public System.DateTime to { get; set; }
                public List<orDatabase.Order> Orders { get; set; }
            }

            public class ServiceProviderPrintReport
            {
                public OrderSearch SearchParamters { get; set; }
                public List<orDatabase.Order> Orders { get; set; }

                public ServiceProviderPrintReport() { Orders = new List<Order>(); }
                public string UserName { get; set; }

            }
        }

        public class Login
        {

            public string UserName { get; set; }
            public string Password { get; set; }
            public string Message { get; set; }


        }
        public class DeleteSPC
        {
            public ServiceProviderCategory ServiceProviderCategory { get; set; }
            public bool CanBeDeleted { get; set; }

        }
        public class DeleteSP
        {
            public ServiceProvider ServiceProvider { get; set; }
            public bool CanBeDeleted { get; set; }

        }
        public class DeleteDriver
        {
            public Driver Driver { get; set; }
            public bool CanBeDeleted { get; set; }

        }
        public class DeleteCustomer
        {
            public Customer customer { get; set; }
            public bool CanBeDeleted { get; set; }

        }
        public class NotiList
        {
            public string ControllerName { get; set; }
            public string ActionName { get; set; }
            public string IndexActionName { get; set; }
            public int OrderStatus { get; set; }
            public bool AlwaysPromote { get; set; }
            public string iconName { get; set; }

            public string HeaderString { get; set; }

        }
        public class DropDownItem
        {
            public int Value { get; set; }
            public string Text { get; set; }

        }
        public class PickDriver
        {
            //public List<Driver> free { get; set; }
            //public List<Driver> busy { get; set; }
            //public List<driverDistance> distances { get; set; }

            public PickDriver()
            {
                //free = new List<Driver>(); busy = new List<Driver>(); distances = new List<driverDistance>();
                Drivers = new List<driverDistance>();
                pickDriverSearchItems = new List<PickDriverSearchItem>();

            }

            public List<driverDistance> Drivers { get; set; }
            public Order Order { get; set; }

            public List<PickDriverSearchItem> pickDriverSearchItems { get; set; }


        }

        public class PickDriverSearchItem
        {
            public int driverid { get; set; }
            public string cssClass { get; set; }
            public string name { get; set; }

        }
        public class DriverMapPath
        {

            public GoogleLocation StartPoint { get; set; }
            public GoogleLocation EndPoint { get; set; }
            public GoogleLocation ServericeProvider { get; set; }
            public Order Order { get; set; }
            public List<TextValue> RoutList { get; set; }
             public Boolean IsDriverBusy { get; set; }
            public Driver Driver { get; set; }

            public DriverMapPath() { RoutList = new List<TextValue>(); }

        }
        public class GoogleLocation
        { public decimal lat { get; set; }
            public decimal lng { get; set; }
        }
        public class driverDistance
        {
            //    public int DriverID { get; set; }
            //    public GoogleDistanceMatrix.Services.GoogleDistanceMatrixApi.Response Distance { get; set; }

            public Driver Driver { get; set; }
            public Boolean IsFree { get; set; }
            public GoogleDistanceMatrix.Services.GoogleDistanceMatrixApi.Response.Element GoogleRespons { get; set; }



        }
        public class TextValue
        { public string Text { get; set; }
          public string Value { get; set; }
          public ActionURL URL { get; set; }
        }
        public class ActionURL
        { public string Action { get; set; }
            public string Contoller { get; set; }
            public int Parametr { get; set; }
            public string Text { get; set; }
        }
        public class AdminHomePage
        {
            public int DrAll { get; set; }
            public int DrOn { get; set; }
            public int DrOff { get; set; }

            public int CusAll { get; set; }
            public int CusNew { get; set; }
            public int CusOld { get; set; }


            public int OrdAll { get; set; }
            public int OrdOpen { get; set; }
            public int OrdClosed { get; set; }


            public int AdAll { get; set; }
            public int AdOn { get; set; }
            public int AdOff { get; set; }


            public int ItemAll { get; set; }
            public int ItemOn { get; set; }
            public int itemOff { get; set; }

            public string MessageBlance { get; set; }

            public ChartClass Chart1 { get; set; }
            public ChartClass Chart2 { get; set; }

            public IEnumerable<orDatabase.Order> OpenOrders { get; set; }
        }
        public class ClientHomePage
        {
            
            public int CusAll { get; set; }


            public bool IsOpen { get; set; }
            public TimeSpan From { get; set; }
            public TimeSpan To { get; set; }


            public int OrdAll { get; set; }
            public int OrdOpen { get; set; }
            public int OrdClosed { get; set; }


            public int ItemAll { get; set; }
            public int ItemOn { get; set; }
            public int itemOff { get; set; }


            public ChartClass Chart1 { get; set; }
            public ChartClass Chart2 { get; set; }

            public IEnumerable<orDatabase.Order> OpenOrders { get; set; }
        }
        public class ChartClass
        {
            public string[] Lables { get; set; }
            public string[] Data { get; set; }
            public string text { get; set; }
            public string PointTilite { get; set; }
            public string label { get; set; }
            public string xAexTitle { get; set; }
            public string yAexTitle { get; set; }
            public int CanvesID { get; set; }
           // public ChartClass() { Lables = new List<object>(); Data = new List<object>(); }
            public string ChartType { get; set; }
            public string Color { get; set; }
        }

        public class Search
        {
            public string KeyWord { get; set; }
            public List<orDatabase.User> Users { get; set; }
            public List<orDatabase.Order> Orders { get; set; }

           public Search() { Users = new List<User>(); Orders = new List<Order>(); }
        }


        public class ClientSearch
        {

            public List<Customer> Customers { get; set; }
            public string KeyWord { get; set; }

            public List<Item> Items { get; set; }
            public List<orDatabase.Order> Orders { get; set; }
            public ClientSearch() { Customers = new List<Customer>(); Items = new List<Item>(); Orders = new List<Order>(); }
        }


        public class RatingControl
        {
            public int Rating { get; set; }
            public string Text { get; set; }

            public ActionURL UserLink { get; set; }

            public bool FromCustomer { get; set; }
            public string UserPhoto { get; set; }

            public orDatabase.Order Order { get; set; }

        }

    }

    public class UI
    {
        public class msg
        {
            private static string txt = null;

            public static string Show
            {
                get
                {
                    var r = txt;
                    txt = null;
                    return r;
                }
                set { txt = value; }
            }

        }
        public static User CurrentUser
        {
            get
            {
                //  return (User)HttpContext.Current.Session["User"];
                //if (HttpContext.Current.Request.Cookies["ord"].Expires > _app.DateTime.Now)
                //{
                    if (HttpContext.Current.Request.Cookies["ord"] != null)
                    {
                        OrDeliveryDBEntities db = new OrDeliveryDBEntities();
                    int id = Convert.ToInt32( HttpContext.Current.Request.Cookies["ord"].Value);
                        return db.Users.FirstOrDefault(x => x.UserID == id );
                    }
               // }
                return null;
            }
            set
            {
                //  HttpContext.Current.Session["User"] = value;
                if (value != null)
                {
                    //   HttpContext.Current.Response.Cookies["ord"].Value = value.UserID.ToString();
                    //HttpContext.Current.Response.Cookies["ord"].Expires = _app.DateTime.Now.AddDays(10);

                    HttpCookie cookie = new HttpCookie("ord", value.UserID.ToString());
                    cookie.Expires = DateTime.Now.AddDays(10);
                    HttpContext.Current.Response.Cookies.Add(cookie);

                }
                else
                { HttpContext.Current.Response.Cookies["ord"].Value = null;
                    HttpContext.Current.Response.Cookies["ord"].Expires = _app.DateTime.Now.AddDays(-1);
                }


            }
        }



        public class Photos
        {
            private static string ConvertToBase64(Stream stream)
            {
                Byte[] inArray = new Byte[(int)stream.Length];

                stream.Read(inArray, 0, (int)stream.Length);
                //  Convert.ToBase64CharArray(inArray, 0, inArray.Length, outArray, 0);
                return Convert.ToBase64String(inArray);
                // return new MemoryStream(Encoding.UTF8.GetBytes(outArray));
            }
            private static string fileNameAndEtention(string base64Photo)
            {
                string base64 = base64Photo.Substring(base64Photo.IndexOf(',') + 1);
                string[] sar = base64Photo.Split(';');
                var ext = sar[0].Remove(0, sar[0].IndexOf("/") + 1);

                if (ext == "jpeg") { ext = "jpg"; }

                string fileName = sar[1].Remove(0, sar[1].Length - 50);
                fileName = fileName.Replace("/", "1");
                fileName = fileName.Replace("=", "2");
                fileName = fileName.Replace(".", "3");
                fileName = fileName.Replace(",", "4");
                fileName = fileName.Replace("+", "5");

                return (fileName + "." + ext);
            }
            public static string replacePhoto(string oldfile, HttpFileCollectionBase file)

            {

                if (file.Count > 0)
                {


                    return replacePhoto(oldfile, file[0]);




                }

                return oldfile;


            }

            public static string replacePhoto(string oldfile, HttpPostedFileBase fe)

            {

                if (fe.ContentType != "application/octet-stream")
                {
                    return "data:" + fe.ContentType + ";base64," + ConvertToBase64(fe.InputStream);



                }
                return oldfile;

            }

            public static string replacePhoto(string oldfile, string file)

            {

                if (!string.IsNullOrEmpty(file)) { return file; }
                else if (!string.IsNullOrEmpty(oldfile)) { return oldfile; }
                else { User u = new User(true); return u.Photo; }


            }
            public static string AbsoluteUrl(string base64Photo)
            {

                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                if (HttpContext.Current.Request.Url.PathAndQuery == "/")
                {
                    if (url.EndsWith("/"))
                    { url = url.Remove(url.Length - 1); }
                }
                else { url = url.Replace(HttpContext.Current.Request.Url.PathAndQuery, string.Empty); }

                string base64 = base64Photo.Substring(base64Photo.IndexOf(',') + 1);
                byte[] bytes = Convert.FromBase64String(base64);


                var fileName = fileNameAndEtention(base64Photo);

                var relativePath = "~/ImageStorage/" + fileName;
                var absolutePath = HttpContext.Current.Server.MapPath(relativePath);

                // check if this file was already created


                if (!System.IO.File.Exists(absolutePath))
                {


                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        Image image = Image.FromStream(ms);
                        image.Save(absolutePath);
                    }

                }

                return url + "/" + relativePath.Replace("~/", "");
            }
            public static string LocalUrl(string base64Photo)
            {

                string base64 = base64Photo.Substring(base64Photo.IndexOf(',') + 1);
                byte[] bytes = Convert.FromBase64String(base64);

                var fileName = fileNameAndEtention(base64Photo);

                var relativePath = "~/ImageStorage/" + fileName;
                var absolutePath = HttpContext.Current.Server.MapPath(relativePath);

                // check if this file was already created


                if (!System.IO.File.Exists(absolutePath))
                {


                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        Image image = Image.FromStream(ms);
                        image.Save(absolutePath);
                    }

                }

                return "/" + relativePath.Replace("~/", "");
            }




        }


        public class gLocation
        {

            private string LID;
            public string lid { get { return "gmap" + LID; } set { LID = value; } }
            public string lng { get; set; }
            public string lat { get; set; }
            public bool readOnly { get; set; }


        }


        public class NotiMethods
        {

            private static OrDeliveryDBEntities db = new OrDeliveryDBEntities();
            public static List<Order> Orders(int sts)
            {
                db = new OrDeliveryDBEntities();
                
                var ol = db.Orders.Where(x => x.OrderStatusID == sts).OrderBy(x => x.OrderDate).ToList();
                if (CurrentUser.Administrator != null)
                {
                    if (sts == OrderStatusEnum.DeclinedBySp)
                    { ol = ol.Where(x => x.LastAudit.ActionDate.AddDays(7) >= _app.DateTime.Now).ToList(); }


                    else if (sts == OrderStatusEnum.DriverDeclined)
                    { ol = db.Orders.Where(x => x.OrderAudits.Any(y => y.NewStatusID == OrderStatusEnum.DriverDeclined )).ToList()
                            .Where(x=> x.LastAudit.ActionDate.AddDays(7) >= _app.DateTime.Now).ToList(); }

                    return ol;

                }


                else if (CurrentUser.ServiceProvider != null)
                { return ol.Where(x => x.SPID == CurrentUser.UserID).ToList(); }
                else { return null; }
            }
            public static string StatusName(int sts)
            {
                db = new OrDeliveryDBEntities();
                return db.OrderStatues.FirstOrDefault(x => x.StatusID == sts).StatusName;
            }
            public static orDatabase.Order MyNoti()
            {
                db = new OrDeliveryDBEntities();
                orDatabase.Order or = new orDatabase.Order();
                if (CurrentUser.Administrator != null)
                {
                    or = db.Orders.Where(x => x.OrderStatusID == orDatabase.OrderStatusEnum.OrderReadyWaiting4Delivery).ToList()
                        .OrderByDescending(x => x.LastAudit.ActionDate).FirstOrDefault();

                }
                else if (CurrentUser.ServiceProvider != null)
                {
                  or =  db.Orders.Where(x => x.SPID == CurrentUser.UserID && x.OrderStatusID == orDatabase.OrderStatusEnum.Sent2SP).ToList()
                        .OrderByDescending(x => x.LastAudit.ActionDate).FirstOrDefault();

                }


                return or;
            }


        }
    }


    namespace WepApi
    {
        namespace Convert
        {
            class Json2DB
            {
                public static void item(_app.WepApi.Order.items it, OrderItem oi, OrDeliveryDBEntities db)
                {
                    var dbitem = db.Items.FirstOrDefault(x => x.ItemID == it.ItemID);
                    oi.ItemID = it.ItemID;
                    oi.Quantity = it.Quantity;
                    oi.ItemPrice = dbitem.ItemPrice;
                    //  oi.OIPropertyValues.Clear();


                    foreach (var p in oi.OIPropertyValues.ToList())
                    {
                        // oi.OIPropertyValues.Remove(p);
                        db.OIPropertyValues.Remove(p);
                    }

                    foreach (var p in it.Properties)
                    {

                        OIPropertyValue pv = new OIPropertyValue();
                        var dbp = db.Properties.Find(p.PropertyID);
                        pv.OIID = oi.OIID;
                        pv.PropertyID = dbp.PropertyID;
                        pv.ValueID = p.SelectedValueID;
                        pv.ValuePrice = dbp.PropertyValues.FirstOrDefault(x => x.ValueID == p.SelectedValueID).ValuePrice;
                        oi.OIPropertyValues.Add(pv);
                    }

                }
            }
            class DB2Json
            {

                private static bool canBecanCanclled(int sts, int? paymnetMethodID)
                {
                    if (paymnetMethodID == null) { paymnetMethodID = 0; }

                    if (sts == 2 && paymnetMethodID == 1) {
                        return true;

                    }
                    else { return false; }
                }
                private static int clacRating(List<orDatabase.Order> ordl) 
                {
                    int r = 0;
                    if (ordl == null) { return 0; }
                    if (ordl.Count > 0)
                    {
                        foreach (var item in ordl)
                        {
                            if(item.OrderRating != null)
                            {  r += item.OrderRating.Value;}
                           
                        }
                        r = r / ordl.Count;
                    }
                    return r;
                }
                private static string AddressLat(orDatabase.CustomerAddress add)
                {
                    if (add == null) { return null; }
                    else { return add.Lat; }
                }
                private static string AddressLng(orDatabase.CustomerAddress add)
                {
                    if (add == null) { return null; }
                    else { return add.Lng; }
                }
                private static WepApi.Order.PaymnetMethod PM(orDatabase.PaymentMethod pymm)
                {
                    if (pymm == null) { return null; }
                    else { return new Order.PaymnetMethod { MethodID = pymm.PMID, MethodName = pymm.PMname }; }
                }
                private static string Splat(orDatabase.ServiceProvider sp)
                { if (sp != null) { return sp.Lat; }
                    return null;
                }
                private static string Splng(orDatabase.ServiceProvider sp)
                {
                    if (sp != null) { return sp.Lng; }
                    return null;
                }
                public static bool isWorkinkNow(TimeSpan from, TimeSpan to)
                {

                    ////DateTime d1, d2;
                    ////d1 = Convert.ToDateTime(from);
                    ////d2 = Convert.ToDateTime(to);
                    //var d = DateTime.Now.Date;
                    //var h = (to - from).TotalHours;
                    //if (h == 0) { return true; }
                    //if (h < 0) {h = 24 + h; }
                    //// else {d =  d.AddHours(h); }
                    //d = d.AddHours(from.Hours + h);
                    //if (d.Date > _app.DateTime.Now.Date) { d = d.AddDays(-1); }

                    //if (d > _app.DateTime.Now) { return true; }
                    //else { return false; }

                    if (from == to) { return true; }
                    if (from.Hours < 12 && to.Hours < 12 )
                    {
                        if(_app.DateTime.Now.TimeOfDay > from)
                        { return true; }
                        if (_app.DateTime.Now.TimeOfDay < to)
                        { return true; }
                    }

                    if (from <= _app.DateTime.Now.TimeOfDay && _app.DateTime.Now.TimeOfDay <= to)
                    { return true; }
                    else { return false; }


                }
                private static int SPID(ServiceProvider sp)
                { if (sp == null) return 0; return sp.SPID; }
                private static int SpCa(ServiceProvider sp)
                { if (sp == null) return 0; return sp.SPCat; }
                public static List<_app.WepApi.Order.order> Orders(List<orDatabase.Order> orders)
                {

                    List<_app.WepApi.Order.order> ord = new List<WepApi.Order.order>();
                    ord = (from x in orders.ToList()
                           select new _app.WepApi.Order.order
                           {
                               AddressID = x.AddressID,
                               CustomerNotes = x.CustomerNotes,
                               OrderDate = x.OrderDate,
                               OrderID = x.OrderID,
                               Rating = x.OrderRating,
                               RatingComment = x.RatingComment,
                               TotalOrder = x.Grandtotal,
                               SPName = x.ServiceProvider.SpName,
                               OrderStatus=x.OrderStatue.StatusName,
                               OrderStatusID=x.OrderStatusID,
                               CanBeCanclled = canBecanCanclled(x.OrderStatusID,x.PMID),
                               CusomerPhone = x.Customer.PhoneNumber,
                               CustmerLat = AddressLat(x.CustomerAddress),
                               CusomerLng = AddressLng(x.CustomerAddress),
                               PaymnetMethod = PM(x.PaymentMethod),
                               SpLat = Splat(x.ServiceProvider),
                               SpLng = Splng(x.ServiceProvider),
                               DeliveryTimeframe = x.ServiceProvider.DeliveryTimeframe

                           }).ToList();
                    return ord;
                }
                public static _app.WepApi.Order.order Order(orDatabase.Order order)
                {

                    _app.WepApi.Order.order ord = new _app.WepApi.Order.order
                    {
                        AddressID = order.AddressID,
                        CustomerNotes = order.CustomerNotes,
                        OrderDate = order.OrderDate,
                        OrderID = order.OrderID,
                        Rating = order.OrderRating,
                        RatingComment = order.RatingComment,
                        TotalOrder = order.Grandtotal,
                        OrderStatus = order.OrderStatue.StatusName,
                        OrderStatusID = order.OrderStatusID,
                        SPName = order.ServiceProvider.SpName,
                        DeliveryCharges = order.ServiceProvider.DeliveryCharges,
                        CanBeCanclled = canBecanCanclled(order.OrderStatusID, order.PMID),
                        CusomerPhone = order.Customer.PhoneNumber,
                        CustmerLat = AddressLat(order.CustomerAddress),
                        CusomerLng = AddressLng(order.CustomerAddress),
                        PaymnetMethod = PM(order.PaymentMethod),
                        SpLat= Splat(order.ServiceProvider),
                        SpLng= Splng(order.ServiceProvider),
                        SPID = SPID(order.ServiceProvider),
                        SpCatID = SpCa(order.ServiceProvider),
                        DeliveryTimeframe = order.ServiceProvider.DeliveryTimeframe

                    };

                    ord.items = (from x in order.OrderItems.ToList()
                                 select new _app.WepApi.Order.items
                                 {
                                     ItemID = x.ItemID,
                                     ItemName = x.Item.ItemName,
                                     ItemPrice = x.ItemPrice,
                                     OIID = x.OIID,
                                     MenuID = x.Item.CatID,
                                     Photo = _app.UI.Photos.AbsoluteUrl(x.Item.Photo),
                                     Quantity = x.Quantity,
                                     TotalPrice = x.TotalPrice,
                                     Properties = (from y in x.OIPropertyValues.ToList()
                                                   select new _app.WepApi.Order.ItemProperty
                                                   {
                                                       IsMandatory = y.Property.IsMandatory,
                                                       PropertyID = y.PropertyID,
                                                       PropertyName = y.Property.PropertyName,
                                                       SelectedValueID = y.ValueID,
                                                       PropertyValues = (from z in y.Property.PropertyValues.ToList()
                                                                         select new _app.WepApi.Order.ItemPropertyValues
                                                                         {
                                                                             ValueID = z.ValueID,
                                                                             ValueName = z.ValueName,
                                                                             ValuePrice = z.ValuePrice

                                                                         }).ToList()


                                                   }).ToList(),
                                     
                                 }).ToList();

                    return ord;
                }
                public static Account.UserProfile Profile(Customer c)
                {
                    Account.UserProfile profile = new Account.UserProfile()
                    {
                        eMail = c.eMail,
                        fName = c.User.fName,
                        lName = c.User.lName,
                        mName = c.User.mName,
                        PhoneNumber = c.PhoneNumber,
                        Photo = _app.UI.Photos.AbsoluteUrl(c.User.Photo)

                    };
                    return profile;

                }
                public static Account.UserProfile Profile(Driver drv)
                {
                    Account.UserProfile profile = new Account.UserProfile()
                    {

                        fName = drv.User.fName,
                        lName = drv.User.lName,
                        mName = drv.User.mName,
                        PhoneNumber = drv.PhoneNumber,
                        Photo = _app.UI.Photos.AbsoluteUrl(drv.User.Photo),
                        BicycleModel = drv.BicycleModel,
                        LicenseNumber = drv.LicenseNumber,
                        

                    };
                    return profile;

                }
                public static _app.WepApi.Order.Sp ServiceProvider(ServiceProvider x)
                {

                    _app.WepApi.Order.Sp sp = new WepApi.Order.Sp
                    {
                        Address = x.Address,
                        DeliveryCharges = x.DeliveryCharges,
                        MinOrder = x.MinOrder,
                        Photo = _app.UI.Photos.AbsoluteUrl(x.Photo),
                        SpName = x.SpName,
                        SPID = x.SPID,
                        WorkingTill = x.WorkingTill,
                        WrokingFrom = x.WrokingFrom,
                        isWorkingNow = isWorkinkNow(x.WrokingFrom, x.WorkingTill),
                        CatID = x.SPCat,
                        CatName = x.ServiceProviderCategory.CatName,
                        DeliveryTime = x.DeliveryTimeframe,
                        Notes = x.Notes,
                        Rating = clacRating(x.Orders.Where(y=> y.OrderStatusID == orDatabase.OrderStatusEnum.Competed).ToList()),
                        Lat = x.Lat,
                        Lng = x.Lng
                        
                    };


                    return sp;
                }
                public static List<_app.WepApi.Order.Sp> ServiceProvider(List<ServiceProvider> sp)
                {

                    List<_app.WepApi.Order.Sp> spl = (from x in sp
                                                      where x.User.Active == true
                                                      select new _app.WepApi.Order.Sp
                                                      {
                                                          Address = x.Address,
                                                          DeliveryCharges = x.DeliveryCharges,
                                                          MinOrder = x.MinOrder,
                                                          Photo = _app.UI.Photos.AbsoluteUrl(x.Photo),
                                                          SpName = x.SpName,
                                                          SPID = x.SPID,
                                                          WorkingTill = x.WorkingTill,
                                                          WrokingFrom = x.WrokingFrom,
                                                          isWorkingNow = isWorkinkNow(x.WrokingFrom,x.WorkingTill),
                                                          Lat = x.Lat,
                                                          Lng = x.Lng,
                                                          Distance = 0,
                                                          Duration = 0,
                                                          CatID= x.SPCat,
                                                          CatName = x.ServiceProviderCategory.CatName,
                                                          DeliveryTime = x.DeliveryTimeframe,
                                                          Notes = x.Notes,
                                                          Rating = clacRating(x.Orders.Where(y => y.OrderStatusID == orDatabase.OrderStatusEnum.Competed).ToList()),
                                                          

                                                      }).ToList();

                    return spl;
                }
                public static List<_app.WepApi.Order.OrderAction> OrderActions(orDatabase.Order order)
                {
                    List<_app.WepApi.Order.OrderAction> oa = new List<Order.OrderAction>();
                    oa = (from x in order.OrderAudits.ToList().OrderBy(y => y.ActionDate).ToList()
                          select new _app.WepApi.Order.OrderAction
                          {
                              actionDate = x.ActionDate,
                              actionID = x.AuditID,
                              actionName = x.OrderStatue.StatusName,
                              StatusID = x.NewStatusID


                          }).ToList();

                    return oa;
                }
                public static List<WepApi.Order.PaymnetMethod> PaymentMethods(List<orDatabase.PaymentMethod> pm)
                {
                    List<WepApi.Order.PaymnetMethod> pml = new List<WepApi.Order.PaymnetMethod>();
                    pml = (from x in pm select new WepApi.Order.PaymnetMethod { MethodID = x.PMID, MethodName = x.PMname }).ToList();
                    return pml;
                }
                public static List<WepApi.Order.Ad> Adds(List<orDatabase.Add> ad)
                {
                    List<WepApi.Order.Ad> ads = new List<Order.Ad>();
                    ads = (from x in ad select new WepApi.Order.Ad
                    {
                         AdID = x.AddID,
                         AdPhoto= _app.UI.Photos.AbsoluteUrl(x.AddPhoto),
                         title = x.ItemsCategory.CatName,
                         DataDetails = new Order.MyDataDetails {
                             CatID = x.ItemsCategory.ServiceProvider.ServiceProviderCategory.CatID,
                             MenuID = x.CatID,
                             
                             SPID = x.ItemsCategory.ServiceProvider.SPID,
                             MaxItemID = 0
                         },
                         ServiceProvider = ServiceProvider(x.ItemsCategory.ServiceProvider)

                    }).ToList();

                    return ads;
                }
                public static Account.Address Address(orDatabase.CustomerAddress ad)
                {
                    return new Account.Address { Address1 = ad.Address1 , Address2 = ad.Address2, AddressID = ad.AddressID,
                    ApartmentNumber = ad.ApartmentNumber, AreaName = ad.AreaName , City = ad.City , Company = ad.Company,
                    DeliveryNotes = ad.DeliveryNotes , Floor = ad.Floor , Lat = ad.Lat , Lng = ad.Lng };
                }
                public static List<Account.Address> Address(List<orDatabase.CustomerAddress> add)
                {
                    List<Account.Address> list = new List<Account.Address>();

                    foreach (var item in add)
                    {
                        list.Add(Address(item));
                    }
                    return list;

                }

                public static List<_app.WepApi.Notification> Notifications(List<orDatabase.Notification> nl)
                {

                    List<_app.WepApi.Notification> noti = new List<Notification>();

                    noti = (from x in nl.OrderByDescending(x=> x.NotificationID).ToList() select new _app.WepApi.Notification
                    {
                         Continet = x.Continet,
                         Header = x.Header,
                         NotificationID = x.NotificationID,
                         PageName="notifications",
                         Photo = _app.UI.Photos.AbsoluteUrl(x.Photo)


                    }).ToList();
                    return noti;
                }

            }



        }
        namespace Account
        {
            public class Singup
            {

                public string PhoneNumber { get; set; }

            }
            public class ConfirmCode
            {
                public string PhoneNumber { get; set; }
                public int Code { get; set; }
                public string DeviceID { get; set; }

            }
            public class UserProfile
            {
                public string fName { get; set; }
                public string mName { get; set; }
                public string lName { get; set; }
                public string Photo { get; set; }
                public string PhoneNumber { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string eMail { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string Password { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string BicycleModel { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string LicenseNumber { get; set; }
                
                


            }
            public class Login
            {
                public string PhoneNumber { get; set; }
                public string Password { get; set; }
                public string DeviceID { get; set; }


            }
            public class UpdateProfileInfo
            {
                public UserProfile userProfile { get; set; }
                public string SessionID { get; set; }
            }
            public class UpdatePassword
            {
                public string SessionID { get; set; }
                public string Password { get; set; }

            }
            public class Address
            {
                public int AddressID { get; set; }
                public string AreaName { get; set; }
                public string City { get; set; }
                public string Address1 { get; set; }
                public string Address2 { get; set; }
                public string ApartmentNumber { get; set; }
                public string Company { get; set; }
                public string Floor { get; set; }
                public string DeliveryNotes { get; set; }
                public string Lat { get; set; }
                public string Lng { get; set; }


            }
            public class upDateAddress
            {
                public string SessionID { get; set; }
                public Address Address { get; set; }
            }
            public class Singin
            {
                public string LoginID { get; set; }
                public string Password { get; set; }
                public string DeviceID { get; set; }

                public LocationSync Location { get; set; }

            }
            public class LocationSync
            {
                public string SessionID { get; set; }
                public decimal Lat { get; set; }
                public decimal Lng { get; set; }

            }
            public class FavouriteSP
            {
                public string SessionID { get; set; }
                public int SPID { get; set; }
            }
        }
        namespace Response
        {
            public class Ok
            {
                public bool Status { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public object Data { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string Messages { get; set; }

                public Ok() { Status = true; }
            }
            public class Error
            {
                public bool Status { get; set; }

                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public List<Message> Phone { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public List<Message> Password { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public List<Message> Account { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public List<Message> Order { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public List<Message> Developer { get; set; }

                public bool HasErrors()
                {

                    if (Phone == null && Password == null && Account == null && Order == null) { return false; }
                    return true;
                }
                public void Clear() {
                    Phone.Clear(); Password.Clear();
                    Account.Clear(); Order.Clear();
                    Developer.Clear();
                }


            }
            public enum ErrorType
            {
                Phone,
                Password,
                Account,
                Order,
                Developer
            }
            public class Message { public string message { get; set; } }
        }
        namespace Order
        {
            public class SPCategory
            {

                public int CatID { get; set; }
                public string CatName { get; set; }
                public string Photo { get; set; }
                public List<Sp> ServiceProviders { get; set; }

                public SPCategory() { ServiceProviders = new List<Sp>(); }
            }
            public class Sp
            {
                public int SPID { get; set; }
                public string SpName { get; set; }
                public decimal MinOrder { get; set; }
                public decimal DeliveryCharges { get; set; }
                public System.TimeSpan WrokingFrom { get; set; }
                public System.TimeSpan WorkingTill { get; set; }
                public string Address { get; set; }
                public string Photo { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Include)]
                public bool isWorkingNow { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Include)]
                public string Lat { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Include)]
                public string Lng { get; set; }

                [JsonProperty(NullValueHandling = NullValueHandling.Include)]
                public int? Distance { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Include)]
                public int? Duration { get; set; }


                public bool isFavourite { get; set; }
                public int CatID { get; set; }
                public string CatName { get; set; }


                public int DeliveryTime { get; set; }
                public string Notes { get; set; }

                public int Rating { get; set; }

            }
            public class Menu
            {
                public int MenuID { get; set; }
                public string MenuName { get; set; }
                public string Photo { get; set; }


            }
            //-- items 
            public class items
            {
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public int OIID { get; set; }
                public int ItemID { get; set; }
                public string ItemName { get; set; }
                public decimal ItemPrice { get; set; }
                public string Photo { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public virtual List<ItemProperty> Properties { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public int Quantity { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public decimal TotalPrice { get; set; }
                public string itemInfo { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public int MenuID { get; set; }
                public items() { Properties = new List<ItemProperty>(); }
            }
            public class ItemProperty
            {
                public int PropertyID { get; set; }
                public string PropertyName { get; set; }
                public bool IsMandatory { get; set; }
                public virtual List<ItemPropertyValues> PropertyValues { get; set; }

                public ItemProperty() { PropertyValues = new List<ItemPropertyValues>(); }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public int SelectedValueID { get; set; }
            }
            public class ItemPropertyValues
            {
                public int ValueID { get; set; }
                public string ValueName { get; set; }
                public decimal ValuePrice { get; set; }
                //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                //public bool isSellected { get; set; }
            }
            public class OrderAction
            {
                public int actionID { get; set; }
                public string actionName { get; set; }
                public System.DateTime actionDate { get; set; }
                public int StatusID { get; set; }

            }
            //-- end of items
            public class order
            {
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public int OrderID { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public System.DateTime OrderDate { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string OrderStatus { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public int OrderStatusID { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public Nullable<int> DriverID { get; set; }

                public int? AddressID { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public Nullable<int> Rating { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string RatingComment { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string CustomerNotes { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public decimal TotalOrder { get; set; }
                public decimal DeliveryCharges { get; set; }
                public List<items> items { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string SPName { get; set; }

                public bool CanBeCanclled { get; set; }

                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public List<OrderAction> orderActions { get; set; }


                public int ItmesQuantaty { get; set; }

                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string CustmerLat { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string CusomerLng { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string CusomerPhone { get; set; }

                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public PaymnetMethod PaymnetMethod { get; set; }



                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string SpLat { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string SpLng { get; set; }

                public int SPID { get; set; }
                public int SpCatID { get; set; }

                public int DeliveryTimeframe { get; set; }

            }
            public class AddItem
            {
                public string SessionID { get; set; }
                public items item { get; set; }

            }
            public class ItemDetails
            {
                public string SessionID { get; set; }
                public int OIID { get; set; }
            }
            public class RateOrder
            {
                public string SessionID { get; set; }
                public int OrderID { get; set; }
                public int rating { get; set; }
                public string Comment { get; set; }
            }
            public class OrderAddress
            {
                public string SessionID { get; set; }
                public int AddressID { get; set; }
            }
            public class MyDataDetails
            {
                //  public string SessionID { get; set; }
                public int CatID { get; set; }
                public int SPID { get; set; }
                public int MenuID { get; set; }
                public int ItemID { get; set; }

                public decimal lat { get; set; }
                public decimal lng { get; set; }

                public string SessionID { get; set; }
                public int MaxItemID { get; set; }
            }
            public class orderDetails
            {
                public string SessionID { get; set; }
                public int OrderID { get; set; }
                public string Notes { get; set; }

            }
            public class DriverOrderDetails
            {
                public Sp SP { get; set; }
                public order Order { get; set; }
                public Account.UserProfile Customer { get; set; }
                public Account.Address CustomerAddress { get; set; }


            }
            public class PaymnetMethod
            {
                public int MethodID { get; set; }
                public string MethodName { get; set; }
               

            }
            public class OrderPayment
            {
                public string SessionID { get; set; }
                public int MethodID { get; set; }
                public string CustomerEmail { get; set; }

            }

            public class OnlinePaymentEmail
            {
                public string SessionID { get; set; }
                public string CutomerEmail { get; set; }

            }

            public class Ad
            {
                public int AdID { get; set; }
                public string AdPhoto { get; set; }
                public string title { get; set; }
                public MyDataDetails DataDetails { get; set; }
                public Sp ServiceProvider { get; set; }

            }
            public class MyOrders
            {
                public string SessionID { get; set; }
                public int OrderStatus { get; set; }

            }        
            public class Customer
            {
                public int CustomerID { get; set; }
                public string PhoneNumber { get; set; }
                public string eMail { get; set; }
                public string Name { get; set; }
                public string Photo { get; set; }

            }
            public class OrderNote
            {
                public string SessionID { get; set; }
                public string OrderNotes { get; set; }
            }

        }
        public class Session
        {
            public string SessionID { get; set; }

        }
        public class Methods
        {
            private OrDeliveryDBEntities db = new OrDeliveryDBEntities();

            public string LogUserIn(User u, string deviceID)
            {
                var ms = db.Users.FirstOrDefault(x => x.UserID == u.UserID)
                    .MobileSessions.Where(x => x.ExpireDate >= _app.DateTime.Now).FirstOrDefault();

                if (ms != null)
                {
                    if (ms.MobileID != deviceID)
                    {
                        ms.ExpireDate = _app.DateTime.Now.AddDays(-1);
                        db.SaveChanges();
                        ms = null;
                    }

                }


                if (ms == null)
                {
                    ms = new MobileSession();

                    ms.SessionID = Guid.NewGuid();
                    ms.UserID = u.UserID;
                    ms.MobileID = deviceID;
                    ms.ExpireDate = _app.DateTime.Now.AddDays(100);
                    db.MobileSessions.Add(ms);

                    var d = db.Drivers.FirstOrDefault(x => x.User.UserID == u.UserID);
                    if (d != null) { d.Online = true; }

                    db.SaveChanges();

                }

                

                return ms.SessionID.ToString();

            }
            public int SendCode(Customer customer)
            {

                if (customer.CodeDate != null)
                {
                    if (customer.CodeDate.Value.AddMinutes(5) >= _app.DateTime.Now)
                    {
                        return customer.ActivationCode.Value;
                    }
                }


                Random r = new Random();
                customer.ActivationCode = r.Next(100000, 999999);
                customer.CodeDate = _app.DateTime.Now;
                customer.User.Active = false;

                _app.Send.TextMessage text = new Send.TextMessage();
                text.SendConfirmText(customer.PhoneNumber, customer.ActivationCode.Value.ToString());
                return customer.ActivationCode.Value;
            }
            public void logUserOut(orDatabase.MobileSession m)
            {
                var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID == m.SessionID);

                ms.ExpireDate = _app.DateTime.Now.AddDays(-1);
                var d = ms.User.Driver;
                if (d != null) { d.Online = false; }
                db.SaveChanges();

            }
        }
        public class MobileWelcomeScreen
        {

            public int dbid { get; set; }
            public string Photo { get; set; }
        }
        public class Notification
        {
            public string Header { get; set; }
            public string Continet { get; set; }
            public string PageName { get; set; }
            public int OrderID { get; set; }
            public int NotificationID { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string Photo { get; set; }

        }
        public class MyNotificationDetails
        {
            public string SessionID { get; set; }
            public int NotificationID { get; set; }

        }
        public class Search
        {
            public string KeyWord { get; set; }

        }

        namespace Paymnets {
            public class BankResponse
            {
                public long PaymentId { get; set; }
                public string result { get; set; }
                public string Auth { get; set; }
                public string Ref { get; set; }
                public string Tranid { get; set; }
                public string trackid { get; set; }
                public string responsecode { get; set; }
                public string eci { get; set; }



            }

        }
    }


    enum SystemSetup
    {
        mobileWellcomeScreen = 1,
        SupportPhone =2
    }


    public class Audits
    {
        private static OrderAudit newAudit(Order o, int status, int user)
        {
            o.OrderStatusID = status;
            return new OrderAudit()
            {

                ActionDate = _app.DateTime.Now,
                NewStatusID = status,
                Order = o,
                OrderID = o.OrderID,
                UserID = user
            };

        }
        private static OrderAudit newAudit(Order o, int status, int user, string notes)
        {
            o.OrderStatusID = status;
            return new OrderAudit()
            {

                ActionDate = _app.DateTime.Now,
                NewStatusID = status,
                Order = o,
                OrderID = o.OrderID,
                UserID = user,
                Notes = notes
            };

        }
        public static void NewForStatus1(Order o, int userid)
        { o.OrderAudits.Add(newAudit(o, 1, userid)); }
        public static void NewForStatus2(Order o, int userid)
        {
            ServiceProviderHub sp = new ServiceProviderHub();
            o.OrderDate = _app.DateTime.Now;
            o.OrderAudits.Add(newAudit(o, 2, userid));
            sp.OrderStatus2(o);
        }
        public static void NewForStatus2(Order o, int userid, string notes)
        {
            ServiceProviderHub sp = new ServiceProviderHub();
            o.OrderDate = _app.DateTime.Now;
            o.OrderAudits.Add(newAudit(o, 2, userid,notes));
            sp.OrderStatus2(o);
        }
        public static void NewForStatus3(Order o, int userid)
        { o.OrderAudits.Add(newAudit(o, 3, userid));
            NewForStatus1(o, userid);
        }
        public static void NewForStatus4(Order o, int userid)
        { o.OrderAudits.Add(newAudit(o, 4, userid)); }
        public static void NewForStatus5(Order o, int userid, string notes)
        { o.OrderAudits.Add(newAudit(o, 5, userid, notes)); }
        public static void NewForStatus6(Order o, int userid)
        {
            o.OrderAudits.Add(newAudit(o, 6, userid));

            AdminHub admin = new AdminHub();
            admin.OrderStatus6(o);
            //  adminHub.Clients.All.OrderStatus6();


        }
        public static void NewForStatus7(Order o, int userid)
        { o.OrderAudits.Add(newAudit(o, 7, userid));
            Send.Notifications noti = new Send.Notifications();
            noti.SendOrder2Driver(o);

        }
        public static void NewForStatus8(Order o, int userid)
        { o.OrderAudits.Add(newAudit(o, 8, userid)); }
        public static void NewForStatus9(Order o, int userid, string notes)
        {
            o.OrderAudits.Add(newAudit(o, 9, userid, notes));
            o.Driver = null;
            NewForStatus6(o, userid);
        }
        public static void NewForStatus10(Order o, int userid)
        { o.OrderAudits.Add(newAudit(o, 10, userid)); }
        public static void NewForStatus11(Order o, int userid)
        {
            o.OrderAudits.Add(newAudit(o, 11, userid));
            OrDeliveryDBEntities db = new OrDeliveryDBEntities();
            o.ProfitShare = db.ServiceProviders.Find(o.SPID).ProfitShare;
            Send.Notifications no = new Send.Notifications();
            no.RateCompeletedOrder(o);
        }
        public static void NewForStatus12(Order o, int userid, string notes)
        { o.OrderAudits.Add(newAudit(o, 12, userid, notes)); }
        public static void NewForStatus13(Order o, int userid, string notes)
        { o.OrderAudits.Add(newAudit(o, 13, userid, notes)); }



    }




}

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class MultipleButtonAttribute : ActionNameSelectorAttribute
    {
        public string Name { get; set; }
        public string Argument { get; set; }

        public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
        {
            var isValidName = false;
            var keyValue = string.Format("{0}:{1}", Name, Argument);
            var value = controllerContext.Controller.ValueProvider.GetValue(keyValue);

            if (value != null)
            {
                controllerContext.Controller.ControllerContext.RouteData.Values[Name] = Argument;
                isValidName = true;
            }

            return isValidName;
        }
    }


