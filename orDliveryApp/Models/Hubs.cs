﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Microsoft.AspNet.SignalR;

namespace orDliveryApp
{
    public class AdminHub : Hub
    {
        IHubContext adminHub = GlobalHost.ConnectionManager.GetHubContext<AdminHub>();
        public void Hello()
        {
            Clients.All.hello();
        }

        public void OrderStatus6(orDatabase.Order ord)
        {


            
           // var context = GlobalHost.ConnectionManager.GetHubContext<AdminHub>();
          //  Clients.All.OrderStatus6(data);
          //  adminHub.Clients.All.OrderStatus6(data);
          adminHub.Clients.All.OrderStatus6(ord.OrderID);
        }

        public void newDriverLogin(int drivid)
        {
            adminHub.Clients.All.newDriverLogin(drivid);
        }
       


    }

    public class ServiceProviderHub : Hub
    {

        IHubContext SpHub = GlobalHost.ConnectionManager.GetHubContext<ServiceProviderHub>();
        public void Hello()
        {
            Clients.All.hello();
        }


        public void OrderStatus2(orDatabase.Order ord)
        {

            SpHub.Clients.All.OrderStatus2(ord.OrderID);
        }



    }



    


}