﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using _app.WepApi.Response;
using orDatabase;

namespace _app
{
    public class Validation
    {
        private _app.WepApi.Response.Error ex = new WepApi.Response.Error();
        private OrDeliveryDBEntities db = new OrDeliveryDBEntities();

        public _app.WepApi.Response.Error NewEx(string msg, _app.WepApi.Response.ErrorType et)
        {



            switch (et)
            {
                case ErrorType.Account:
                    if (ex.Account == null) { ex.Account = new List<Message>(); }
                    ex.Account.Add(new Message { message = msg });
                    break;
                case ErrorType.Order:
                    if (ex.Order == null) { ex.Order = new List<Message>(); }
                    ex.Order.Add(new Message { message = msg });
                    break;
                case ErrorType.Password:
                    if (ex.Password == null) { ex.Password = new List<Message>(); }
                    ex.Password.Add(new Message { message = msg });
                    break;
                case ErrorType.Phone:
                    if (ex.Phone == null) { ex.Phone = new List<Message>(); }
                    ex.Phone.Add(new Message { message = msg });
                    break;
                case ErrorType.Developer:
                    if (ex.Developer == null) { ex.Developer = new List<Message>(); }
                    ex.Developer.Add(new Message { message = msg });
                    break;
            }


            return ex;
        }
        public _app.WepApi.Response.Error PhoneNumer(string ph)
        {

            if (ph.Any(x => char.IsLetter(x))) { NewEx("هذا ليس برقم", ErrorType.Phone); }

           // if (!ph.Contains("05")) { NewEx("الرقم المدرج ليس رقم جوال سعودي", ErrorType.Phone); }
        //    else { if (ph.IndexOf("05") != 0) { ph = ph.Remove(0, ph.IndexOf("05")); } }
            if (ph.Length != 8) { NewEx("رقم الجوال لابد ان يكون 8 ارقام", ErrorType.Phone); }

            return ex;



        }
        public Error Password(string password)
        {
            if (password.Length < 6) { NewEx("كلمه المرور يجب ان نكون 6 ارقام او حروف علي الاقل", ErrorType.Password); }
            return ex;
        }
        public Error fname(string name)
        {
            if (string.IsNullOrEmpty(name)) { NewEx("الاسم الاول لا يمكن ان يكون فارغ", ErrorType.Account); }
            return ex;
        }
        public Error lname(string name)
        {
            if (string.IsNullOrEmpty(name)) { NewEx("الاسم الاخير لا يمكن ان يكون فارغ", ErrorType.Account); }
            return ex;
        }
        public Error isCustomer(Customer c)
        {
            if (c == null) { NewEx("لم يتم العثور علي حساب عميل بهذه المدخلات", ErrorType.Account);
            //    return ex;
            }
          //  if (c.User.Active == false) { NewEx("حساب غير مفعل", ErrorType.Account); }
            return ex;
        }
        public Error isCustomer(string phone)
        {

            var c = db.Customers.FirstOrDefault(x => x.User.LoginName == phone);
            return isCustomer(c);
        }
        public Error isCustomer(MobileSession ms)
        {
            var c = db.MobileSessions.FirstOrDefault(x => x.SessionID == ms.SessionID).User.Customer;
            if (c == null) { NewEx("هذا ليس بحساب عميل", ErrorType.Account); }
            return ex;
        }
        public Error SingUp(string ph)
        {
            PhoneNumer(ph);
            if (ex.HasErrors()) { return ex; }
            var c = db.Users.FirstOrDefault(x => x.LoginName == ph);
            if (c != null) NewEx("رقم جوال مسجل لدينا من قبل حاول استعاده كلمه المرور", ErrorType.Account);
            return ex;
        }
        public Error ConfrimCode(_app.WepApi.Account.ConfirmCode model)
        {
            ResendCode(model.PhoneNumber);
            var c = db.Customers.FirstOrDefault(x => x.PhoneNumber == model.PhoneNumber);
            if (c.ActivationCode != model.Code) { return NewEx("كود غير مطابق", ErrorType.Account); }
            return ex;
        }
        public Error UpdateCustomerProfile(_app.WepApi.Account.UpdateProfileInfo model)
        {
            //PhoneNumer(model.userProfile.PhoneNumber);
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            isCustomer(ms);
            if (ex.HasErrors()) { return ex; }
            Password(model.userProfile.Password);
            fname(model.userProfile.fName);
            lname(model.userProfile.lName);
            return ex;


        }
        public Error ResendCode(string PhoneNumber)
        {
            PhoneNumer(PhoneNumber);
            if (ex.HasErrors()) { return ex; }
            var c = db.Customers.FirstOrDefault(x => x.PhoneNumber == PhoneNumber);
            isCustomer(c);
            if (ex.HasErrors()) { return ex; }
            if (c.User.Active) { return NewEx("صاحب الحساب لم يطلب استرجاع كلمه المرور", ErrorType.Account); }
            return ex;
        }
        public Error RestPassword(string phonenumber)
        {
            PhoneNumer(phonenumber);
            if (ex.HasErrors()) { return ex; }
            isCustomer(phonenumber);
            return ex;

        }
        public Error SessionID(string session)
        {

            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == session);
            if (ms == null) { NewEx("خطاء في الحساب برجاء اعاده تسجيل الدخول", ErrorType.Account); return ex; }

            if (ms.ExpireDate <= _app.DateTime.Now)
            { NewEx("لم تستخدم حسابك منذ فتره برجاء اعاده تسجيل الدخول", ErrorType.Account); }
            return ex;
        }
        public Error Address(WepApi.Account.Address model)
        {
            if (string.IsNullOrEmpty(model.AreaName)) { NewEx("لابد من توفير اسم الحي او المنطقه", ErrorType.Account); }
            if (string.IsNullOrEmpty(model.City)) { NewEx("لابد من توفير اسم القطاع", ErrorType.Account); }
            if (string.IsNullOrEmpty(model.Address1)) { NewEx("لابد من توفير اسم الشارع", ErrorType.Account); }
            if (string.IsNullOrEmpty(model.Address1)) { NewEx("لابد من توفير رقم المنزل", ErrorType.Account); }
            if (string.IsNullOrEmpty(model.Lat) || 
                string.IsNullOrEmpty(model.Lng) ) { NewEx("برجاء تحديد العنوان من الخريطه", ErrorType.Account); }


            return ex;
        }
        public Error NoDataFound()
        {
            NewEx("لا يوجد  بينات في هذا القسم", ErrorType.Order);
            return ex;
        }
        public Error BaseketEmpty()
        {
            NewEx("سله المشتريات فارغه", ErrorType.Order);
            return ex;
        }
        public Error ChangeSP()
        {
            NewEx("يوجد اصناف من موفر خدمه اخر في سله المشتريات ،" +
                " لابد من مسح المشرتيات  في السله من اجل الاستمرار ", ErrorType.Order);
            return ex;
        }
        public Error MissingPropertyValue(orDatabase.Property p)
        {
            NewEx("لابد من تحديد " + p.PropertyName, _app.WepApi.Response.ErrorType.Order);
            return ex;
        }
        public Error ZeroQuantaty()
        {
            NewEx("الكميه لابد ان تكون اكبر من صفر", _app.WepApi.Response.ErrorType.Order);
            return ex;
        }
        public Error CheckOutOrder(Order order)
        {
            if (order == null) { NewEx("لايوجد طلب حالي لهذا المستخدم", ErrorType.Order); return ex;}
            
            if (order.OrderItems.Count == 0) { BaseketEmpty(); }
            if (order.OrderStatusID != OrderStatusEnum.NewOrder) { NewEx("يوجد طلب جاري العمل عليه بالفعل", ErrorType.Order); }
            if (order.CustomerAddress == null) { NewEx("برجاء تحديد العنوان", ErrorType.Order); }
            if (order.PaymentMethod == null) { NewEx("برجاء تحديد طريقة الدفع", ErrorType.Order); return ex; }
            if (order.PMID.Value == 2 && 
                order.BankPayements.Where(x=> x.PaymentStatusID == PayemntStatusEnum.Success).Count() == 0) { NewEx("لم يتم قبول الدفع بواسطه بطاقه الائتمان " +
                "، رجاء اعاده المحاوله", ErrorType.Order); }

            return ex;
        }

        public Error Email(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                var valid = (addr.Address == email);
                if (valid == false)
                { NewEx("بريد الكتروني غير سليم", ErrorType.Order); }
                return ex;
            }
            catch
            {
                NewEx("بريد الكتروني غير سليم", ErrorType.Order);
                return ex;
            }
           

        }
        
       
        public Error CanBeModifiedByCustomer(Order order)
        {
           
            if (order.OrderStatusID != OrderStatusEnum.NewOrder) { NewEx("هذا الطلب جاري العمل عليه بالفعل", ErrorType.Order); }
            return ex;
        }
        public Error OpenOrder(Order order)
        {
            if (order == null) {NewEx("لا يوجد طلبات جاريه العمل عليها", ErrorType.Order); return ex; }

            if (order.OrderStatusID == OrderStatusEnum.NewOrder || order.OrderItems.Count() == 0 || order.ServiceProvider == null)
            { NewEx("لا يوجد لك طلبات جاري العمل عليها الان", ErrorType.Order); }
            
            return ex;
        }
        public Error CustomerRateOrder(Order ord)
        {
            if (ord == null) { ex = NoDataFound(); return ex; }
            if (ord.OrderRating != null)
            {
                ex = NewEx("تم تقيم الطلب من قبل", _app.WepApi.Response.ErrorType.Order);
                return ex;
            }

            if (ord.OrderStatusID != orDatabase.OrderStatusEnum.Competed)
            { ex = NewEx("لم يكتمل الطلب بنجاح لا يمكن اتمام التقيم", ErrorType.Order); }
            return ex;
        }
      

        // -- drivers


        public Error isDriver(Driver d)
        {
            if (d == null)
            {
                NewEx("لم نعثر علي حسابك برجاء التاكد من كلمه المرور واسم المستخدم", ErrorType.Account);
                //    return ex;
            }
            return ex;
        }
        public Error isDriver(MobileSession ms)
        {
            var d = db.MobileSessions.FirstOrDefault(x => x.SessionID == ms.SessionID).User.Driver;
            if (d == null) { NewEx("هذا ليس بحساب سائق", ErrorType.Account); }
            return ex;
        }
        public Error DriverRateCustomer(Order ord)
        {
            if (ord == null) { ex = NoDataFound(); return ex; }
            if (ord.DriverRating != null)
            {
                ex = NewEx("تم تقيم الطلب من قبل", _app.WepApi.Response.ErrorType.Order);
                return ex;
            }

            if (ord.OrderStatusID != orDatabase.OrderStatusEnum.Competed)
            { ex = NewEx("لم يكتمل الطلب بنجاح لا يمكن اتمام التقيم", ErrorType.Order); }
            return ex;
        }
        public Error UpdateDriverProfile(_app.WepApi.Account.UpdateProfileInfo model)
        {
            //PhoneNumer(model.userProfile.PhoneNumber);
            var ms = db.MobileSessions.FirstOrDefault(x => x.SessionID.ToString() == model.SessionID);
            isDriver(ms);
            if (ex.HasErrors()) { return ex; }
            Password(model.userProfile.Password);
            fname(model.userProfile.fName);
            lname(model.userProfile.lName);
            PhoneNumer(model.userProfile.PhoneNumber);
            if (ex.HasErrors()) { return ex; }
            if (string.IsNullOrEmpty(model.userProfile.BicycleModel)) { NewEx("نوع المركبة مطلوب ", ErrorType.Account); }
            if (string.IsNullOrEmpty(model.userProfile.LicenseNumber)) { NewEx("رقم رخصة القياده مطلوب ", ErrorType.Account); }
            return ex;


        }

       
        // genral
        public void Clear()
        {
            ex = new Error();

        }
    }
}

