﻿using System;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using orDatabase;

namespace GoogleDistanceMatrix.Services
{
    public class GoogleDistanceMatrixApi
    {
        public class Response
        {
            public string Status { get; set; }

            [JsonProperty(PropertyName = "origin_addresses")]
            public string[] OriginAddresses { get; set; }

            [JsonProperty(PropertyName = "destination_addresses")]
            public string[] DestinationAddresses { get; set; }

            public Row[] Rows { get; set; }

            public class Data
            {
                public int Value { get; set; }
                public string Text { get; set; }
            }

            public class Element
            {
                public string Status { get; set; }
                public Data Duration { get; set; }
                public Data Distance { get; set; }
            }

            public class Row
            {
                public Element[] Elements { get; set; }
            }
        }

        private string Key { get; set; }
        private string Url { get; set; }

        private string[] OriginAddresses { get; set; }
        private string[] DestinationAddresses { get; set; }

        public GoogleDistanceMatrixApi(string[] originAddresses, string[] destinationAddresses)
        {
            OriginAddresses = originAddresses;
            DestinationAddresses = destinationAddresses;

            var appSettings = ConfigurationManager.AppSettings;

            if (string.IsNullOrEmpty(appSettings["GoogleDistanceMatrixApiUrl"]))
            {
                throw new Exception("GoogleDistanceMatrixApiUrl is not set in AppSettings.");
            }
            Url = appSettings["GoogleDistanceMatrixApiUrl"];

            if (string.IsNullOrEmpty(appSettings["GoogleDistanceMatrixApiKey"]))
            {
                throw new Exception("GoogleDistanceMatrixApiKey is not set in AppSettings.");
            }
            Key = appSettings["GoogleDistanceMatrixApiKey"];
        }

        public  Response GetResponse()
        {
           // System.Threading.Thread.Sleep(1500);
            var uri = new Uri(GetRequestUrl());
            WebRequest tRequest = WebRequest.Create(uri);
            tRequest.Method = "get";

            using (WebResponse tResponse = tRequest.GetResponse()) 
            {

                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                {

                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                    {

                        String sResponseFromServer = tReader.ReadToEnd();

                        string str = sResponseFromServer;
                       
                        OrDeliveryDBEntities db = new OrDeliveryDBEntities();
                        db.GoogleRequests.Add(new GoogleRequest { ReqDate = _app.DateTime.Now });
                        db.SaveChanges();
                       return JsonConvert.DeserializeObject<Response>(str);

                    }
                }
            }




        }

        private string GetRequestUrl()
        {
            OriginAddresses = OriginAddresses.Select(HttpUtility.UrlEncode).ToArray();
            var origins = string.Join("|", OriginAddresses);
            DestinationAddresses = DestinationAddresses.Select(HttpUtility.UrlEncode).ToArray();
            var destinations = string.Join("|", DestinationAddresses);
            return $"{Url}?origins={origins}&destinations={destinations}&key={Key}";
        }
    }
}